;;; dashboard.el - functions to interface Emacs with a Dashboard
;;;
;;; Authors:
;;;    Peter Teichman <peter@ximian.com>
;;;

;; Usage: 
;;
;; (dashboard-send-cluepacket context focused '(((type . "keyword")
;;                                               (relevance . 9)
;;                                               (content . "gimp"))
;;                                              ((type . "keyword")
;;                                               (relevance . 2)
;;                                               (content . "eggs"))))
;;
;; relevance and type are optional

(defcustom dashboard-frontend "dashboard.el"
  "*Client name sent to the Dashboard server."
  :type 'string
  :group 'dashboard)

(defcustom dashboard-host "localhost"
  "*Dashboard host to connect to."
  :type 'string
  :group 'dashboard)

(defcustom dashboard-port 5913
  "*Dashboard host to connect to."
  :type 'integer
  :group 'dashboard)

; this function stolen shamelessly from w3's url.el
; there, it is called url-insert-entities-in-string
(defun dashboard-escape-xml-entities (string)
  "Convert HTML markup-start characters to entity references in STRING.
  Also replaces the \" character, so that the result may be safely used as
  an attribute value in a tag.  Returns a new string with the result of the
  conversion.  Replaces these characters as follows:
    &  ==>  &amp;
    <  ==>  &lt;
    >  ==>  &gt;
    \"  ==>  &quot;"
  (if (string-match "[&<>\"]" string)
      (save-excursion
        (set-buffer (get-buffer-create " *entity*"))
        (erase-buffer)
        (buffer-disable-undo (current-buffer))
        (insert string)
        (goto-char (point-min))
        (while (progn
                 (skip-chars-forward "^&<>\"")
                 (not (eobp)))
          (insert (cdr (assq (char-after (point))
                             '((?\" . "&quot;")
                               (?& . "&amp;")
                               (?< . "&lt;")
                               (?> . "&gt;")))))
          (delete-char 1))
        (buffer-string))
    string))

(defun dashboard-send-raw-cluepacket (packet)
  (let ((socket (condition-case err
                    (open-network-stream "dashboard" nil
                                         dashboard-host dashboard-port)
                  (file-error nil))))
    (cond (socket
           (process-send-string socket packet)
           (delete-process socket)))))

(defun dashboard-build-clue (clue)
  (let ((attrs     "")
        (type      (cdr (assoc 'type clue)))
        (relevance (cdr (assoc 'relevance clue)))
        (content   (cdr (assoc 'content clue))))
    (if type
        (setq attrs (concat attrs
			    " Type=\"" (dashboard-escape-xml-entities type)
			    "\"")))
    (if relevance
        (setq attrs (concat attrs
                            " Relevance=\"" (int-to-string relevance) "\"")))
    (concat "<Clue" attrs ">"(dashboard-escape-xml-entities content)
	    "</Clue>")))

(defun dashboard-build-cluepacket (clues)
  (if clues
      (concat (dashboard-build-clue (car clues))
              (dashboard-build-cluepacket (cdr clues)))
    nil))

(defun dashboard-send-cluepacket (context focused clues)
  (let ((cluestr (concat "<CluePacket>"
			 (concat "<Frontend>"
				 (dashboard-escape-xml-entities dashboard-frontend)
				 "</Frontend>")
			 (concat "<Context>"
				 (dashboard-escape-xml-entities context)
				 "</Context>")
			 (concat "<Focused>" (if focused
						 "True"
					       "False")
				 "</Focused>")
                         (dashboard-build-cluepacket clues)
                         "</CluePacket>\n")))
    (dashboard-send-raw-cluepacket cluestr)))


(defun find-tag-default ()
  (save-excursion
    (while (looking-at "\\sw\\|\\s_")
      (forward-char 1))
    (if (or (re-search-backward "\\sw\\|\\s_"
				(save-excursion (beginning-of-line) (point))
				t)
	    (re-search-forward "\\(\\sw\\|\\s_\\)+"
			       (save-excursion (end-of-line) (point))
			       t))
	(progn (goto-char (match-end 0))
	       (buffer-substring-no-properties
                (point)
                (progn (forward-sexp -1)
                       (while (looking-at "\\s'")
                         (forward-char 1))
                       (point))))
      nil)))

(defun dashboard-build-and-send-cluepacket-from-identifier-at-point ()
  (interactive)
  (setq tag (find-tag-default))
  (dashboard-send-cluepacket (buffer-name) 't `(((type . "identifier")
						 (relevance . 10)
						 (content . ,tag)))))

(global-set-key "\M--" 'dashboard-build-and-send-cluepacket-from-identifier-at-point)

(provide 'dashboard)
