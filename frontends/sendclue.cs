using System;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Net.Sockets;
using System.Text;

	class Driver {
		static void Main (string [] args)
		{

			const int portNumber = 5913;

			TcpClient tcpClient = new TcpClient();

			try {
				tcpClient.Connect ("localhost", portNumber);

				NetworkStream networkStream = tcpClient.GetStream ();

				if (networkStream.CanWrite) {
					string s = "";

					s = "<cluepacket>";

					foreach (string arg in args) {
						s = String.Concat (s, String.Format ("<clue>{0}</clue>", arg));
					}

					s = String.Concat (s, "</cluepacket>");

					Byte[] sendBytes = Encoding.ASCII.GetBytes (s);
					networkStream.Write (sendBytes, 0, sendBytes.Length);
      
				}
				else {
					Console.WriteLine ("You can not read data from this stream");
					tcpClient.Close ();
				}
			}
			catch (Exception e ) {
				Console.WriteLine (e.ToString ());
			}

		}
	}
