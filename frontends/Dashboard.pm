package Dashboard;

use strict;
use Socket;
use IO::Handle;
use Errno;

sub new {
    my $class = shift;

    my $frontend = shift || "Dashboard.pm";
    my $host = shift     || "127.0.0.1";
    my $port = shift     || 5913;

    my $self = {'frontend' => $frontend,
		'host'     => $host,
		'port'     => $port};

    return bless($self, $class);
}

sub _connect {
    my $self   = shift;

    my $iaddr  = inet_aton($self->{host});
    my $paddr  = sockaddr_in($self->{port}, $iaddr);

    my $proto  = getprotobyname('tcp');

    socket(SOCK, PF_INET, SOCK_STREAM, $proto);
    SOCK->blocking(0);

    my $ret = connect(SOCK, $paddr);
    if ($ret or $!{EINPROGRESS}) {
	return *SOCK;
    } else {
	return undef;
    }
}

sub _escape {
    my ($self, $data) = @_;
    $data =~ s/&/&amp;/g;
    $data =~ s/>/&gt;/g;
    $data =~ s/</&lt;/g;
    return $data;
}

sub send_cluepacket {
    my ($self, $context, $focused, $clues) = @_;

    my $sock = $self->_connect();

    if (not defined $sock) {
	return;
    }

    my $fe = $self->_escape($self->{frontend});
    my @cluestrs = ("<CluePacket><Frontend>$fe</Frontend>");

    if (defined $context) {
	push @cluestrs, "<Context>" . $self->_escape($context) . "</Context>";
    }

    if ($focused) {
	push @cluestrs, "<Focused>True</Focused>";
    } else {
	push @cluestrs, "<Focused>False</Focused>";
    }	

    foreach my $clue (@$clues) {
	my $attrs = "";
	foreach my $key ('type', 'relevance') {
	    if (exists $clue->{$key}) {
		my $attr = $self->_escape($clue->{$key});
		$attrs .= " \u$key=\"$attr\"";
	    }
	}

	my $data = $self->_escape($clue->{data});
	push @cluestrs, "<Clue$attrs>$data</Clue>";
    }

    push @cluestrs, "</CluePacket>\n";

    my $str = join "", @cluestrs;
    $sock->print($str);
    $sock->close();
}

1;

__END__

=pod

=head1 NAME

Dashboard - a perl frontend api to the Dashboard

=head1 SYNOPSIS

use Dashboard;

my $dash = Dashboard("Your Application Name");
$dash->send_cluepacket("context", 1, [{'data' => 'test data'}]);

The arguments to send_cluepacket are, in order:
    * context - the application context of the cluepacket
    * focus   - a boolean, indicating whether the application is
                currently focused
    * clues   - the list of clues

That last argument to send_cluepacket is a listref of hashrefs.  Each
hash has a 'data' key, which is the clue contents, and optionally a
'type' key and a 'relevance' key.

The values of 'type' and 'relevance' are described in doc/cluepacket.txt
in the dashboard distribution.

At the moment, type can be one of:
    * date
    * fullname
    * email
    * imname
    * url
    * keyword
    * textblock

relevance is an integer between 1 and 10, inclusive.  10 is the most
relevant a clue can be, 1 is the least.

=head1 DESCRIPTION

Dashboard provides a perl client interface to a Dashboard daemon:
http://nat.org/dashboard/

=head1 AUTHORS

Peter Teichman <peter@ximian.com>

=cut
