#!/usr/bin/perl
# Sends clue packets to the GNOME Dashboard application (see
# http://www.nat.org/dashboard).  Does not do any interpretation
# Of cluepackets, and only sends $msg with type textblock.
# crschmid@uiuc.edu

use Irssi;
use Irssi::Irc;
use Dashboard;
$VERSION = "0.2";
%IRSSI = (
    authors     => 'Christopher Schmidt',
    contact     => 'crschmid@uiuc.edu',
    name        => 'cluepacket',
    description => 'Sends cluepackets to dashboard',
    license     => 'Unlimited',
    url         => 'http://dashboard.crschmidt.net',
);

sub on_public {
    my ($server, $msg, $nick, $addr, $target) = @_;

    $nick = $server->{'nick'} if ($nick =~ /^#/);
    my $dash = new Dashboard("irssi");
    my $window = Irssi::active_win();
    #if ($target eq $window->{'name'}) {
    my $w = $window->{'active'};
    my $name = $w->{'name'};
    if (!$target || $target eq $name) {
    $dash->send_cluepacket(
            "$name",
            1,
            [
                {
                    'data' => $msg,
                    'type' => "textblock",
                    'relevance' => 10,
                },
            ]
        );
    }

}

# FIXME: add ability to turn on/off cluepacket sending.

# Put hooks on events
Irssi::signal_add_last("message public", "on_public");
Irssi::signal_add_last("message own_public", "on_public");

