#!/usr/bin/perl -lw

# Authors: Jamin P. Gray <jamin@pubcrawler.org>
# Purpose: Convenience script to send a cluepacket to Dashboard from 
#          the command line

use Getopt::Long;
use Dashboard;

Getopt::Long::Configure("bundling");

my @clues = ();
GetOptions("frontend|f=s" => \$frontend, 
           "context|x=s"  => \$context, 
           "focused|F"    => \$focused, 
           "clue|c=s"     => \@clues, 
           "help|?"       => \$help);

&print_usage unless (@clues and not defined($help));

$frontend = "send-cluepacket.pl" unless defined $frontend;
$context = "send-cluepacket.pl" unless defined $context;
$focused = 0 unless defined $focused;
my @Clues;

for (@clues) {
  my ($type, $relevance, $text);
  if (/(\w+) (\w+) (.+)/) {
    $type = $1;
    $relevance = $2;
    $text = $3;
  } else {
    &print_usage;
  } 
  push @Clues, 
    {
    'data'      => $text,
    'type'      => $type,
    'relevance' => $relevance
    };
}
my $dash = new Dashboard($frontend);
$dash->send_cluepacket($context, $focused, \@Clues);

sub print_usage {
print qq{
Usage: send-cluepacket.pl [OPTIONS] 
Sends a cluepacket to Dashboard

  -f, --frontend=WORD              frontend is WORD
  -x, --context=WORD               context is WORD
  -F, --focused                    frontend is focused 
  -c, --clue="TYPE RELEVANCE TEXT" can be repeated for a cluepacket with multiple
                                   clues.  TYPE is cluetype.  RELEVANCE is integer 1-10.  

  --help               display this help and exit

Example:
send-cluepacket.pl --frontend myfrontend --context mycontext -F --clue "textblock 10 testing script" --clue "textblock 9 test2"
};
exit 0;
}


