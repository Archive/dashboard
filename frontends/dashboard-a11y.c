
/* 
 * dashboard-a11y:
 *
 * This runs out-of-process and uses the atk-bridge to listen to
 * keyboard events for all applications (that are running with
 * GNOME_ACCESSIBLE=1 or GTK_MODULES=gail:atk-bridge set in the
 * environment).
 *
 * It emits word_at_point, sentence_at_point, line_at_point, and
 * textblock (meaning all the text currently being edited) clues.  
 *
 * These are all generated at the same time after a space is
 * typed (marking end of word, sort of), a punctuation character
 * is received, or text is pasted.
 *
 * TODO: 
 *   - There are probably a million input corner cases that need
 *     to be handled.
 *   - See about making this run in-process as part of frontend 
 *     code, to avoid the atk-bridge lameness (need to set env 
 *     vars).
 */

#include <cspi/spi.h>
#include <glib.h>
#include <stdio.h>

#include <dashboard-frontend.c>

#define DEBUG

static void
emit_keyboard_cluepacket (gchar *word_at_point,
			  gchar *sentence_at_point,
			  gchar *line_at_point,
			  gchar *textblock)
{
	char *cluepacket;

#ifdef DEBUG
	g_print ("EMITTING KEYBOARD CLUEPACKET:\n"
		 "\tword_at_point: %s\n"
		 "\tsentence_at_point: %s\n"
		 "\tline_at_point: %s\n"
		 "\ttextblock: %s\n",
		 word_at_point,
		 sentence_at_point,
		 line_at_point,
		 textblock);
#endif

	cluepacket = 
		dashboard_build_cluepacket_then_free_clues (
			"keyboard",
			TRUE,
			"PLACEHOLDER-CONTEXT-FIXME",
			dashboard_build_clue (word_at_point, 
					      "word_at_point", 
					      100),
			dashboard_build_clue (sentence_at_point, 
					      "sentence_at_point", 
					      80),
			dashboard_build_clue (line_at_point, 
					      "line_at_point", 
					      80),
			dashboard_build_clue (textblock, 
					      "textblock", 
					      40),
			NULL);

	dashboard_send_raw_cluepacket (cluepacket);

	g_free(cluepacket);
}

static void
text_changed_cb (const AccessibleEvent *event,
		 void                  *user_data)
{
	AccessibleText *textobj;
	gchar *changed;
	gunichar curchar;
	long int offset, start_offset, end_offset;
	gchar *current_word;
	gchar *current_sentence;
	gchar *current_line;
	gchar *current_text_body;

#ifdef DEBUG
	g_print ("%s: called for %s\n", __FUNCTION__, event->type);
#endif

	/* don't care if its an insert or delete */
	/*
	if (strcmp (event->type, "object:text-changed:insert") != 0)
		return;
	*/

	textobj = Accessible_getText (event->source);
	if (textobj == NULL)
		return;

	changed = AccessibleTextChangedEvent_getChangeString (event);
	if (changed == NULL)
		return;

	switch (g_utf8_strlen (changed, strlen (changed))) {
	case 0:
		return;
	case 1:
		curchar = g_utf8_get_char (changed);

		/* only generate events after whole words, or end of sentence */
		if (!g_unichar_ispunct (curchar) && 
		    !g_unichar_isspace (curchar))
			return;
	}

	offset = AccessibleText_getCaretOffset (textobj);

	current_word = 
		AccessibleText_getTextAtOffset (
			textobj, 
			offset, 
			SPI_TEXT_BOUNDARY_WORD_START,
			&start_offset, 
			&end_offset);
	current_sentence = 
		AccessibleText_getTextAtOffset (
			textobj, 
			offset, 
			SPI_TEXT_BOUNDARY_SENTENCE_START,
			&start_offset, 
			&end_offset);
	current_line = 
		AccessibleText_getTextAtOffset (
			textobj, 
			offset, 
			SPI_TEXT_BOUNDARY_LINE_START,
			&start_offset, 
			&end_offset);
	current_text_body = 
		AccessibleText_getText (
			textobj, 
			0, 
			-1);

	emit_keyboard_cluepacket (current_word, 
				  current_sentence, 
				  current_line, 
				  current_text_body);
}

int main (int argc, char **argv)
{
	int i;
	AccessibleEventListener *listener;

	SPI_init ();

	listener = SPI_createAccessibleEventListener (text_changed_cb, NULL);
	SPI_registerGlobalEventListener (listener, "object:text-changed");

#ifdef DEBUG
	g_print ("Starting up...\n");
#endif

	SPI_event_main ();

#ifdef DEBUG
	g_print ("Exiting...\n");
#endif

	SPI_deregisterGlobalEventListenerAll (listener);
	AccessibleEventListener_unref (listener);

	return 0;
}
