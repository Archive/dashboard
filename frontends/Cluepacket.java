import java.util.*;
import java.io.*;
import java.net.*;

public class Cluepacket
{
    private static int DBOARD_PORT = 5913;
    
    public Cluepacket()
    {
    }
    
    public String buildClue(String type, String text, String relevance)
    {
        StringBuffer sBuf = new StringBuffer();
        sBuf.append("\t<Clue Type=\"" + type + "\" Relevance=\"" + relevance + "\">");
        sBuf.append(escape(text));
        sBuf.append("</Clue>\n"); 
                
        return sBuf.toString();
    }
    
    public String buildCluePacket(String frontend, String context, 
                                  boolean focused, ArrayList clues)
    {
        StringBuffer sBuf = new StringBuffer();
        sBuf.append("<Cluepacket>\n");
        sBuf.append("\t<Frontend>" + frontend + "</Frontend>\n");
        sBuf.append("\t<Context>" + context + "</Context>\n");
        sBuf.append("\t<Focused>" + focused + "</Focused>\n");
        for (Iterator i = clues.iterator(); i.hasNext();)
        {
            String clue = (String)i.next();
            sBuf.append(clue);
        }
        
        sBuf.append("</Cluepacket>\n");
        
        return sBuf.toString();
    }
    
    public void sendRawCluepacket(String cluepacket) throws IOException
    {
        try
        {
            Socket socket = new Socket("127.0.0.1", DBOARD_PORT);
            
            OutputStream rawOut = socket.getOutputStream();
            InputStream rawIn = socket.getInputStream();
            DataInputStream in = new DataInputStream(rawIn);
            DataOutputStream out = new DataOutputStream(rawOut);
            out.writeBytes(cluepacket);
            out.flush();
            socket.close();
        }
        catch(ConnectException ce)
        {
        }
    }
    
    public String escape(String str) 
    {
        StringBuffer buffer;
        char ch;
        String entity;

        buffer = null;
        for (int i = 0; i < str.length(); i++) 
        {
            ch = str.charAt(i);
            switch(ch) 
            {
                case '<' :
                    entity = "&lt;";
                    break;
                case '>' :
                    entity = "&gt;";
                    break;
                case '&' :
                    entity = "&amp;";
                    break;
                default :
                    entity = null;
                    break;
            }
            if (buffer == null) 
            {
                if (entity != null) 
                {
                    buffer = new StringBuffer(str.length() + 20);
                    buffer.append(str.substring(0, i));
                    buffer.append(entity);
                }
            }
            else 
            {
                if (entity == null) 
                {
                    buffer.append(ch);
                }    
                else 
                {
                    buffer.append(entity);
                }
            }
        }
        return (buffer == null) ? str : buffer.toString();
    }
       
    public static void main(String[] args) throws IOException
    {
        Cluepacket cpack = new Cluepacket();
        ArrayList aList = new ArrayList();
        aList.add(cpack.buildClue("imname", "gnomeboy", "10"));
        aList.add(cpack.buildClue("url", "http://www.gnome.org", "10"));
        String cpacket = cpack.buildCluePacket("Dashboard", "Dashboard",
                                               true, aList);
        cpack.sendRawCluepacket(cpacket);
    }
}

