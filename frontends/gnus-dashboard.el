(add-hook 'gnus-select-article-hook 'jjdg-send-mail-clue)

(defun jjdg-send-mail-clue ()
  (setq components (gnus-extract-address-components(gnus-fetch-original-field
						    "from")))
  (setq name (or (car components) "None"))
  (setq email (car (cdr components)))

  (save-excursion
    (set-buffer gnus-original-article-buffer)
    (save-restriction
      (when (message-goto-body)
	(narrow-to-region (point) (point-max)))
      (setq body (buffer-string))))

  (setq clues `(((type . "full_name")
		 (relevance . 10)
		 (content . ,name))
		((type . "email")
		 (relevance . 10)
		 (content . ,email))
		((type . "textblock")
		 (relevance . 10)
		 (content . ,body))))

  (setq subject (gnus-fetch-original-field "subject"))
  (when subject
    (setq clues (cons `((type . "textblock")
			(relevance . 10)
			(content . ,subject))
		      clues)))

  (setq org (gnus-fetch-original-field "organization"))
  (when org
    (setq clues (cons `((type . "org")
			(relevance . 8)
			(content . ,org))
		      clues)))

  (dashboard-send-cluepacket "context" 't clues ))
