/*
 * Simple X-Chat 2.0 dashboard frontend plugin
 *
 * Please see: http://nat.org/dashboard
 *
 * Author:
 *    Nat Friedman <nat@nat.org>
 *
 * Compile with (on Debian):
 *   gcc -Wall -I/usr/include/xchat -I/usr/include/glib-2.0 \
 *   -I/usr/lib/glib-2.0/include -shared xchat2-plugin.c    \
 *   -o xchat2-dashboard-plugin.so
 *
 */

#include <glib.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include "xchat-plugin.h"

#include "dashboard-frontend.c"

static xchat_plugin *ph;	/* plugin handle */

#define LINES_TO_KEEP 20

typedef struct {
	/* line[0] is the oldest line */
	char     *line[LINES_TO_KEEP];
	int       total_lines;
	gboolean  focused;
} context_t;

context_t *current_context = NULL;
GHashTable *contexts = NULL;

static char *
xchat_build_clue (context_t *context)
{
	char *textblock;
	char *clue;
	int   i;

	textblock = g_strdup (" ");

	for (i = 0; i < context->total_lines; i ++) {
		char *textblock_new;

		textblock_new = g_strconcat (textblock, context->line [i], " ", NULL);
		g_free (textblock);
		textblock = textblock_new;
	}

	clue = dashboard_build_clue (textblock, "textblock", 10);

	g_free (textblock);

	return clue;
}

static void
context_add_line (context_t *context, char *word[])
{
	int i;

	if (context->total_lines < LINES_TO_KEEP) {
		context->line [context->total_lines] = g_strdup (word [2]);
		context->total_lines ++;
		return;
	}

	/* Shift the lines down one */
	g_free (context->line [0]);
	for (i = 1; i < LINES_TO_KEEP; i ++)
		context->line [i - 1] = context->line [i];

	context->line [LINES_TO_KEEP - 1] = g_strdup (word [2]);
}

static context_t *
context_new (gpointer xcontext)
{
	context_t *context;

	context = g_new0 (context_t, 1);
	context->focused = TRUE;

	g_hash_table_insert (contexts, xcontext, context);

	return context;
}

static void
xchat_send_cluepacket (context_t *context)
{
	char *cluepacket;
	char *context_str;

	context_str = g_strdup_printf ("%p", context);

	cluepacket = dashboard_build_cluepacket_then_free_clues (
		"X-Chat",
		context->focused,
		context_str,
		/* FIXME: do chunking */
		xchat_build_clue (context),
		NULL);

	g_free (context_str);

	dashboard_send_raw_cluepacket (cluepacket);
}

static int
text_cb (char *word[], void *userdata)
{
	gpointer   xcontext;
	context_t *context;
	char       string[32768];

	xcontext = xchat_get_context (ph);
	if (xcontext == NULL)
		return 1;

	context = g_hash_table_lookup (contexts, xcontext);
	if (context == NULL)
		xchat_print (ph, "Dashboard plugin: Could not find context!");
		

	context_add_line (context, word);

	if (context->focused)
		xchat_send_cluepacket (context);

	snprintf (string, sizeof (string), "DASHBOARD: [%s] [%s] [%s] [%s]",
		  word[0], word[1], word[2], word[3]);

	xchat_print (ph, string);

	return XCHAT_EAT_NONE;
}

int
xchat_plugin_deinit (void)
{
	xchat_print (ph, "Dashboard plugin shutting down\n");

	return 1;
}

static int
new_context_cb (char *word[], void *userdata)
{
	context_t *context;
	gpointer   xcontext;

	xcontext = xchat_find_context (ph, NULL, NULL);
	if (xcontext == NULL)
		return 1;

	context = context_new (xcontext);

	current_context = context;

	return 1;
}

static int
kill_context_cb (char *word[], void *userdata)
{
	/* FIXME: leak */

	return 1;
}

static int
focus_tab_cb (char *word[], void *userdata)
{
	context_t *context;
	gpointer   xcontext;

	xcontext = xchat_get_context (ph);
	if (xcontext == NULL)
		return 1;

	context = (context_t *) g_hash_table_lookup (contexts, xcontext);
	if (context == NULL)
		context = context_new (xcontext);

	context->focused = TRUE;

	xchat_send_cluepacket (context);

	if (current_context)
		current_context->focused = FALSE;

	current_context = context;

	return 1;
}

static int
focus_window_cb (char *word[], void *userdata)
{
	if (! current_context)
		return 1;

	current_context->focused = ! current_context->focused;

	return 1;
}

int
xchat_plugin_init (xchat_plugin  *plugin_handle,
		   char         **plugin_name,
		   char         **plugin_desc,
		   char         **plugin_version,
		   char          *arg)
{
	ph = plugin_handle;

	*plugin_name = "Dashboard";
	*plugin_desc = "Dashboard frontend plugin";
	*plugin_version = "0.0";

	xchat_hook_print (ph, "Channel Message",         XCHAT_PRI_NORM, text_cb, 0);
	xchat_hook_print (ph, "Channel Message Hilight", XCHAT_PRI_NORM, text_cb, 0);
	xchat_hook_print (ph, "Private Message",         XCHAT_PRI_NORM, text_cb, 0);
	xchat_hook_print (ph, "Private Message to",      XCHAT_PRI_NORM, text_cb, 0);
	xchat_hook_print (ph, "Message Send",            XCHAT_PRI_NORM, text_cb, 0);
	xchat_hook_print (ph, "Your Message",            XCHAT_PRI_NORM, text_cb, 0);

	xchat_hook_print (ph, "Open Context",            XCHAT_PRI_NORM, new_context_cb, 0);
	xchat_hook_print (ph, "Close Context",           XCHAT_PRI_NORM, kill_context_cb, 0);
	xchat_hook_print (ph, "Focus Tab",               XCHAT_PRI_NORM, focus_tab_cb, 0);
	xchat_hook_print (ph, "Focus Window",            XCHAT_PRI_NORM, focus_window_cb, 0);

	contexts = g_hash_table_new (g_direct_hash, g_direct_equal);

	xchat_print (ph, "Dashboard plugin loaded.\n");

	return 1;
}
