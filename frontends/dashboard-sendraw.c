#include <glib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdio.h>

#include <dashboard-frontend.c>

int
main (int argc, char **argv)
{
	char *rawcluepacket = NULL;
	while (!feof (stdin)) {
		char *newpacket;
		char newline[1024];

		if (fgets (newline, sizeof (newline), stdin)) {
			newpacket = g_strdup_printf ("%s%s", rawcluepacket ? rawcluepacket : " ", newline);
			g_free (rawcluepacket);
			rawcluepacket = newpacket;
		} else
			break;
	}

	dashboard_send_raw_cluepacket_sync (rawcluepacket);

	return 0;
}
