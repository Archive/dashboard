# Simple Pork script to send dashboard information
# Usage: Place in a directory, and after pork has started, type 
#        /perl_load /location/of/file
# Requirements: Needs the Dashboard perl package, distributed with Dashboard
# Features: Sends aim_name clues based on aim name, as well as textblock clues
#           of messages, both sent and recieved.
# Limitations: There is no way to determine focus from the perl hooks in
#              pork. As such, all conversations send cluepackets for aim
#              name and text. A patch to the actual pork code will likely be
#              needed to add focus capability to dashboard packets.
# Written by Christopher Schmidt (crschmid@uiuc.edu), last modified 03-29-04

use Dashboard;

sub setup {
    PORK::event_add("RECV_IM", "recv_im_handler");
    PORK::event_add("SEND_IM", "send_im_handler");
    return (0);
}

sub recv_im_handler {
    my ($buddy, $dummy, $msg, $account) = @_;
    my $dash = new Dashboard("Pork");
    $dash->send_cluepacket(
        "pork conversation",
        1,
        [
            {
                'data' => $msg,
                'type' => "textblock",
                'relevance' => 5,
            },
            {
                'data' => $buddy,
                'type' => "aim_name",
                'relevance' => 10,
            }
        ]
    );

    return(0);
}

sub send_im_handler {
    my ($buddy, $msg, $account) = @_;

    my $dash = new Dashboard("Pork");
    $dash->send_cluepacket(
        "pork conversation",
        1,
        [
            {
                'data' => $msg,
                'type' => "textblock",
                'relevance' => 5,
            },
        ]
    );
    return(0);
}

setup();
