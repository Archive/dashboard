
/* 
 * libdashboard-input.so:
 *
 * This GTK module runs in-proc and transmits cluepackets for
 * user input. It does not use the atk-bridge or libspi/cspi.
 * To activate, set
 * GTK_MODULES="gail:/path/to/libdashboard-input.so" 
 * in the environment.
 *
 * It emits word_at_point, sentence_at_point, line_at_point, and
 * textblock (meaning all the text currently being edited) clues.  
 *
 * These are all generated at the same time after a space is
 * typed (marking end of word, sort of), a punctuation character
 * is received, or text is pasted.
 *
 * TODO: 
 *   - There are probably a million input corner cases that need
 *     to be handled.
 */


#include <atk/atk.h>
#include <stdio.h>

#include "dashboard-frontend.c"

/* #define ENABLE_CARET_MOVED */

typedef enum {
	WORD     = 1 << 0,
	SENTENCE = 1 << 1, 
	LINE     = 1 << 2,
	BODY     = 1 << 3
} BoundryType;

static gchar *
build_clue_from_text_boundry (AtkText    *textobj, 
			      gint        offset,
			      BoundryType boundry)
{
	gchar *type, *txt, *clue;
	gint relevence, atkbound;

	switch (boundry) {
	case WORD:
		atkbound = ATK_TEXT_BOUNDARY_WORD_START;
		type = "word_at_point";
		relevence = 100;
		break;
	case SENTENCE:
		atkbound = ATK_TEXT_BOUNDARY_SENTENCE_START;
		type = "sentence_at_point";
		relevence = 80;
		break;
	case LINE:
		atkbound = ATK_TEXT_BOUNDARY_LINE_START;
		type = "line_at_point";
		relevence = 80;
		break;
	case BODY:
		txt = atk_text_get_text (textobj, 0, -1);
		type = "textblock";
		relevence = 40;
		break;
	}

	if (boundry != BODY)
		txt = atk_text_get_text_at_offset (textobj, 
						   offset, 
						   atkbound,
						   NULL,
						   NULL);

	clue = dashboard_build_clue (txt, type, relevence);
	g_free (txt);

	return clue;
}

static const gchar *
build_context_from_label (AtkText *textobj)
{
	AtkRelationSet *relset;
	AtkRelation    *labelrel;
	GPtrArray      *targetarr;
	AtkObject      *labelobj;
	int             i;
	const gchar    *name;
	const gchar    *retctx = "UNKNOWN"; 

	/* first get the object name */
	name = atk_object_get_name (ATK_OBJECT (textobj));
	if (name)
		return name;

	/* then use any labelled-by relation's names */
	relset = atk_object_ref_relation_set (ATK_OBJECT (textobj));
	if (!relset) 
		return retctx;

	labelrel = 
		atk_relation_set_get_relation_by_type (
			relset,
			ATK_RELATION_LABELLED_BY);
	if (labelrel) {
		targetarr = atk_relation_get_target (labelrel);
		for (i = 0; i < targetarr->len; i++) {
			labelobj = g_ptr_array_index (targetarr, i);
			if (!labelobj)
				continue;

			name = atk_object_get_name (labelobj);
			if (name) {
				retctx = name;
				break;
			}
		}
	}

 FINISH:
	g_object_unref (relset);
	return retctx;
}

static gchar *
build_text_context_cluepacket (AtkText *textobj, 
			       gint     offset,
			       gint     bounds)
{
	char *cluepacket;
	GList *clues = NULL, *iter;

	if (bounds & WORD)
		clues = g_list_prepend (clues, 
					build_clue_from_text_boundry (textobj, 
								      offset,
								      WORD));
	if (bounds & SENTENCE)
		clues = g_list_prepend (clues,
				        build_clue_from_text_boundry (textobj, 
								      offset,
								      SENTENCE));
	if (bounds & LINE)
		clues = g_list_prepend (clues,
					build_clue_from_text_boundry (textobj, 
								      offset,
								      LINE));
	if (bounds & BODY)
		clues = g_list_prepend (clues,
					build_clue_from_text_boundry (textobj, 
								      offset,
								      BODY));

	cluepacket = 
		dashboard_build_cluepacket_from_cluelist (
			"keyboard",
			TRUE,
			build_context_from_label (textobj),
			clues);

	for (iter = clues; iter; iter = iter->next) {
		g_free (iter->data);
	}
	g_list_free (clues);

	return cluepacket;
}

static gboolean
should_send_clues_for_object (AtkObject *obj)
{
	AtkStateSet *stateset;
	gboolean visible_and_editable, password_text;
	
	/* Check object is visible and focused, to avoid sending
	 * events for apps that fill text boxes without the user
	 * typing anything (like bug-buddy).
	 */
	stateset = atk_object_ref_state_set (obj);
	visible_and_editable = 
		atk_state_set_contains_state (stateset, ATK_STATE_VISIBLE) &&
		atk_state_set_contains_state (stateset, ATK_STATE_FOCUSED) &&
		atk_state_set_contains_state (stateset, ATK_STATE_EDITABLE);
	g_object_unref (stateset);

	password_text = (atk_object_get_role (obj) == ATK_ROLE_PASSWORD_TEXT);

	return visible_and_editable && !password_text;
}

static gboolean
send_cluepacket (gpointer user_data)
{
	char **pcluepacket = user_data;

	dashboard_send_raw_cluepacket (*pcluepacket);
	g_print ("Dashboard: Sending keyboard input cluepacket...\n%s\n",
		 *pcluepacket);

	g_free (*pcluepacket);
	*pcluepacket = NULL;

	return FALSE;
}

static void
set_cluepacket_send_timeout (char **pcluepacket, 
			     char  *new_cluepacket,
			     guint *pclueid)
{
	g_free (*pcluepacket);
	*pcluepacket = new_cluepacket;

	if (*pclueid)
		g_source_remove (*pclueid);
	*pclueid = g_timeout_add (500, send_cluepacket, pcluepacket);
}

#if ENABLE_CARET_MOVED
/* 
 * caret_moved Disabled until we can avoid the duplicate clue
 * sending on keypress.
 */
static gboolean
atk_caret_moved_listener (GSignalInvocationHint *signal_hint,
			  guint                  n_param_values,
			  const GValue          *param_values,
			  gpointer               data)
{
	static char *timeout_cluepacket = NULL;
	static guint timeout_id = 0;

	AtkText *textobj;
	int offset;
	char *cluepacket;

	g_print ("in %s\n", __FUNCTION__);

	textobj = ATK_TEXT (g_value_get_object (param_values + 0));
	if (!textobj)
		return TRUE;

	if (!should_send_clues_for_object (ATK_OBJECT (textobj)))
		return TRUE;

	offset = g_value_get_int (param_values + 1);

	cluepacket = build_text_context_cluepacket (textobj, 
						    offset, 
						    (SENTENCE | LINE));
	set_cluepacket_send_timeout (&timeout_cluepacket,
				     cluepacket,
				     &timeout_id);

	return TRUE;
}
#endif

static gboolean
atk_text_changed_listener (GSignalInvocationHint *signal_hint,
			   guint                  n_param_values,
			   const GValue          *param_values,
			   gpointer               data)
{
	static char *timeout_cluepacket = NULL;
	static guint timeout_id = 0;

	AtkText *textobj;
	gint start = 0, changed_len = 0;
	gunichar curchar = 0;
	gboolean send_body = TRUE;
	char *cluepacket;

	g_print ("in %s\n", __FUNCTION__);

	textobj = ATK_TEXT (g_value_get_object (param_values + 0));
	if (!textobj)
		return TRUE;

	if (!should_send_clues_for_object (ATK_OBJECT (textobj)))
		return TRUE;

	start = g_value_get_int (param_values + 1);
	changed_len = g_value_get_int (param_values + 2);

	if (changed_len == 1) {
		curchar = atk_text_get_character_at_offset (textobj, start);
		g_print ("*** curchar == %c (%d)\n", curchar, curchar);

		/* ignore spaces */
		if (g_unichar_isspace (curchar))
			return TRUE;

		/* 
		 * send the body only if this is a punctuation
		 * of some sort, is there a unicode-compliant way
		 * of specifying end-of-sentence?
		 */
		if (!g_unichar_ispunct (curchar))
			send_body = FALSE; 
	}

	cluepacket = 
		build_text_context_cluepacket (
			textobj, 
			start + changed_len, 
			(WORD | SENTENCE | LINE | (send_body ? BODY : 0)));
	set_cluepacket_send_timeout (&timeout_cluepacket,
				     cluepacket,
				     &timeout_id);

	return TRUE;
}

static gboolean
atk_selection_changed_listener (GSignalInvocationHint *signal_hint,
				guint                  n_param_values,
				const GValue          *param_values,
				gpointer               data)
{
	static char *timeout_cluepacket = NULL;
	static guint timeout_id = 0;

	GObject *gobject;
	AtkText *textobj;
	int      n_sels, i;
	char    *sel_text, *sel_clue, *cluepacket;
	GList   *clues = NULL, *iter;

	gobject = g_value_get_object (param_values + 0);
	textobj = ATK_TEXT (gobject);

	n_sels = atk_text_get_n_selections (textobj);

	for (i = 0; i < n_sels; i++) {
		sel_text = atk_text_get_selection (textobj, i, NULL, NULL);
		if (sel_text) {
			sel_clue = dashboard_build_clue (sel_text, 
							 "text_selection", 
							 100);
			clues = g_list_prepend (clues, sel_clue);
			g_free (sel_text);
		}
	}

	if (clues) {
		cluepacket = 
			dashboard_build_cluepacket_from_cluelist (
				"keyboard",
				TRUE,
				build_context_from_label (textobj),
				clues);
		set_cluepacket_send_timeout (&timeout_cluepacket,
					     cluepacket,
					     &timeout_id);

		for (iter = clues; iter; iter = iter->next) {
			g_free (iter->data);
		}
		g_list_free (clues);
	}

	return TRUE;
}

static void
atk_focus_tracker_callback (AtkObject *object)
{
	/* Do nothing */
}

static guint atk_focus_tracker_id = 0;
static guint atk_caret_moved_id = 0;
static guint atk_text_changed_id = 0;
static guint atk_selection_changed_id = 0;

static void
register_signal_listeners (void)
{
	/* Make sure the atk types are loaded, so we can lookup the signals */
	GObject   *ao = g_object_new (ATK_TYPE_OBJECT, NULL);
	AtkObject *bo = atk_no_op_object_new (ao);

	/* Setup a focus tracker, as events never get generated without one */
	atk_focus_tracker_id = 
		atk_add_focus_tracker (atk_focus_tracker_callback);

	/* Register for the events we actually care about */
#if ENABLE_CARET_MOVED
	atk_caret_moved_id = 
		atk_add_global_event_listener (
			atk_caret_moved_listener, 
			"Gtk:AtkText:text-caret-moved");
#endif
	atk_text_changed_id = 
		atk_add_global_event_listener (
			atk_text_changed_listener, 
			"Gtk:AtkText:text-changed");
	atk_selection_changed_id =
		atk_add_global_event_listener (
			atk_selection_changed_listener, 
			"Gtk:AtkText:text-selection-changed");

	g_object_unref (G_OBJECT (bo));
	g_object_unref (ao);
}

static void
deregister_signal_listeners (void)
{
	atk_remove_focus_tracker (atk_focus_tracker_id);
#if ENABLE_CARET_MOVED
	atk_remove_global_event_listener (atk_caret_moved_id);
#endif
	atk_remove_global_event_listener (atk_text_changed_id);
	atk_remove_global_event_listener (atk_selection_changed_id);
}

static gint toplevels = 0;

static void
atk_toplevel_added (AtkObject *object,
		    guint     index,
		    AtkObject *child)
{
	if (toplevels == 0) {
		g_print ("Initing toplevel!\n");
		register_signal_listeners ();
	}

	toplevels++;
}

static void
atk_toplevel_removed (AtkObject *object,
		      guint     index,
		      AtkObject *child)
{
	toplevels--;
	if (toplevels == 0) {
		deregister_signal_listeners ();
	}
	if (toplevels < 0) {
		g_warning ("More toplevels removed than added\n");
		toplevels = 0;
	}
}

static gboolean dashboard_gtk_initialized = FALSE;

int
gtk_module_init (gint *argc, gchar **argv[])
{
	if (dashboard_gtk_initialized)
		return 0;
	dashboard_gtk_initialized = TRUE;

	if (!bonobo_init (argc, argv ? *argv : NULL)) {
		g_error ("Could not initialize Bonobo");
	}

	if (bonobo_activation_iid_get ()) {
		g_signal_connect (atk_get_root (),
				  "children-changed::add",
				  (GCallback) atk_toplevel_added,
				  NULL);
		g_signal_connect (atk_get_root (),
				  "children-changed::remove",
				  (GCallback) atk_toplevel_removed,
				  NULL);
	} else {
		g_print ("Dashboard GTK Plugin initialized\n");
		register_signal_listeners ();
	}

        return 0;
}

void
gnome_accessibility_module_init (void)
{
	g_print ("Dashboard Gnome Plugin initialized\n");
	register_signal_listeners ();
}

void
gnome_accessibility_module_shutdown (void)
{
	g_print ("Dashboard Gnome Plugin initialized\n");
	deregister_signal_listeners ();
}
