"""\
This is a python frontend to the Dashboard.  Example usage:

from dashboard import Dashboard
dash = Dashboard('Your Application Name')

dash.send_cluepacket("context", 1, [{'data' : 'test data'}])

The arguments to send_cluepacket are, in order:
    * context - the application context of the cluepacket
    * focus   - a boolean, indicating whether the application is
                currently focused
    * clues   - the list of clues

That last argument to send_cluepacket is a list of dictionaries.  Each
dictionary has a 'data' key, which is the clue contents, and
optionally a 'type' key and a 'relevance' key.

The values of 'type' and 'relevance' are described in doc/cluepacket.txt
in the dashboard distribution.

At the moment, type can be one of:
    * date
    * fullname
    * email
    * imname
    * url
    * keyword
    * textblock

See the dashboard cluepacket documentation for an up-to-date list of
the valid clue types:

    http://cvs.gnome.org/lxr/source/dashboard/doc/cluepacket.txt

relevance is an integer between 1 and 10, inclusive.  10 is the most
relevant a clue can be, 1 is the least.
"""

class Dashboard:
    def __init__(self, frontend='dashboard.py', host='localhost',
                 port=5913, debug=0):
        self.frontend = frontend
        self.host     = host
        self.port     = port
        self.debug    = debug

    def _connect(self):
        import socket, errno
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setblocking(0)

        try:
            sock.connect((self.host, self.port))
        except socket.error, e:
            if e.args[0] == errno.EINPROGRESS:
                return sock
            else:
                raise

    def _escape(self, data):
        data = data.replace("&", "&amp;")
        data = data.replace("<", "&lt;")
        data = data.replace(">", "&gt;")
        return data

    def send_cluepacket(self, context, focused, clues):
        try:
            sock = self._connect()
    
            cluestrs = ['<CluePacket>',
                        '<Frontend>%s</Frontend>' % self._escape(self.frontend),
                        '<Context>%s</Context>' % self._escape(self.frontend)
                        ]

            if focused:
                cluestrs.append('<Focused>True</Focused>')
            else:
                cluestrs.append('<Focused>False</Focused>')
            
            for clue in clues:
                attrs = ''
                for key in ['type', 'relevance']:
                    if clue.has_key(key):
                        attrs += ' %s="%s"' % \
                                 (key.capitalize(),
                                  self._escape(clue[key].__str__()))
    
                cluestrs.append('<Clue%s>%s</Clue>' \
                                % (attrs, self._escape(clue['data'])))
    
            cluestrs.append('</CluePacket>\n')
    
            import string
            str = string.join(cluestrs, '')

            sock.send(str)
            sock.close()
        except:
            if self.debug:
                raise

if __name__ == "__main__":
    dashboard = Dashboard("dashboard.py", debug=1)

    cluepacket = [{'data' : 'gimp'},
                  {'type' : 'date',
                   'data' : '2003-03-26'}]

    dashboard.send_cluepacket(cluepacket)
