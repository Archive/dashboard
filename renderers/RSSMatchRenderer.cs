//
// GNOME Dashboard
//
// RSSMatchRenderer.cs: Knows how to render RSS matches.
//
// Author:
//   Kevin Godby <godbyk@yahoo.com>
//

using System;
using System.Collections;
using System.Xml;
using System.IO;

[assembly:Dashboard.MatchRendererFactory ("Dashboard.RSSMatchRenderer")]

namespace Dashboard {

	class RSSMatchRenderer : MatchRenderer {

		public int MaxEntries = -1;	// The max number of entries to show (per feed)
		public int MatchNumber = 0;	// This is used later on..

		public override void Startup ()
		{
			Type = "RSS";
		}

		public override string HTMLRenderMatches (ArrayList matches)
		{
			DateTime StartExec = DateTime.Now;

			StringWriter sw = new StringWriter ();
			XmlWriter xw = new XmlTextWriter (sw);

			xw.WriteStartElement ("div");	// Start the rss results block
			// xw.WriteElementString ("u", "RSS Feed");	// Print the title
			// xw.WriteStartElement ("br");
			// xw.WriteEndElement ();	// br

			ArrayList Titles = new ArrayList ();
	
			// Get all the unique rss feed titles
			foreach (Match m in matches) {
				string Title = Convert.ToString (m ["Title"]);

				if (!Titles.Contains (Title))
					Titles.Add (Title);
			}

			// Let's display some output already!
			foreach (string title in Titles)
				HTMLRenderSingleRSSTitle (title, matches, xw);

			xw.WriteEndElement ();	// End rss results block

			xw.Close ();

			Console.WriteLine ("..Renderer: RSS.. elapsed time {0}", DateTime.Now - StartExec);

			return sw.ToString ();
		}

		private void HTMLRenderSingleRSSTitle (string Title, ArrayList matches, XmlWriter xw)
		{
			string Icon = "internal:rss.png";

			// xw.WriteStartElement ("img");	// rss icon
			// xw.WriteAttributeString ("src", Icon);

			xw.WriteStartElement ("font");	// Make the font smaller to fit window width
			xw.WriteAttributeString ("size", "+0");

			xw.WriteElementString ("u", " Weblog: " + Title);
			xw.WriteStartElement ("br");
			xw.WriteEndElement ();	// br

			xw.WriteStartElement ("table");
			xw.WriteAttributeString ("border", "0");
			xw.WriteAttributeString ("cellpadding", "0");
			xw.WriteAttributeString ("cellspacing", "0");

			MatchNumber = 0;
			int NumEntries = 0;
			bool color_band = true;
			foreach (Match m in matches) {
				NumEntries++;
				if ((NumEntries <= MaxEntries) || (MaxEntries == 0) || (MaxEntries == -1))
					HTMLRenderSingleRSSItem (m, xw, color_band);
				color_band = ! color_band;
			}

			xw.WriteEndElement ();	// table

			xw.WriteEndElement ();	// font

		}

		private void HTMLRenderSingleRSSItem (Match m, XmlWriter xw, bool color_band)
		{
			string Title       = Convert.ToString (m ["Title"]);
			string Description = Convert.ToString (m ["Description"]);
			string ItemTitle   = Convert.ToString (m ["ItemTitle"]);
			string Weblink     = Convert.ToString (m ["Weblink"]);
			string GUID        = Convert.ToString (m ["GUID"]);
			DateTime PubDate   = DateTime.Parse (Convert.ToString (m ["PubDate"]));
			string Icon        = "internal:rss.png";

			// Console.WriteLine ("..Renderer: RSS.. PubDate = {0}", PubDate);

			// Grab first sentence from Description
			string BareDescription = StripHTML (Description);
			string Sentence = BareDescription.Substring (0, BareDescription.IndexOf (".") + 1) + " . .";

			// Grab first few words...
			string [] words = BareDescription.Split (' ');
			if (words.Length > 20) {
				string Blurb = String.Join (" ", words, 0, 20);
				Sentence = Blurb + " . . .";
			} else
				Sentence = BareDescription;

			xw.WriteStartElement ("tr");
			xw.WriteStartElement ("td");
			if (color_band)
				xw.WriteAttributeString ("bgcolor", "#f6f2f6");

			// xw.WriteString ("- ");
			xw.WriteStartElement ("a");
			xw.WriteAttributeString ("href", Weblink);
			xw.WriteAttributeString ("style", "text-decoration: none;");
			xw.WriteString (ItemTitle);
			xw.WriteEndElement ();	// a href

			// If it's an entry from today, or the most recent entry show a short intro
			if ((PubDate == DateTime.Today) || (MatchNumber == 0)) {
				xw.WriteStartElement ("br");
				xw.WriteEndElement ();	// br

				xw.WriteStartElement ("font");
				xw.WriteAttributeString ("size", "-1");
				xw.WriteAttributeString ("color", "#666666");
				xw.WriteRaw (Sentence);
				xw.WriteEndElement ();	// font
			}

			// xw.WriteStartElement ("br");
			// xw.WriteEndElement ();	// br

			xw.WriteEndElement ();	// td
			xw.WriteEndElement ();	// tr

			MatchNumber++;
		}

		private string StripHTML (string myhtml) {
			// I'm sure there's a better regex or builtin function for this..
			System.Text.RegularExpressions.Regex tags = new System.Text.RegularExpressions.Regex(@"<[^>]+>|</[^>]+>");
			return tags.Replace(myhtml, "");
		}
	}
}
