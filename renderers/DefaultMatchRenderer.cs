//
// GNOME Dashboard
//
// DefaultMatchRenderer.cs: The vanilla renderer for match types with
// no type-specific renderer to call their own.  Cold, lonely match
// types.
//
// Author:
//   Nat Friedman <nat@nat.org>
//

using System;
using System.Collections;

[assembly:Dashboard.MatchRendererFactory ("Dashboard.DefaultMatchRenderer")]

namespace Dashboard {

	class DefaultMatchRenderer : MatchRenderer {

		public override void Startup ()
		{
			Type = "Default";
		}

		public override string HTMLRenderMatches (ArrayList matches)
		{
			string html = "";

			foreach (Match m in matches)
				html += HTMLRenderSingleMatch (m);

			return html;
		}

		private string HTMLRenderSingleMatch (Match m)
		{
			if (m ["Icon"] == null && m ["Text"] == null)
				return "";

			string html;

			html = String.Format (
					      "<table border=0 cellpadding=0 cellspacing=0>" +
					      "<tr>");

			if (m ["Icon"] != null)
				html += String.Format (
						       "    <td valign=center>" +
						       + "        <a href=\"{0}\"><img src=\"{1}\" border=0></a>" +
						       "    </td>",
						       m ["Action"],
						       m ["Icon"]);

			html += String.Format ("<td>&nbsp;&nbsp;</td>" +
					       "    <td valign=top>" +
					       "        <a href=\"{0}\" style=\"text-decoration: none;\">{1}" +
					       "    </td>" +
					       "</tr>" +
					       "</table>",
					       m ["Action"],
					       m ["Text"]);

			return html;
		}
	}
}
