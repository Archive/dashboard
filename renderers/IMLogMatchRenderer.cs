//
// GNOME Dashboard
//
// IMLogMatchRenderer.cs: Knows how to render IM conversation logs.
//
// Author:
//   Nat Friedman <nat@nat.org>
//

using System;
using System.Collections;

[assembly:Dashboard.MatchRendererFactory ("Dashboard.IMLogMatchRenderer")]

namespace Dashboard {

	class IMLogMatchRenderer : MatchRenderer {

		public override void Startup ()
		{
			Type = "IMLog";
		}

		private class AliasComparer : IComparer {
			public int Compare (object a, object b)
			{
				Match m1 = (Match) a;
				Match m2 = (Match) b;

				return String.Compare ((string) m1 ["Alias"], (string) m2 ["Alias"]);
			}
		}

		public override string HTMLRenderMatches (ArrayList matches)
		{
			string html = "";
			string current_alias = null;

			// Sort by alias
			matches.Sort (new AliasComparer ());

			foreach (Match m in matches) {
				string alias = (string) m ["Alias"];

				if (current_alias != null && current_alias != alias)
					html += "</table>\n\n"; // Footer

				if (current_alias != alias)
					html += HTMLRenderHeader (m);

				html += HTMLRenderSingleIMConv (m);

				current_alias = alias;
			}

			html += "</table>\n\n"; // Footer

			return html;
		}

		private string HTMLRenderHeader (Match m)
		{
			string html;

			html = String.Format ("<table border=0 width=100%>"                             +
					      "    <tr bgcolor=#ecd953>"                                +
					      "        <td> <img border=0 src=\"{0}\"> </td>"           +
					      "        <td valign=center>"                              +
					      "            <font size=+1>Conversations with {1}</font>" +
					      "        </td>"                                           +
					      "    </tr>",
					      m ["Icon"],
					      m ["Alias"]);

			return html;
		}

		private string HTMLRenderSingleIMConv (Match m)
		{
			string html;

			html = String.Format ("<tr bgcolor=#f7ec93>"                                                    +
					      "    <td colspan=2> <font size=-1>"                                       +
					      "        <a style=\"text-decoration: none;\" href=\"{1}\">{0}</a>"        +
					      "    </font></td>"                                                        +
					      "</tr>",
					      m ["Date"],
					      m ["URL"]);

			return html;
		}
	}
}
