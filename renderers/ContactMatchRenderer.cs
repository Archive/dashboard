//
// GNOME Dashboard
//
// ContactMatchRenderer.cs
//
//

using System;
using System.Collections;

[assembly:Dashboard.MatchRendererFactory ("Dashboard.ContactMatchRenderer")]

namespace Dashboard {

	class ContactMatchRenderer : MatchRenderer {

		public override void Startup ()
		{
			Type = "Contact";
		}

		public override string HTMLRenderMatches (ArrayList matches)
		{
			string html = HTMLRenderHeader ();

			foreach (Match m in matches)
				html += HTMLRenderContact (m);

			html += "</table>";

			return html;
		}

		private string HTMLRenderHeader ()
		{
			string html;

			html = "<table border=0 width=100%>"                             +
				"    <tr bgcolor=\"#fffa6e\">"                                +
				"        <td valign=center colspan=2>"               +
				"            <font size=\"+2\">People</font>" +
				"        </td>"                                           +
				"    </tr>";
			return html;
		}

		private string ContactRow (Match m, string key, string name)
		{
			if (m [key] == null)
				return "";
			return String.Format ("<tr><td align=\"right\">{0}:</th><td>{1}</td></tr>",
					      name, m [key]);
		}

		private string HTMLRenderContact (Match m)
		{
			string html = "<table>";

			if (m ["Name"] == null)
				return "";

			if (m ["Photo"] != null) {
				MatchRenderer.AddImageHack ((string) m ["Name"], (byte[]) m ["Photo"]);
				html += "<tr><td colspan=2>";
				html += String.Format ("<img width=200 src=\"imagehack:{0}\">", m ["Name"]);
				html += "</td></tr>";
			}

			html += ContactRow (m, "Name", "Name");
			html += ContactRow (m, "Email", "Email");
			html += ContactRow (m, "HomePhone", "Phone");
			html += ContactRow (m, "MobilePhone", "Mobile");

			html += "</table>";

			return html;
		}
	}
}
