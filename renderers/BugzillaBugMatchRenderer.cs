//
// GNOME Dashboard
//
// BugzillaBugMatchRenderer.cs: Knows how to render BugzillaBug matches.
//
// Author:
//   Kevin Godby <godbyk@yahoo.com>
//   Lukas Lipka <lukas@pmad.net>
//

using System;
using System.Collections;
using System.Xml;
using System.IO;

[assembly:Dashboard.MatchRendererFactory ("Dashboard.BugzillaBugMatchRenderer")]

namespace Dashboard {

	class BugzillaBugMatchRenderer : MatchRenderer {

		public override void Startup ()
		{
			Type = "BugzillaBug";
		}

		public override string HTMLRenderMatches (ArrayList matches)
		{
			StringWriter sw = new StringWriter ();
			XmlWriter xw = new XmlTextWriter (sw);

			xw.WriteStartElement ("div");	// Start the bugzilla results block
			xw.WriteElementString ("u", "Bugzilla Bugs");	// Print the title

			xw.WriteStartElement ("table");	// Start the results table
			xw.WriteAttributeString ("width", "100%");
			xw.WriteAttributeString ("border", "0");
			xw.WriteAttributeString ("cellpadding", "0");
			xw.WriteAttributeString ("cellspacing", "0");
			
			foreach (Match m in matches)
				HTMLRenderSingleBugzillaBug (m, xw);

			xw.WriteEndElement ();	// End results table
			xw.WriteEndElement ();	// End bugzilla results block

			xw.Close ();

			return sw.ToString ();
		}

		private void HTMLRenderSingleBugzillaBug (Match m, XmlWriter xw)
		{
			string Number  = Convert.ToString (m ["Number"]);
			string Product = Convert.ToString (m ["Product"]);
			string Owner   = Convert.ToString (m ["Owner"]);
			string Email   = Convert.ToString (m ["Owner"]);
			string Summary = Convert.ToString (m ["Summary"]);
			string Status  = Convert.ToString (m ["Status"]);
			string Icon    = "internal:bug.png";

			// Trim the sender name
			if (Owner.IndexOf ("@") != -1)
				Owner = Owner.Substring (0, (Owner.LastIndexOf ("@") - 1));

			xw.WriteStartElement ("tr");
			xw.WriteStartElement ("td");
			xw.WriteAttributeString ("valign", "top");

			xw.WriteStartElement ("img");	// Bug icon
			xw.WriteAttributeString ("src", Icon);
			xw.WriteEndElement ();	// img
			xw.WriteEndElement ();	// td

			xw.WriteStartElement ("td");
			xw.WriteStartElement ("font");	// Make the font smaller to fit window width
			xw.WriteAttributeString ("size", "-2");

			xw.WriteString ("#" + Number + ": " + Summary);
			xw.WriteStartElement ("br");
			xw.WriteEndElement ();	// br

			xw.WriteString (Product + " ");

			xw.WriteStartElement ("a");
			xw.WriteAttributeString ("href", "mailto:" + Email);
			xw.WriteString (Owner);
			xw.WriteEndElement ();	// a href

			xw.WriteStartElement ("font");
			xw.WriteAttributeString ("color", GetStatusColor (Status));
			xw.WriteString (Status);
			xw.WriteEndElement ();	// font

			xw.WriteEndElement ();	// font
			xw.WriteEndElement ();	// td
			xw.WriteEndElement ();	// tr

		}
		
		private string GetStatusColor (string status)
		{
			switch (status) {

			case "RESOLVED":
				return "#00dd00";

			case "NEEDINFO":
				return "#cad505";

			case "ASSIGNED":
				return "#1438e8";

			case "VERIFIED":
				return "#acadaf";

			case "UNCONFIRMED":
				return "#acadaf";

			case "NEW":
			case "REOPENED":
				return "#ff0000";

			case "CLOSED":
				return "#000000";

			default:
				return "#000000";

			}
		}
	}
}
