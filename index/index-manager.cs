/*
 * GNOME Dashboard Text Index
 *
 * index-manager.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   This is the primary access point into the document index.  Objects which
 *   wish to use a document index should instantiate an instance of this object
 *   with the name of its store, e.g. local_files or remote_html.  Indices are
 *   segregated based on name space, i.e. the local_file index does not
 *   maintain the index of remote_html documents.
 *
 * TODO:
 *
 *   - add functionality for reindexing of a document
 *
 */

using System;
using System.IO;
using System.Text;
using System.Collections;

namespace Dashboard.Index {

public class IndexMatch : IComparable {
	private double         relevance;
	private SourceMetaData source;

	public double Relevance {
		get {
			return relevance;
		}

		set {
			relevance = value;
		}
	}

	public SourceMetaData Source {
		get {
			return source;
		}

		set {
			source = value;
		}
	}

	public int CompareTo (Object that)
	{
		if ((that == null) ||
		    (!(that is IndexMatch)) ||
		    (((IndexMatch)that).Source == null))
		{
			return 1;
		}

		if (this.Source.URI.Equals (((IndexMatch ) that).Source.URI))
			return 0;

		if (this.relevance > ((IndexMatch) that).relevance)
			return -1;

		return 1;
	}
}

public class IndexManager {
	private static string TEXT_INDEX_DIR_PATH = ".dashboard/backend-data/text-index";

	private static string VECTOR_DB_DIR  = "vectors";
	private static string SOURCES_DB_DIR = "sources";

	private BasisManager basis_mgr;

	private VectorManager vector_mgr;
	private SourcesManager sources_mgr;

	public IndexManager (string store_name)
	{
		basis_mgr = BasisManager.Instance;

		string path_format = 
			Environment.GetEnvironmentVariable ("HOME") + "/" + TEXT_INDEX_DIR_PATH + "/{0}/" +  store_name + ".db";

		vector_mgr = new VectorManager (String.Format (path_format, VECTOR_DB_DIR));
		sources_mgr = new SourcesManager (String.Format (path_format, SOURCES_DB_DIR));
	}

	public bool Initialized {
		get {
			return vector_mgr != null && sources_mgr != null && basis_mgr != null;
		}
	}

	private Extractor [] PreProcess (ref TextReader text, string mime_type)
	{
		Extractor [] extractors = new Extractor [1];

	// pipe reader through clue extractors

		extractors [0] = new URLExtractor (text);

		text = extractors [0];

	// pipe through necessary preprocessing filters

		if (mime_type.Equals ("text/html"))
			text = new HTMLFilteredReader (text);

		return extractors;
	}

	private ArrayList Process (TextReader text, string uri, string mime_type, bool retrieve, out ArrayList extracted_clues)
	{
		ArrayList matches = null;
		extracted_clues = null;

		if (!(mime_type.Equals ("text/plain") || mime_type.Equals ("text/html")))
			return null;


		Extractor [] extractors = PreProcess (ref text, mime_type);

	// if the passed in uri is null then we don't want to update.  also, if
	// the document already exists in the index then don't update (FIXME)

		bool update_index = (uri != null) && !sources_mgr.ContainsURI (uri);

	// generate token vector

		VectorToken [] vector = basis_mgr.ClassifyText (text, update_index);

		if (update_index) {
			int id = sources_mgr.UpdateSources (uri);

			vector_mgr.AddToIndex (vector, id);
		}

		if (retrieve) {
			ArrayList results = vector_mgr.Query (vector);

			matches = PostProcessResults (results);
		}
		else
			matches = null;

	// grab the extracted clue from the extractors
	// FIXME:  don't clobber previous extracted clues

		foreach (Extractor ext in extractors)
			extracted_clues = ext.ExtractedClues;

		return matches;
	}

	public ArrayList PostProcessResults (ArrayList results)
	{
		ArrayList matches = new ArrayList ();

	// find max_product to normalize the result list

		double max_product = 0.0;

		foreach (QueryResult result in results)
			if (result.Product > max_product)
				max_product = result.Product;

	// start the construction of the retrieval set

		SortedList list = new SortedList ();

		foreach (QueryResult result in results) {
			IndexMatch match = new IndexMatch ();

			match.Source = sources_mgr.GetSource (result.SourceID);

			match.Relevance = result.Product / max_product;

			try {
				list.Add (match, null);
			}
			catch (ArgumentException e) {
				Console.WriteLine ("ERROR");
				Console.WriteLine ("[{0}] {1}", match.Relevance, match.Source);
			}
		}

		matches = new ArrayList ();

		IDictionaryEnumerator iter = list.GetEnumerator ();

		foreach (IndexMatch match in list.Keys)
			matches.Add (match);

		return matches;
	}

	public ArrayList Retrieve (TextReader text, string mime_type, out ArrayList extracted_clues)
	{
		return Process (text, null, mime_type, true, out extracted_clues);
	}

	public void Index (TextReader text, string uri, string mime_type, out ArrayList extracted_clues)
	{
		Process (text, uri, mime_type, false, out extracted_clues);
	}

	public ArrayList IndexAndRetrieve (TextReader text, string uri, string mime_type, out ArrayList extracted_clues)
	{
		return Process (text, uri, mime_type, true, out extracted_clues);
	}

	public void OutputIndex ()
	{
		sources_mgr.OutputDB ();
	}
}

}
