/*
 * GNOME Dashboard Text Index
 *
 * html-indexer.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   This is a command line interface into the HTML index.
 *
 */

using System;
using System.IO;
using System.Text;
using System.Collections;

using Dashboard.Index;

public class FuckNut {
	private static void Usage ()
	{
		Console.WriteLine ("usage: html-indexer [-r \"text block to query\"]");
	}

	public static void Main (string [] args)
	{
		if (args.Length == 0) {
			Usage ();

			return;
		}

		IndexManager mgr = new IndexManager ("remote_html");

		int argc = 0;

		while (argc < args.Length) {
			if (args [argc] [0] == '-') {
				switch (args [argc++] [1]) {
				case 'r':
					ArrayList clues;

					ArrayList matches = mgr.Retrieve (new StringReader (args [argc++]), "text/plain", out clues);

					Console.WriteLine ("matches:");

					foreach (IndexMatch m in matches)
						Console.WriteLine ("[{0}] {1}", m.Relevance, m.Source);

					Console.WriteLine ();
					Console.WriteLine ("clues:");

					foreach (string clue in clues)
						Console.WriteLine (clue);

					break;
				}
			}
			else {
				Usage ();

				return;
			}
		}
	}
}
