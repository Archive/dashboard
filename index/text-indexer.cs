/*
 * GNOME Dashboard Text Index
 *
 * text-indexer.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   Command line interface into the plain text index.
 *
 */

using System;
using System.IO;
using System.Text;
using System.Collections;

using Dashboard.Index;

public class FuckNut {
	private static void Usage ()
	{
		Console.WriteLine ("usage: text-indexer [-i files_to_index ...] [-u files_to_query_and_index] [-r \"text block to query\"]");
	}

	public static void Main (string [] args)
	{
		if (args.Length == 0) {
			Usage ();

			return;
		}

		IndexManager mgr = new IndexManager ("local_file");

		int argc = 0;

		while (argc < args.Length) {
			if (args [argc] [0] == '-') {
				switch (args [argc++] [1]) {
				case 'i':
					while (argc < args.Length && args [argc] [0] != '-') {
						string filename = new FileInfo (args [argc++]).FullName;

						try
						{
							TextReader text = new StreamReader (filename, Encoding.ASCII);
							string uri = "file://" + filename;

							ArrayList extracted_clues;

							mgr.Index (text, uri, "text/plain", out extracted_clues);

							if (extracted_clues != null && extracted_clues.Count > 0) {
									Console.WriteLine ("clues extracted from {0}:", uri);
									foreach (string clue in extracted_clues)
											Console.WriteLine (clue);
							}
						}
						catch (IOException ioe)
						{
								Console.WriteLine("Failed to index {0}", filename);
						}
					}

					break;

				case 'u':
					while (argc < args.Length && args [argc] [0] != '-') {
						string filename = new FileInfo (args [argc++]).FullName;

						TextReader text = new StreamReader (filename, Encoding.ASCII);
						string uri = "file://" + filename;

						ArrayList extracted_clues;

						ArrayList matches = mgr.IndexAndRetrieve (text, uri, "text/plain", out extracted_clues);

						if (extracted_clues.Count > 0) {
							Console.WriteLine ("clues extracted from {0}:", uri);
							foreach (string clue in extracted_clues)
								Console.WriteLine (clue);
						}
					}

					break;

				case 'r':
					ArrayList clues;

					ArrayList matches = mgr.Retrieve (new StringReader (args [argc++]), "text/plain", out clues);

					Console.WriteLine ("matches:");

					foreach (IndexMatch m in matches)
						Console.WriteLine ("[{0}] {1}", m.Relevance, m.Source);

					Console.WriteLine ();
					Console.WriteLine ("clues:");

					foreach (string clue in clues)
						Console.WriteLine (clue);

					break;
				}
			}
			else {
				Usage ();

				return;
			}
		}
	}
}
