/*
 * GNOME Dashboard Text Index
 *
 * streamfilter-html.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   This sits on top of a text stream and filters out HTML tags.
 *
 * Remarks:
 *
 *   Removes anything in between a '<' and '>' (even if it's a javascript
 *   operator).
 *
 * TODO:
 *
 *   - (maybe) figure out if the open/close tag char is a javascript operator
 *     and act accordingly.  this might not be necessary since the worst that
 *     happens is that some of the javascript tokens will get confused as
 *     natural language tokens which will most likely fall to the side because
 *     of frequencies.
 *
 *   - translate HTML entities, e.g. &amp;
 *
 */

using System;
using System.IO;

namespace Dashboard.Index {

public class HTMLFilteredReader : TextReader {
	private TextReader reader;

	private static int S_0 = 0;
	private static int S_1 = 1;

	public HTMLFilteredReader (TextReader r)
	{
		reader = r;
	}

	public override int Read ()
	{
		int b;

		int state = S_0;

		while ((b = reader.Read ()) >= 0) {
			if (((char) b) == '<')
				state = S_1;

			else if (((char) b) == '>')
				state = S_0;

			else
				if (state == S_0)
					return b;
		}

		return b;
	}
}

}
