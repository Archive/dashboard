/*
 * GNOME Dashboard Text Index
 *
 * extractor-url.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   Snoops the underlying stream for URLs, without affecting that stream.
 *
 * TODO:
 *
 *   - should match the url as a regex group and add that to the list.
 *     otherwise strings like "url1:http://www.blah.org" could be added.
 */

using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Dashboard.Index {

public class URLExtractor : Extractor {
	private static int    MAX_URL_LENGTH = 1024;
	private static string URL_REGEX      = "(((http|ftp|https)://)|(www|ftp)[-A-Za-z0-9]*\\.)[-A-Za-z0-9\\.]+(:[0-9]*)?(/)?";

	private StringBuilder buf;
	private Regex         regex;

	public URLExtractor (TextReader reader) : base (reader)
	{
		buf   = new StringBuilder ();
		regex = new Regex (URL_REGEX, RegexOptions.IgnoreCase | RegexOptions.Compiled);
	}

	private bool IsURLCharacter (char c)
	{
		if (Char.IsLetterOrDigit (c))
			return true;

		return (c == '$' || c == '&' || c == '+' || c == ',' ||
		        c == '/' || c == ':' || c == ';' || c == '=' ||
		        c == '?' || c == '@' || c == '#' || c == '%' ||
		        c == '~' || c == '.');
	}

	public override int Read ()
	{
		int b = reader.Read ();

		if (IsURLCharacter ((char) b))
			buf.Append ((char) b);
		else {

// FIXME: should match the url as a regex group and add that to the list.
// otherwise strings like "url1:http://www.blah.org" could be added.

			if (buf.Length >= 4 && buf.Length <= MAX_URL_LENGTH && regex.Match (buf.ToString ()).Success)
				clues.Add (buf.ToString ());

			buf.Length = 0;
		}

		return b;
	}
}

}
