/*
 * GNOME Dashboard Text Index
 *
 * basis-manager.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   Each document in the index is reduced to a vector.  The dimensions of the
 *   vector correspond to the words contained in the document and the magnitude
 *   along that dimension corresponds to the frequency of the word in the
 *   document.  The notion of a basis in the indexer refers to the vector of
 *   all words in the corpus of documents maintained by the indexer and the
 *   frequency of that word in the corpus, not a specific document.  Every
 *   document vector is a subset of the basis vector.  The BasisManager
 *   maintains the basis vector in the file "basis.db".  In addition the
 *   BasisManager is responsible for translating incoming text blocks to their
 *   vector representations.  While each document vector is a subset of the
 *   basis vector for the entire corpus, the indices of each word dimension are
 *   consistent with the basis vector, e.g. if a word "superduper" appears in
 *   the corpus and has dimension index 45 in the basis, the instance of
 *   "superduper" will also have index 45 in each document vector.  As such,
 *   the document vectors returned by this module are sparse vectors.  Vectors
 *   are compared to each other by calculating their dot product.  Because of
 *   this the vectors are normalized.
 *
 * TODO:
 *
 *   - use UTF-8 encoding - sync basis to disk in a seperate thread.
 *
 */

using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Collections;

namespace Dashboard.Index {

class CorpusToken {
	public int col_index;
	public int n_inter;   // number of docs containing this token
	public int n_corp;    // number of occurences of token in corpus

	public string word;

	public bool seen;     // place holder for finding n_inter

/**
 * the token byte array contains:
 * bytes | data
 * ------|----------------
 * 0     | length of token
 * 1-4   | col_index
 * 5-8   | n_inter
 * 9-12  | n_corp
 * 13-   | word
 */
	public byte [] ToByteArray ()
	{
		int length = 13 + word.Length;

		byte [] token_bytes = new byte [length];

		byte [] buf = BitConverter.GetBytes (length);
		Array.Copy (buf, 0, token_bytes, 0, 1);

		buf = BitConverter.GetBytes (col_index);
		Array.Copy (buf, 0, token_bytes, 1, 4);

		buf = BitConverter.GetBytes (n_inter);
		Array.Copy (buf, 0, token_bytes, 5, 4);

		buf = BitConverter.GetBytes (n_corp);
		Array.Copy (buf, 0, token_bytes, 9, 4);

	// FIXME: use UTF-8

		buf = Encoding.ASCII.GetBytes (word);
		Array.Copy (buf, 0, token_bytes, 13, buf.Length);

		return token_bytes;
	}

	public CorpusToken (byte [] token_bytes)
	{
// the length of the token packet is not contained in the byte array passed
// into this function.

		col_index   = BitConverter.ToInt32 (token_bytes, 0);
		n_inter = BitConverter.ToInt32 (token_bytes, 4);
		n_corp  = BitConverter.ToInt32 (token_bytes, 8);

		int word_len = token_bytes.Length - 12;

		char [] word_chars = new char [word_len];
		Encoding.ASCII.GetChars (token_bytes, 12, word_len, word_chars, 0);

		word = new string (word_chars);
	}

	public CorpusToken (string w, int i)
	{
		word = w;
		col_index = i;

		n_inter = 1;
		n_corp  = 1;

		seen = true;
	}
}

public class VectorToken {
	public string word;

	public int    col_index;
	public double freq;

	public VectorToken (string w, int i, double f)
	{
		word = w;

		col_index = i;
		freq  = f;
	}
}

public class BasisManager {
	private static string BASIS_DB_PATH = ".dashboard/backend-data/text-index/basis.db";

	private static BasisManager instance = null;

	public static BasisManager Instance {
		get {
			if (instance == null)
				instance = new BasisManager ();

			return instance;
		}
	}

	private Hashtable basis;
	private Hashtable synched_basis;

	private int dim     = 0;    // dimension of the corpus basis
	private int n_docs  = 0;    // num docs in corpus

	private FileStream basis_fs;
	private string db_path;

	public BasisManager ()
	{
		basis         = new Hashtable ();
		synched_basis = Hashtable.Synchronized (basis);

		db_path = Path.Combine (Environment.GetEnvironmentVariable ("HOME"), BASIS_DB_PATH);

		if (! Directory.Exists(Path.GetDirectoryName(db_path))) {
			Console.WriteLine ( "*** Oh, {0} doesn't exist, creating.", Path.GetDirectoryName(db_path));
			Directory.CreateDirectory(Path.GetDirectoryName(db_path)); // FIXME: What exceptions can CreateDirectory throw?
		}

		try {
			basis_fs = File.OpenRead (db_path);

			LoadBasis ();
		}
		catch (FileNotFoundException e) {
			Console.WriteLine ("no such file ({0})", db_path);
		}
	}

	public VectorToken [] ClassifyText (TextReader reader, bool update_basis)
	{
		IEnumerator iter = new Tokenizer (reader);

		ResetSeenFlags ();

		Hashtable vector_hash = new Hashtable ();

		int n_words_in_doc = 0;

		while (iter.MoveNext ()) {
			int col_index;      // this is the col_index of this word in the
			                    // basis vector

			string word = FilterBank.FilterToken ((string) iter.Current);

			if (word != null) {
				if (update_basis) {
					CorpusToken tok = null;

					tok = (CorpusToken) basis [word];

					if (tok != null) {

		// check with placeholder to see if word's already appeared in the
		// current doc, if not, increment interdoc freq

						if (!tok.seen)
							++tok.n_inter;

		// always increment corpus freq

						++tok.n_corp;

		// update placeholder

						tok.seen = true;
					}
					else {
						tok = new CorpusToken (word, dim);

						Interlocked.Increment (ref dim);

						synched_basis.Add (word, tok);
					}

					col_index = tok.col_index;
				}

		// if we're not updating the basis database we still need to know the
		// col_index of the word in the basis vector so we need to search it

				else
					col_index = IndexOfWordInBasis (word);

		// provided this word is col_index worthy, we add it to the out vector

				if (col_index >= 0) {
					VectorToken v_tok = (VectorToken)vector_hash [word];

					if (v_tok != null)
						v_tok.freq += 1.0;
					else {
						v_tok = new VectorToken (word, col_index, 1.0);

						vector_hash.Add (word, v_tok);

						++n_words_in_doc;
					}
				}
			}
		}

		Interlocked.Increment (ref n_docs);

	// TODO: don't write basis to file every query, it's retarded.

		if (update_basis)
			SaveBasis ();

		return HashtableVectorToArray (vector_hash, n_words_in_doc);
	}

/*
 * construct the vector, making null elements instead of allocing elements with
 * freq = 0
 */

	private VectorToken [] HashtableVectorToArray (Hashtable hash, int n_words_in_doc)
	{
		VectorToken [] vector = new VectorToken [dim];

		for (int i = 0; i < vector.Length; ++i)
			vector [i] = null;

		IDictionaryEnumerator iter = hash.GetEnumerator ();

		double magnitude = 0.0;

		while (iter.MoveNext ()) {
			VectorToken token = (VectorToken)iter.Value;
			token.freq /= n_words_in_doc;

			magnitude += token.freq * token.freq;

			vector [token.col_index] = token;
		}

	// normalize vector

		double norm_coeff = Math.Sqrt (magnitude);

		for (int i = 0; i < vector.Length; ++i)
			if (vector [i] != null)
				vector [i].freq /= norm_coeff;

		return vector;
	}

	public void SaveBasis ()
	{
		lock (basis.SyncRoot) {
			basis_fs = File.OpenWrite (db_path);

			byte [] n_docs_bytes = BitConverter.GetBytes (n_docs);
			byte [] dim_bytes    = BitConverter.GetBytes (dim);

			basis_fs.Write (n_docs_bytes, 0, n_docs_bytes.Length);
			basis_fs.Write (   dim_bytes, 0,    dim_bytes.Length);

			IDictionaryEnumerator iter = basis.GetEnumerator ();

			while (iter.MoveNext ()) {
				byte [] token = ((CorpusToken) iter.Value).ToByteArray ();

				basis_fs.Write (token, 0, token.Length);
			}

			basis_fs.Close ();
		}
	}

	public void LoadBasis ()
	{
		byte [] buf = new byte [8];

		basis_fs.Read (buf, 0, 8);

		n_docs = BitConverter.ToInt32 (buf, 0);
		dim    = BitConverter.ToInt32 (buf, 4);

		int token_len = basis_fs.ReadByte ();

		while (token_len > 0) {
			buf = new byte [token_len-1];
			basis_fs.Read (buf, 0, buf.Length);

			CorpusToken tok = new CorpusToken (buf);

			basis.Add (tok.word, tok);

			token_len = basis_fs.ReadByte ();
		}

		basis_fs.Close ();
	}

/*
 * in order to calculate the number a documents a token appears in we want in
 * increment the counter once per document, so the CorpusToken contains a bool
 * flag indicating whether or not it's already been tallied once
 */

	private void ResetSeenFlags ()
	{
		lock (basis.SyncRoot) {
			IDictionaryEnumerator iter = basis.GetEnumerator ();

			while (iter.MoveNext ())
			 	((CorpusToken) iter.Value).seen = false;
		}
	}

	public override string ToString ()
	{
		string buf = "";

		IDictionaryEnumerator iter = basis.GetEnumerator ();

		while (iter.MoveNext ()) {
			string      key = (string) iter.Key;
			CorpusToken tok = (CorpusToken)iter.Value;

			double f_inter = ((double) tok.n_inter) / n_docs;
			double f_corp  = ((double) tok.n_corp ) / dim;

			buf += String.Format ("{0}: {1}\t [{2},\t{3}]\n", tok.col_index, tok.word, f_inter, f_corp);
		}

		buf += "\n";
		buf += String.Format ("{0} words", dim);

		return buf;
	}

/*
 * see if word is in the basis, if it is return it's col_index in the basis
 * vector, otherwise return -1
 */

	private int IndexOfWordInBasis (string word)
	{
		CorpusToken tok = (CorpusToken) basis [word];

		if (tok == null)
			return -1;

		return tok.col_index;
	}
}

}
