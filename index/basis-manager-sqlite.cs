/*
 * GNOME Dashboard Text Index
 *
 * basis-manager-sqlite.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   This file is unused.  It was an attempt to implement the basis manager in
 *   Sqlite.
 *
 */

using System;
using System.IO;
using System.Text;
using System.Collections;

using Mono.Data.SqliteClient;

namespace Dashboard.Index {

class CorpusToken {
	public int col_index;
	public int n_inter;   // number of docs containing this token
	public int n_corp;    // number of occurences of token in corpus

	public string word;

	public bool seen;     // place holder for finding n_inter

	public CorpusToken (string w, int i) : this (w, i, 1, 1, true) { }

	public CorpusToken (string w, int i, int n_i, int n_c) : this (w, i, n_i, n_c, false) { }

	public CorpusToken (string w, int i, int n_i, int n_c, bool s)
	{
		word = w;
		col_index = i;

		n_inter = n_i;
		n_corp  = n_c;

		seen = s;
	}
}

public class VectorToken {
	public string word;

	public int    col_index;
	public double freq;

	public VectorToken (string w, int i, double f)
	{
		word = w;

		col_index = i;
		freq  = f;
	}
}

public class BasisManager {
	private static string BASIS_DB_PATH = ".dashboard/backend-data/text-index/basis.db";

	private static BasisManager instance = null;

	public static BasisManager Instance {
		get {
			if (instance == null)
				instance = new BasisManager ();

			return instance;
		}
	}

	private SqliteConnection conn;

	private Hashtable basis;

	private int dim     = 0;    // dimension of the corpus basis
	private int n_docs  = 0;    // num docs in corpus

	private string db_path;

	public BasisManager ()
	{
		basis = new Hashtable ();

		db_path = Path.Combine (Environment.GetEnvironmentVariable ("HOME"), BASIS_DB_PATH);

		bool create_table = ! File.Exists (db_path);

		conn = new SqliteConnection ();
		conn.ConnectionString = "URI=file:" + db_path;

		conn.Open ();

		if (create_table)
			CreateDatabase ();

		LoadBasis ();
	}

	private void CreateDatabase ()
	{
		SqliteCommand cmd = new SqliteCommand ();
		cmd.Connection = conn;

		cmd.CommandText = 
			"create table basis    ( " +
			"  col_index    int    , " +
			"  n_inter      int    , " +
			"  n_corp       int    , " +
			"  word         text     " +
			")                   ";

		cmd.ExecuteNonQuery ();

		cmd.CommandText = 
			"create table corpus (" +
			"  dim     int      , " +
			"  n_docs  int        " +
			")                    ";

		cmd.ExecuteNonQuery ();

		cmd.Dispose ();
	}

	public VectorToken [] ClassifyText (TextReader reader, bool update_basis)
	{
		IEnumerator iter = new Tokenizer (reader);

		ResetSeenFlags ();

		Hashtable vector_hash = new Hashtable ();

		int n_words_in_doc = 0;

		while (iter.MoveNext ()) {
			int col_index;      // this is the col_index of this word in the
			                    // basis vector

			string word = FilterBank.FilterToken ((string) iter.Current);

			if (word != null) {
				if (update_basis) {
					CorpusToken tok = null;

					tok = (CorpusToken)basis [word];

					if (tok != null) {

		// check with placeholder to see if word's already appeared in the
		// current doc, if not, increment interdoc freq

						if (!tok.seen)
							++tok.n_inter;

		// always increment corpus freq

						++tok.n_corp;

		// update placeholder

						tok.seen = true;

		// save the update to the db

//						UpdateBasisDB (tok, false);
					}
					else {
						tok = new CorpusToken (word, dim++);

						basis.Add (word, tok);

		// insert the new token into the db

//						UpdateBasisDB (tok, true);
					}

					col_index = tok.col_index;
				}

		// if we're not updating the basis database we still need to know the
		// col_index of the word in the basis vector so we need to search it

				else
					col_index = IndexOfWordInBasis (word);

		// provided this word is col_index worthy, we add it to the out vector

				if (col_index >= 0) {
					VectorToken v_tok = (VectorToken)vector_hash [word];

					if (v_tok != null)
						v_tok.freq += 1.0;
					else {
						v_tok = new VectorToken (word, col_index, 1.0);

						vector_hash.Add (word, v_tok);

						++n_words_in_doc;
					}
				}
			}
		}

		++n_docs;

	// TODO: don't write basis to file every query, it's retarded.

		if (update_basis) {
			UpdateCorpusDB ();
			SyncBasisToFile ();
		}

		return HashtableVectorToArray (vector_hash, n_words_in_doc);
	}

/*
 * construct the vector, making null elements instead of allocing elements with
 * freq = 0
 */

	private VectorToken [] HashtableVectorToArray (Hashtable hash, int n_words_in_doc)
	{
		VectorToken [] vector = new VectorToken [dim];

		for (int i = 0; i < vector.Length; ++i)
			vector [i] = null;

		IDictionaryEnumerator iter = hash.GetEnumerator ();

		double magnitude = 0.0;

		while (iter.MoveNext ()) {
			VectorToken token = (VectorToken)iter.Value;
			token.freq /= n_words_in_doc;

			magnitude += token.freq * token.freq;

			vector [token.col_index] = token;
		}

	// normalize vector

		double norm_coeff = Math.Sqrt (magnitude);

		for (int i = 0; i < vector.Length; ++i)
			if (vector [i] != null)
				vector [i].freq /= norm_coeff;

		return vector;
	}

	private void SyncBasisToFile ()
	{
		SqliteCommand cmd = new SqliteCommand ();
		cmd.Connection = conn;

		cmd.CommandText = "delete from basis";

		cmd.ExecuteNonQuery ();

		ArrayList tokens = new ArrayList (basis.Values);

		foreach (CorpusToken tok in tokens) {
			cmd.CommandText = String.Format (
				"insert into basis                  " +
				"(col_index, word, n_inter, n_corp) " +
				"  values                           " +
				"('{0}', '{1}', '{2}', '{3}')       ",
				tok.col_index,
				tok.word,
				tok.n_inter,
				tok.n_corp);

			cmd.ExecuteNonQuery ();
		}
	}

	private void UpdateBasisDB (CorpusToken tok, bool insert)
	{
		Console.WriteLine ("i would like to {0} {1}", insert ? "insert" : "update", tok.word);

		SqliteCommand cmd = new SqliteCommand ();
		cmd.Connection = conn;

		if (insert)
			cmd.CommandText = String.Format (
				"insert into basis                  " +
				"(col_index, word, n_inter, n_corp) " +
				"  values                           " +
				"('{0}', '{1}', '{2}', '{3}')       ",
				tok.col_index,
				tok.word,
				tok.n_inter,
				tok.n_corp
			);
		else
			cmd.CommandText = String.Format (
				"update basis set     " +
				"  word     = '{1}' , " +
				"  n_inter  = '{2}' , " +
				"  n_corp   = '{3}'   " + 
				"where                " +
				"  col_index    = '{0}'   ",
				tok.col_index,
				tok.word,
				tok.n_inter,
				tok.n_corp
			);

		cmd.ExecuteNonQuery ();

		cmd.Dispose ();

		Console.WriteLine ("done");
	}

	private void UpdateCorpusDB ()
	{ /*
		SqliteCommand cmd = new SqliteCommand ();
		cmd.Connection = conn;

		cmd.CommandText = String.Format (String.Concat (
			"update corpus set     " ,
			"  dim       = '{1}' , " ,
			"  n_docs    = '{2}'   "),
			dim,
			n_docs
			);

		cmd.ExecuteNonQuery ();
		cmd.Dispose ();
*/	}

	public void LoadBasis ()
	{
		SqliteCommand cmd = new SqliteCommand ();
		cmd.Connection = conn;

		cmd.CommandText = 
			"select      " +
			"  col_index   , " +
			"  n_inter , " +
			"  n_corp  , " +
			"  word      " +
			"from basis  ";

		SqliteDataReader rdr = cmd.ExecuteReader ();

		while (rdr.Read ()) {
			string word    = rdr [3].ToString ();
			int    col_index   = Convert.ToInt32 (rdr [0]);
			int    n_inter = Convert.ToInt32 (rdr [1]);
			int    n_corp  = Convert.ToInt32 (rdr [2]);

			CorpusToken tok = new CorpusToken (word, col_index, n_inter, n_corp);
			basis.Add (tok.word, tok);
		}

		cmd.CommandText = 
			"select       " +
			"  dim      , " +
			"  n_docs     " +
			"from corpus  ";

		rdr = cmd.ExecuteReader ();

		if (rdr.Read ()) {
			dim    = Convert.ToInt32 (rdr [0]);
			n_docs = Convert.ToInt32 (rdr [1]);
		}

		cmd.Dispose ();
	}

/*
 * in order to calculate the number a documents a token appears in we want in
 * increment the counter once per document, so the CorpusToken contains a bool
 * flag indicating whether or not it's already been tallied once
 */

	private void ResetSeenFlags ()
	{
		IDictionaryEnumerator iter = basis.GetEnumerator ();

		while (iter.MoveNext ())
			 ((CorpusToken) iter.Value).seen = false;
	}

	public override string ToString ()
	{
		string buf = "";

		IDictionaryEnumerator iter = basis.GetEnumerator ();

		while (iter.MoveNext ()) {
			string      key = (string) iter.Key;
			CorpusToken tok = (CorpusToken)iter.Value;

			double f_inter = ((double) tok.n_inter) / n_docs;
			double f_corp  = ((double) tok.n_corp ) / dim;

			buf += String.Format ("{0}: {1}\t [{2},\t{3}]\n", tok.col_index, tok.word, f_inter, f_corp);
		}

		buf += "\n";
		buf += String.Format ("{0} words", dim);

		return buf;
	}

/*
 * see if word is in the basis, if it is return it's col_index in the basis
 * vector, otherwise return -1
 */

	private int IndexOfWordInBasis (string word)
	{
		CorpusToken tok = (CorpusToken) basis [word];

		if (tok == null)
			return -1;

		return tok.col_index;
	}
}

public class TestThisFucker {
	public static void Main (string [] args)
	{
		Console.WriteLine (BasisManager.Instance);
	}
}

}
