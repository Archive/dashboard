/*
 * GNOME Dashboard Text Index
 *
 * filter-bank.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   The FilterBank culls out unwanted word tokens, such as common words.  Also
 *   the FilterBank modifies incoming tokens, e.g. reduces tokens to their
 *   stems.
 *
 */

using System;

namespace Dashboard.Index {

public class FilterBank {
	private static string [] filters = {
		"Dashboard.Index.WordLengthFilter",
		"Dashboard.Index.CommonWordFilter",
		"Dashboard.Index.StemmingFilter"
	};

	private static TokenFilter [] bank;

	static FilterBank ()
	{
		bank = new TokenFilter [filters.Length];

		for (int i = 0; i < bank.Length; ++i) {
			Type filter_type = Type.GetType (filters [i]);
			bank [i] = (TokenFilter) Activator.CreateInstance (filter_type);
		}
	}

	public static string FilterToken (string token)
	{
		foreach (TokenFilter filter in bank) {
			if (token == null)
				return null;

			token = filter.Filter (token);
		}

		return token;
	}
}

}
