/*
 * GNOME Dashboard Text Index
 *
 * tokenizer.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   Sits on top of a text stream and breaks the stream into words that are
 *   seperated by either punctuation or whitespace.
 *
 * Remarks:
 *
 *   Here's the transition table for the state machine used to lex the stream
 *   into words tokens.
 *
 *         +-------------+-------------+-------------+-------------+
 *         |  WORD_CHAR  | PUNCTUATION | WHITE_SPACE |  CATCH_ALL  |
 *   +-----+-------------+-------------+-------------+-------------+
 *   |     |             |             |             |             |
 *   | S_0 |     S_1     |     S_0     |     S_0     |     S_2     |
 *   |     |             |             |             |             |
 *   +-----+-------------+-------------+-------------+-------------+
 *   |     |             |             |             |             |
 *   | S_1 |     S_1     |     S_3     |     S_0     |     S_2     |
 *   |     |             |             |             |             |
 *   +-----+-------------+-------------+-------------+-------------+
 *   |     |             |             |             |             |
 *   | S_2 |     S_2     |     S_2     |     S_0     |     S_2     |
 *   |     |             |             |             |             |
 *   +-----+-------------+-------------+-------------+-------------+
 *   |     |             |             |             |             |
 *   | S_3 |     S_2     |     S_2     |     S_0     |     S_2     |
 *   |     |             |             |             |             |
 *   +-----+-------------+-------------+-------------+-------------+
 * 
 *   - if transitioning to state S_1 and the incoming character to the token
 * 
 *   - if transitioning to state S_0 from S_1 or S_3 update Current pointer to
 *     the token.
 * 
 *   - if transitioning to state S_2 from S_1 or S_3 reset the token to zero.
 *
 */

using System;
using System.IO;
using System.Text;
using System.Collections;

namespace Dashboard.Index {

public class Tokenizer : IEnumerator {
	private string current;

	private TextReader reader;

	private StringBuilder buf;

	public object Current {
		get {
			return current;
		}
	}

// states for the word lexer

	private static int S_0     = 0;
	private static int S_1     = 1;
	private static int S_2     = 2;
	private static int S_3     = 3;

	private int state;

// symbols for the lexer

	private static int A_WORDCHAR    = 0;
	private static int A_PUNCT       = 1;
	private static int A_WHITESPACE  = 2;
	private static int A_CATCH_ALL   = 3;

// transition table

	private static int [,] transition_table = {
		{ 1, 0, 0, 2 },
		{ 1, 3, 0, 2 },
		{ 2, 2, 0, 2 },
		{ 2, 2, 0, 2 }
	};

	public Tokenizer (TextReader r)
	{
		reader = r;

		state = S_0;

		buf = new StringBuilder ();
	}

	private int ClassifySymbol (char c)
	{
		if (Char.IsLetterOrDigit (c))
			return A_WORDCHAR;

		if (Char.IsPunctuation (c))
			return A_PUNCT;

		if (Char.IsWhiteSpace (c))
			return A_WHITESPACE;

		return A_CATCH_ALL;
	}

	public bool MoveNext ()
	{
		buf.Length = 0;

		int b;

		while ((b = reader.Read ()) >= 0) {
			int a = ClassifySymbol ((char) b);

			int state_0 = state;

			state = transition_table [state_0, a];

			if (state == S_0 && (state_0 == S_1 || state_0 == S_3)) {
				current = buf.ToString ();

				return true;
			}

			if (state == S_2 && (state_0 == S_1 || state_0 == S_3))
				buf.Length = 0;

			if (state == S_1)
				buf.Append ((char) b);
		}

// FIXME:  why does this throw a NullReferenceException when an incoming file
//         doesn't have a trailing newline (or maybe EOF)?

//		reader.Close ();

		if (state == S_1) {
			current = buf.ToString ();

			state = S_0;

			return true;
		}

		return false;
	}

	public void Reset ()
	{
	}
}

}
