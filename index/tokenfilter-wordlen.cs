/*
 * GNOME Dashboard Text Index
 *
 * tokenfilter-wordlen.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 */

namespace Dashboard.Index {

public class WordLengthFilter : TokenFilter {
	private static int MAX_WORD_LEN = 50;

	public override string Filter (string token)
	{
		if (token.Length > MAX_WORD_LEN)
			return null;
		else
			return token;
	}
}

}
