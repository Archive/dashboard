/*
 * GNOME Dashboard Text Index
 *
 * sources-manager.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   Each document stored in the index has a URI which corresponds to the
 *   source of the document.  This is particularly important since the index
 *   does not store the original document.  In addition to the URI this file
 *   will also store metadata associated with the document, e.g. mtime,
 *   extracted clues, author, etc.  This store is particular to the name space
 *   of the index, e.g. the SourcesManager for local_files is different than
 *   that for remote_html.  SourcesManager stores the data in
 *   sources/<store_name>.db.
 *
 * Remarks:
 *
 *   Requires Sqlite.
 *
 * TODO:
 *
 *   - turn this into an interface in order to create specified source
 *   managers.
 *
 */

using System;
using System.IO;
using System.Data;
using System.Threading;

using Mono.Data.SqliteClient;

namespace Dashboard.Index {

public class SourceMetaData {
	private string uri;
	private int    id;
	private long   last_write_time;

	public string URI {
		get {
			return uri;
		}

		set {
			uri = value;
		}
	}

	public int ID {
		get {
			return id;
		}

		set {
			id = value;
		}
	}

	public long LastWriteTime {
		get {
			return last_write_time;
		}

		set {
			last_write_time = value;
		}
	}

	public override string ToString ()
	{
		return String.Format ("[{0}] {1} ({2})", id, uri, last_write_time);
	}
}

public class SourcesManager {
	private SqliteConnection conn;

	private Mutex mutex;

	public SourcesManager (string path)
	{
		if (! Directory.Exists(Path.GetDirectoryName(path))) {
			Console.WriteLine ( "*** Oh, {0} doesn't exist, creating.", Path.GetDirectoryName(path));
			Directory.CreateDirectory(Path.GetDirectoryName(path)); // FIXME: What exceptions can CreateDirectory throw?
		}

		bool create_table = ! File.Exists (path);

		conn = new SqliteConnection ();

		conn.ConnectionString = "URI=file:" + path;

		conn.Open ();

		mutex = new Mutex ();

		if (create_table)
			CreateDatabase ();
	}

	private void CreateDatabase ()
	{
		mutex.WaitOne ();

		SqliteCommand cmd = new SqliteCommand ();
		cmd.Connection = conn;

		cmd.CommandText =
			"create table sources (        " +
			"  id                   int  , " +
			"  uri                  text , " +
			"  last_write_time      int    " +
			")                             " ;

		cmd.ExecuteNonQuery ();
		cmd.Dispose ();

		mutex.ReleaseMutex ();
	}

	public bool ContainsURI (string uri)
	{
		return GetID (uri) >= 0;
	}

	public int GetID (string uri)
	{
		mutex.WaitOne ();

		SqliteCommand cmd = new SqliteCommand ();
		cmd.Connection = conn;

		cmd.CommandText = String.Format (
			"select             "  +
			"  id               "  +
			"from sources       "  +
			"where uri = '{0}'  "  +
			"limit 1            "  ,
			uri
		);

		SqliteDataReader rdr = cmd.ExecuteReader ();

		if (rdr.Read ()) {
			int id = Convert.ToInt32 (rdr ["id"]);

			rdr.Close ();
			cmd.Dispose ();

			mutex.ReleaseMutex ();

			return id;
		}

		rdr.Close ();
		cmd.Dispose ();

		mutex.ReleaseMutex ();

		return -1;
	}

	public SourceMetaData GetSource (int id)
	{
		mutex.WaitOne ();

		SqliteCommand cmd = new SqliteCommand ();
		cmd.Connection = conn;

		cmd.CommandText = String.Format (
			"select             "  +
			"  uri,             "  +
			"  last_write_time  "  +
			"from sources       "  +
			"where id = {0}     "  +
			"limit 1            "  ,
			id
		);

		SqliteDataReader rdr = cmd.ExecuteReader ();

		if (rdr.Read ()) {
			SourceMetaData src = new SourceMetaData ();

			src.ID   = id;
			src.URI  = rdr [0].ToString ();

			string lwt = rdr [1].ToString ();

			src.LastWriteTime = Convert.ToInt64 (lwt);

			rdr.Close ();
			cmd.Dispose ();

			mutex.ReleaseMutex ();

			return src;
		}

		rdr.Close ();
		cmd.Dispose ();

		mutex.ReleaseMutex ();

		return null;
	}

/*
 * if the path is already contained, do nothing.  otherwise, add the path to
 * the db.  in either case return the id of the record in the db.
 */

	public int UpdateSources (string uri)
	{
		int id = GetID (uri);

		if (id >= 0)
			return id;

		mutex.WaitOne ();

		SqliteCommand cmd = new SqliteCommand ();
		cmd.Connection = conn;

		cmd.CommandText =
			"select            " +
			"  id              " +
			"from sources      " +
			"order by id desc  " +
			"limit 1           " ;

		SqliteDataReader rdr = cmd.ExecuteReader ();

		if (rdr.Read ())
			id = Convert.ToInt32 (rdr ["id"]) + 1;
		else
			id = 0;

		rdr.Close ();
		cmd.Dispose ();

		cmd = new SqliteCommand ();
		cmd.Connection = conn;

		long lwt = DateTime.Now.ToFileTime ();

		if (uri.StartsWith ("file://"))
			lwt = File.GetLastAccessTime (uri.Substring (7)).ToFileTime ();

		cmd.CommandText = String.Format (
			"insert into sources         " +
			"(uri, id, last_write_time)  " +
			"  values                    " +
			"('{0}', {1}, {2})           " ,
			uri                            ,
			id                             ,
			lwt
		);

		cmd.ExecuteNonQuery ();
		cmd.Dispose ();

		mutex.ReleaseMutex ();

		return id;
	}

	public void OutputDB ()
	{
		mutex.WaitOne ();

		SqliteCommand cmd = new SqliteCommand ();
		cmd.Connection = conn;

		cmd.CommandText =
			"select         " +
			"  *            " +
			"from sources   " ;

		SqliteDataReader rdr = cmd.ExecuteReader ();

		while (rdr.Read ())
			Console.WriteLine ("{0}: [{1}] {2}", rdr [0], rdr [2], rdr [1]);

		mutex.ReleaseMutex ();
	}
}

/* public class TestThisFucker {
	public static void Main (string [] args)
	{
		SourcesManager sm = new SourcesManager (args [0]);

//		Console.WriteLine (sm.UpdateSources (args [1]));

		Source src = sm.GetSource (Convert.ToInt32 (args [1]));

		Console.WriteLine (src);
	}
} */

}
