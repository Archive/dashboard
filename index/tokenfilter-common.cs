/*
 * GNOME Dashboard Text Index
 *
 * tokenfilter-common.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   Common words are unhelpful in indexing.  Words such as "and" or "the" are
 *   ineffective in discriminating classes of documents.  Currently this filter
 *   uses a static list of commonly used english words.  I have no idea where
 *   this list came from originally.  Ideally, this filter will cull out
 *   "common" words based on the particular word distribution of the local
 *   corpus and the filtered words will be dynamically determined.
 *
 * TODO;
 *
 *   - use corpus frequencies to determine common words.
 *
 */

using System;
using System.IO;
using System.Collections;

namespace Dashboard.Index {

public class CommonWordFilter : TokenFilter {
	private static SortedList list;

	static CommonWordFilter ()
	{
		list = new SortedList ();

		// Use either the system installed common_words.txt, or the users one.
		string cw_path = ConfigPath("common_words.txt");

		StreamReader stream = new StreamReader (cw_path);

		string line = stream.ReadLine ();

		while (line != null) {
			try {
				list.Add (line.ToLower (), null);
			}
			catch (ArgumentException e) {
				if (!e.Message.Equals("element already exists"))
					throw e;
			}

			line = stream.ReadLine ();
		}
	}

	public override string Filter (string token)
	{
		if (list.Contains (token))
			return null;
		else
			return token;
	}

}

}
