/*
 * GNOME Dashboard Text Index
 *
 * vector-manager.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   The VectorManager matches incoming document vectors against the stored
 *   document vectors.  Index matches are based on the dot product of these
 *   vectors.  The VectorManager stores these vectors in
 *   vectors/<store_name>.db.
 *
 * Remarks:
 *
 *   See the basis-manager.cs file for more description of document vectors.
 *
 */

using System;
using System.IO;
using System.Threading;
using System.Collections;

namespace Dashboard.Index {

public class QueryResult {
	private double product;
	private int    source_id;

	public double Product {
		get {
			return product;
		}

		set {
			product = value;
		}
	}

	public int SourceID {
		get {
			return source_id;
		}

		set {
			source_id = value;
		}
	}
}

class VectorNode {
	private VectorToken [] vector;
	private int source_id;

	public VectorToken [] Vector {
		get {
			return vector;
		}

		set {
			vector = value;
		}
	}

	public int SourceID {
		get {
			return source_id;
		}

		set {
			source_id = value;
		}
	}

	public override string ToString ()
	{
		return String.Format ("dim = {0} [{1}]", vector.Length, source_id);
	}
}

class IndexEnumerator : IEnumerator {
	private Object current;

	private string     index_db_path;
	private FileStream index_fs;

	private Mutex mutex;

	public IndexEnumerator (string path, Mutex m)
	{
		index_db_path = path;

		mutex = m;

		OpenIndexStream();
	}

	public Object Current {
		get {
			return current;
		}
	}

	private void OpenIndexStream ()
	{
		try {
			index_fs = File.OpenRead (index_db_path);
		}
		catch (FileNotFoundException e) {
			index_fs = null;
		}
	}
	
	public bool MoveNext ()
	{
		mutex.WaitOne ();

		if (index_fs == null)
		{
			mutex.ReleaseMutex();
			return false;
		}

		byte [] buf = new byte [4];

		if (index_fs.Read (buf, 0, buf.Length) <= 0)
		{
			mutex.ReleaseMutex();
			return false;
		}

		int dim = BitConverter.ToInt32 (buf, 0);

		buf = new byte [8 * dim];

		int bytes_read = index_fs.Read (buf, 0, buf.Length);

		if (bytes_read < 8*dim)
		{
			mutex.ReleaseMutex();
			return false;
		}

		VectorNode node = new VectorNode ();

		VectorToken [] v = new VectorToken [dim];

		for (int i = 0; i < dim; ++i) {
			double freq = BitConverter.ToDouble (buf, i*8);

			v [i] = new VectorToken ("", i, freq);
		}

		node.Vector = v;

		buf = new byte [4];

		index_fs.Read (buf, 0, buf.Length);

		node.SourceID = BitConverter.ToInt32 (buf, 0);

		current = node;

		mutex.ReleaseMutex ();

		return true;
	}

	public void Reset ()
	{
		if (index_fs == null)
			return;

		index_fs.Close ();

		OpenIndexStream();
	}
}

public class VectorManager {
	private string      index_db_path;
	private IEnumerator index;

	private Mutex mutex;

	public VectorManager (string path)
	{
		index_db_path = path;

		if (! Directory.Exists(Path.GetDirectoryName(path))) {
			Console.WriteLine ( "*** Oh, {0} doesn't exist, creating.", Path.GetDirectoryName(path));
			Directory.CreateDirectory(Path.GetDirectoryName(path)); // FIXME: What exceptions can CreateDirectory throw?
		}

		mutex = new Mutex ();

		index = new IndexEnumerator (index_db_path, mutex);
	}

	public void AddToIndex (VectorToken [] vector, int id)
	{
		mutex.WaitOne ();

		byte [] buf = BitConverter.GetBytes (vector.Length);

		FileStream fs = new FileStream (index_db_path, FileMode.Append, FileAccess.Write);

		fs.Write (buf, 0, buf.Length);

		for (int i = 0; i < vector.Length; ++i) {
			double freq = 0.0;

			if (vector [i] != null)
				freq = vector [i].freq;

			buf = BitConverter.GetBytes (freq);

			fs.Write (buf, 0, buf.Length);
		}

		buf = BitConverter.GetBytes (id);

		fs.Write (buf, 0, buf.Length);

		fs.Flush ();

		mutex.ReleaseMutex ();
	}

	public ArrayList Query (VectorToken [] vector)
	{
		index.Reset ();

		ArrayList list = new ArrayList ();

		while (index.MoveNext ()) {
			VectorNode node = (VectorNode) index.Current;

			double dot_prod = 0.0;

			int min_dim = vector.Length > node.Vector.Length ? node.Vector.Length : vector.Length;

			for (int i = 0; i < min_dim; ++i)
				if (vector [i] != null)
					dot_prod += vector [i].freq * node.Vector [i].freq;

			if (dot_prod > 0) {
				QueryResult result = new QueryResult ();

				result.Product   = dot_prod;
				result.SourceID  = node.SourceID;

				list.Add (result);
			}
		}

		return list;
	}

	public void OutputVector (VectorToken [] vector)
	{
		for (int i = 0; i < vector.Length; ++i) {
			if (vector [i] != null)
				Console.WriteLine ("{0}:\t{1}\t [{2}]", vector [i].col_index, vector [i].word, vector [i].freq);
		}

		Console.WriteLine ();
		Console.WriteLine ("dim = {0}", vector.Length);
	}

	public void OutputIndex ()
	{
		index.Reset ();

		while (index.MoveNext ())
			Console.WriteLine (index.Current);
	}
}

}
