/*
 * GNOME Dashboard Text Index
 *
 * extractor.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   General base class for an Extractor.  An Extractor snoops the underlying
 *   stream for more structured clues, such as URLs or email addresses.  An
 *   Extractor should not affect the underlying stream and remain entirely
 *   passive.
 *
 */

using System;
using System.IO;
using System.Collections;

namespace Dashboard.Index {

public abstract class Extractor : TextReader {
	protected TextReader reader;
	protected ArrayList clues;

	public ArrayList ExtractedClues {
		get {
			return clues;
		}
	}

	public Extractor (TextReader r)
	{
		reader = r;
		clues  = new ArrayList ();
	}

	public override void Close ()
	{
		reader.Close ();
	}
}

}
