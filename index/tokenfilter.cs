/*
 * GNOME Dashboard Text Index
 *
 * tokenfilter.cs
 *
 * Authors:
 *   Jim Krehl <jimmuhk@yahoo.com>
 *
 * Purpose of this file:
 *
 *   FIXME
 *
 */

using System;
using System.IO;

namespace Dashboard.Index {

	public abstract class TokenFilter {
		public abstract string Filter (string token);


		// The path to this backends system configuration directory.
		protected static string ConfigSystemPath ()
		{
			return Path.Combine(Environment.GetEnvironmentVariable
							 ("DASHBOARD_BACKEND_PATH"),
							 "text-index");
		}

		// The path to this backends user configuration directory.
		protected static string ConfigUserPath ()
		{
			return Path.Combine(Path.Combine(Environment.GetEnvironmentVariable
							 ("HOME"),
							 ".dashboard"),
					    "text-index");
		}

		// Use either a system installed file, or one from the text-index directory
		// in the users home directory.
		protected static string ConfigPath (string Filename)
		{
			if (File.Exists(Path.Combine (ConfigUserPath(), Filename))) {
				return Path.Combine (ConfigUserPath(), Filename);
			} else {
				return Path.Combine(ConfigSystemPath(), Filename);
			}
		}

	}


}
