
using System;
using System.Collections;
using System.Xml;
using GLib;
using Gtk;
using GtkSharp;
using System.Runtime.InteropServices;

namespace Dashboard {
	public class AssociatrixBackend : Backend {
		[DllImport ("libebook")]
		extern static IntPtr e_book_simple_find (string field, 
							 string needle, 
							 string how);

		[DllImport ("libename")]
		extern static IntPtr e_name_western_parse (string full_name);
		[DllImport ("libename")]
		extern static void e_name_western_free (IntPtr ename);

		public override bool Startup ()
		{
			this.Name = "Associatrix";
			this.SubscribeToClues ("textblock", 
					       "sentence_at_point", 
					       "aim_name" /* for gui entry */);
			this.Initialized = true;

			return this.Initialized;
		}

		struct ENameWestern {
			/* Public */
			public string prefix;
			public string first;
			public string middle;
			public string nick;
			public string last;
			public string suffix;
		}

		private string GetXmlText (XmlDocument xml, string tag)
		{
			XmlNode node;

			node = xml.SelectSingleNode (tag);
			if (node == null)
				return null;

			return node.InnerText;
		}

		protected override Match ProcessClueSimple (Clue clue)
		{
			if (clue.Text == null || clue.Text == "")
				return null;

			IntPtr p;
			Console.WriteLine ("AB(assoc) TRYLOCK!");
			lock (typeof (Addressbook)) {
				Console.WriteLine ("AB(assoc) LOCKED!");
				p = e_book_simple_find ("x-evolution-any-field",
							"", 
							"contains");
			}
			Console.WriteLine ("AB(assoc) UNLOCKED!");

			GLib.List l = new GLib.List (p, typeof (string));
			ArrayList matches = new ArrayList (l);
			ArrayList names = new ArrayList ();
			string upperclue = clue.Text.ToUpper ();

			foreach (string xml in matches) {
				XmlDocument card = new XmlDocument ();

				card.LoadXml (xml);

				string full_name  = GetXmlText (card, "//fname");
				if (full_name == null)
					continue;

				IntPtr pename = e_name_western_parse (full_name);
				ENameWestern ename = (ENameWestern) Marshal.PtrToStructure (pename, typeof (ENameWestern));

				Console.WriteLine ("*** ebook entry: {0} {1} {2} {3}",
						   ename.first, ename.last, ename.middle, ename.nick);

				// look for an exactish match
				if ((ename.first != null && 
				     ename.last != null && 
				     upperclue.IndexOf (ename.first.ToUpper ()) > -1 &&
				     upperclue.IndexOf (ename.last.ToUpper ()) > -1)) {
					ArrayList newclues = new ArrayList ();

					newclues.Add (new Clue ("full_name", 
								full_name, 
								clue.Relevance * 2));

					// don't display anything, just chain the clue
					return new Match (this.Name,
							  null,
							  0,
							  null);

				}

				if ((ename.first  != null && upperclue.IndexOf (ename.first.ToUpper ())  > -1) ||
				    (ename.last   != null && upperclue.IndexOf (ename.last.ToUpper ())   > -1) ||
				    (ename.middle != null && upperclue.IndexOf (ename.middle.ToUpper ()) > -1) ||
				    (ename.nick   != null && upperclue.IndexOf (ename.nick.ToUpper ())   > -1)) {
					names.Add (full_name);
				}

				e_name_western_free (pename);
			}

			if (names.Count > 0) {
				string avoidcache = "";
				foreach (string name in names) {
					avoidcache += name;
				}
				return new Match (this.Name, 
						  GenerateMaybeMatchHTML (names), 
						  0, 
						  avoidcache);
			} else
				return null;
		}

		public string GenerateMaybeMatchHTML (ArrayList names) {
			string html = 
				"<table border=0 width=100%>" +
				"  <tr>" +
				"    <td colspan=2>" +
				"      <h2><u>Do you mean...</u></h2>" +
				"    </td>" +
				"  </tr>";

			foreach (string name in names) {
				Console.WriteLine ("Do you mean... {0}", new Uri ("clue://full_name/" + name));
				html += string.Format ("<tr>" +
						       "  <td width=\"10%\" align=\"right\">o</td>" +
						       "  <td><a href=\"{1}\">{0}</a></td>" +
						       "</tr>",
						       name,
						       new Uri ("clue://full_name/" + name));
			}

			html += "</table>";

			return html;
		}
	}
}
