//
// backend-manpages.cs: Looks up a C identifier in man sections 2 and
// 3, and displays the NAME and SYNOPSIS found at the man page.
// Clicking causes yelp to load the man page.
//
// Author:
//    Cesar Lopez Nataren (cesar@ciencias.unam.mx)
//


using System;
using System.IO;
using ICSharpCode.SharpZipLib.GZip;
using System.Diagnostics;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;

[assembly:Dashboard.BackendFactory ("Dashboard.ManPagesBackend")]

namespace Dashboard {

	class ManPage {
		public string identifier;
		public string path;
		public string basename;
		public string section;
	}

	class ManPagesBackend : BackendSimple
	{
// 		private const string MAN_PAGE_PATHS = "/usr/share/man/man1 /usr/share/man/man2 /usr/share/man/man3 /usr/share/man/man4 /usr/share/man/man5 /usr/share/man/man6 /usr/share/man/man7 /usr/share/man/man8 /usr/share/man/man9 /usr/share/man/mann";
// This is not really a path, but an argument to locate to find the
// right pages, so this trick argument will do all the work for us...
		private const string MAN_PAGE_PATHS = "/man/man";
		private ArrayList manpages;

		public override bool Startup ()
		{
			Name = "Manual Pages";

			Console.WriteLine ("Manual Pages backend starting");

			manpages = new ArrayList ();

			try {
				//
				// Use 'locate' to find all the files in the user's
				// documents directory.
				//
				Process locate = new Process ();
				locate.StartInfo.FileName               = "/usr/bin/locate";
				locate.StartInfo.Arguments              = MAN_PAGE_PATHS;
				locate.StartInfo.UseShellExecute        = false;
				locate.StartInfo.RedirectStandardOutput = true;

				locate.Start ();

				// Read the man page file names line by line.
				string man_line;
				while ((man_line = locate.StandardOutput.ReadLine ()) != null) {
					if (File.Exists (man_line)) {

						// FIXME: Hack to deal with all the chroots on my system
						if (! man_line.StartsWith ("/usr"))
							continue;

						string[] split = man_line.Split ('/');
						string filename = split [split.Length - 1];

						ManPage mp = new ManPage ();
						mp.path       = man_line;
						mp.identifier = filename.Substring (0, filename.IndexOf ('.'));
						mp.basename   = filename.Substring (0, filename.LastIndexOf ('.'));
						mp.section    = filename.Substring (filename.IndexOf ('.') + 1, 1);

						manpages.Add (mp);
					}
				}

				Console.WriteLine ("ManPages backend loaded {0} man pages.", manpages.Count);

			} finally {
				//
				// Signal that we are ready to accept clues.
				//
				this.SubscribeToClues ("identifier");
				this.Initialized = true;
			}

			return true;
		}


		protected override ArrayList ProcessClueSimple (Clue clue)
		{
			ArrayList manpagematches = GetMatchingManPages (clue.Text);
			if (manpagematches == null || manpagematches.Count == 0)
				return null;

			ArrayList matches = new ArrayList();

			foreach (ManPage mp in manpagematches)
				matches.Add (GenerateMatch (mp, clue));

			return matches;
		}

		ArrayList GetMatchingManPages (string identifier)
		{
			ArrayList matches = new ArrayList ();

			foreach (ManPage mp in this.manpages)
				if (mp.identifier == identifier) {
					Console.WriteLine ("Man page backend found a match");
					matches.Add (mp);
				}

			return matches;
		}

		Match GenerateMatch (ManPage mp, Clue clue)
		{
			string syn = "";
			string page = GenerateManPage (mp);
			if (page != null)
				syn = ExtractProto (mp, page);

			Match match = new Match ("ManPage", clue);
			match ["Icon"]   = "internal:book.png";
			match ["Action"] = "exec:yelp man:" + mp.basename;
			match ["Text"]   = mp.identifier + "(" + mp.section + ")";
			match ["Title"]  = match ["text"];

			return match;
		}

		string GenerateManPage (ManPage mp)
		{
			Process man = new Process ();
			man.StartInfo.FileName               = "/usr/bin/man";
			man.StartInfo.Arguments              = mp.path;
			man.StartInfo.UseShellExecute        = false;
			man.StartInfo.RedirectStandardOutput = true;

			man.Start ();

			return man.StandardOutput.ReadToEnd ();
		}

		string ExtractProto (ManPage mp, string man_text)
		{
			string proto_rx = @"S\010SY\010YN\010NO\010OP\010PS\010SI\010IS\010S(.+?)D\010DE\010ES\010SC\010CR\010RI\010IP\010PT\010TI\010IO\010ON\010N";

			Regex r = new Regex (proto_rx, RegexOptions.Multiline | RegexOptions.Compiled);

			System.Text.RegularExpressions.Match m;
			for (m = r.Match (man_text); m.Success; m = m.NextMatch ())
				return m.Groups[1].ToString (); // FIXME: Catch multiprotos

			return "";
		}
	}
}
