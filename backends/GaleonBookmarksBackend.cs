using System;
using System.IO;
using System.Xml;
using System.Collections;

[assembly:Dashboard.BackendFactory ("Dashboard.GaleonBookmarksBackend")]

namespace Dashboard {

	class GaleonBookmarksBackend : BackendSimple {
		XmlDocument         doc;
		XmlNodeList         titles;
		XmlDocument         favicondoc;
		
		public override bool Startup ()
		{
			Name = "Galeon";

			string home_dir = Environment.GetEnvironmentVariable ("HOME");
			string path;

			try {
				path = Path.Combine (home_dir, ".galeon/bookmarks.xbel");

				if (!File.Exists (path)){
					path = Path.Combine (home_dir, ".galeon/bookmarks.xml");
					Console.WriteLine ("Galeon Bookmarks Path: " + path);
					if (!File.Exists (path))
						return false;
				}

				doc = new XmlDocument ();
				doc.Load (path);

				titles = doc.SelectNodes ("//bookmark/title");
				Console.WriteLine ("Galeon Bookmarks backend loaded {0} bookmarks.", titles.Count);

				path = Path.Combine (home_dir, ".galeon/favicon_cache.xml");
				Console.WriteLine ("Galeon FavIcon Path: " + path);
				if (!File.Exists (path)) {
					Console.WriteLine ("Galeon FavIcon Not Found: " + path);
					return false;
				}


				favicondoc = new XmlDocument ();
				favicondoc.Load (path);
 				
				// FIXME: need to subscribe to clues that we
				// should try matching against. Probably
				// textblock and sentence_at_point:
				// this.SubscribeToClues ("textblock", "sentence_at_point");

				this.Initialized = true;

				return true;
			} catch {
				return false;
			}
		}

		protected override ArrayList ProcessClueSimple (Clue clue)
		{
			ArrayList matches = new ArrayList();

			if (! this.Initialized)
				return null;

			if (clue.Text.Length == 0)
				return null;

			string clue_text = clue.Text.ToLower ();
	
			foreach (XmlNode node in titles) {
				string t = node.InnerText;
				string lower = t.ToLower ();

				if (lower.IndexOf (clue_text) == -1)
					continue;

				if (node.ParentNode == null)
					throw new Exception ("Null found");
				if (node.ParentNode.Attributes ["href"] == null) {
					throw new Exception ("Null found222");
				}

                                string uri = node.ParentNode.Attributes ["href"].InnerText;

				// Galeon Saves FavIcon Cache without 
				// Trailing Slash.  Code Around.
				string myurl = uri;
				if (myurl.EndsWith("/")) {
					myurl = myurl.Substring(0,myurl.Length - 1);
				}
				// End Code Around

                                string xpath = "GaleonFaviconCache/entry[@url=\"" + myurl + "\"]";
                                XmlNode favicon = favicondoc.SelectSingleNode (xpath);
                                string iconuri;
                                if (favicon != null)
                                        iconuri = favicon.Attributes ["favicon"].InnerText;
                                else
                                        iconuri ="internal:bookmark.png";
	
				Match match = new Match ("WebLink", clue);
				match ["Icon"] = iconuri;
				match ["Title"] = t;
				match ["URL"] = myurl;

				matches.Add (match);
			}

			return matches;
		}
	}
}
