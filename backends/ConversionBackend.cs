//
// GNOME Dashboard
// 
// ConversionBackend.cs: ???
//
// Copyright (C) 2003 Pierre ANDREWS
//
// Read the file .dashboard/backend-data/conversion/config.xml for
// conversion informations: the '<Conversion>' tag.
//
// We expect to find the attributes:
//
//     'Enabled' {true,false} to decide if we want this configuration to generate a match
//     'From' the identifier of the Unit this convertion implies (only one convertion by unit)
//     'To' the identifier of the output Unit (for displaying purpose)
//     'Factor' the multiplication factor in the conversion (double)
//     'Add' the value to add after the multiplication (double)
//
// Hence the conversion is made like this: (Factor*value)+Add (we only
// do linear conversions).
//
// For example:
//
//     <Conversion Enabled="true" From="oF" To="oC" Factor="0.55" Add="-17.6"/>
//
// Will convert measure clue like "32 oF" or "43oF" (space are
// ignored) to 0oF or 6.1oC (respectively)
//
// Measure clue are extracted with regexpchainer, so the From
// identifier should correspond to the extracted regexp in
// ~/.dashboard/backend-data/regexchainer/config.xml
//

using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Xml;
using System.Web;
using System.IO;

namespace Dashboard {

	class ConversionBackend : Backend {

		struct Conversion {
			public string To;
			public double Factor;
			public double Add;
		}
 
		//all the conversions string (From) -> Conversion
		protected Hashtable conversions = new Hashtable ();

		//configuration information
		protected DateTime lastModified;
		protected string path;

		public override bool Startup ()
		{
			Name = "Conversion";
	  
			// Find out where to find this file.
			path  = ConfigPath ("config.xml");
    		    
			this.Initialized = ReadConversion ();
			this.SubscribeToClues ("measure");

			return this.Initialized;
		}

		protected bool ReadConversion () {
			try {
				XmlDocument doc = new XmlDocument ();
				doc.Load (path);

				lastModified = File.GetLastWriteTime (path);

				//get the right config node
				XmlNode config = doc.SelectSingleNode ("//Backend[@Name='backend-conversion']");
				//get all the Conversion nodes
				XmlNodeList chains = config.SelectNodes ("//Conversion");

				//for all conversions
				foreach (XmlNode node in chains) {
					//check if conversion enabled
					bool enabled = Boolean.Parse (node.Attributes["Enabled"].InnerText);
					if (enabled) {
						Conversion c = new Conversion ();
						//all tags are mandatory
						c.To = node.Attributes["To"].InnerText;
						c.Factor = Double.Parse (node.Attributes["Factor"].InnerText);
						c.Add = Double.Parse (node.Attributes["Add"].InnerText);
		  
						string from = node.Attributes["From"].InnerText;
						conversions.Add (from,c);
					}
				}

				return true;
			} catch (Exception ex) {
				Console.WriteLine ("conversion Exception");
				Console.WriteLine (ex.Message);
				return false;
			}
		}

		public override BackendResult ProcessCluePacket (CluePacket cp)
		{
			//check if configuration has changed
			if (lastModified != File.GetLastWriteTime (path)) {
				conversions.Clear ();
				ReadConversion ();
			}
	  
			if (! this.Initialized)
				return null;
	  
			//not to display more than once the same conversion
			ArrayList done_conversions = new ArrayList ();

			BackendResult result = new BackendResult (this, cp);

			int match_cnt = 0;
			//for each clue in the cluepacket
			foreach (Clue clue in cp.Clues) {
				if (! ClueTypeSubscribed (clue))
					continue;

				if (clue.Text.Length == 0)
					return null;

				string clue_text = clue.Text;

				if (done_conversions.Contains (clue_text))
					continue;

				done_conversions.Add (clue_text);
				//split the clue text
				Regex re = new Regex (@" (-?[0-9,\.]+) * (.+)");
				System.Text.RegularExpressions.Match matches = re.Match (clue_text);
				//Console.WriteLine ("Conv--{0}",matches.Success);
				if (matches.Success) {
					string from = matches.Groups[2].Captures[0].ToString ();
					//Console.WriteLine ("Conv--{0}",from);
					Object o = conversions[from];
					//if we know how to convert this unit
					if (o != null) {
						try {
							Conversion conv = (Conversion) o;
							double value = Double.Parse (matches.Groups[1].Captures[0].ToString ());
			
							double new_value = value*conv.Factor+conv.Add;
							Match match = new Match ("Conversion"); // FIXME: Wrong category
							string fromtxt = string.Format ("{0:N2}{1}", value, from);
							match.AddItem (new MatchItem ("Original value", fromtxt, "text", "plain"));
							string totxt = string.Format ("{0:N2}{1}", new_value, conv.To);
							match.AddItem (new MatchItem ("Converted value", totxt, "text", "plain"));
							result.AddMatch (match, clue);
						} catch (FormatException ex) {
							//this wasn't a valid measure text
							continue;
						}
					}
				}
			}

			if (result.GetMatches ().Count == 0)

				return null;

			return result;
		}

	}
}

