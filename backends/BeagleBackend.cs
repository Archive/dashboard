//
// BeagleBackend.cs: Queries Beagle.  See module 'beagle'
// in GNOME CVS.
//
//
// Author:
//    Nat Friedman <nat@novell.com>
//    Joe Shaw <joeshaw@novell.com>
//


using System;
using System.IO;
using System.Collections;
using System.Threading;

using Beagle;

[assembly:Dashboard.BackendFactory ("Dashboard.BeagleBackend")]

namespace Dashboard {

	class BeagleBackend : Backend {
		private Beagle.Query query;
		private ArrayList hits;

		public override bool Startup ()
		{
			Console.WriteLine ("Beagle backend starting up...");

			Name = "Beagle";

			this.Initialized = true;
			
			SubscribeToClues ("*", "keyword", "uri", "textblock", "content");

			return true;
		}

		static Match HitToMatch (Beagle.Hit hit, CluePacket cp)
		{
			Match match = new Match (hit.Type, cp.Clues);
			string url_string = hit.Uri.ToString ();
			string local_path = hit.Uri.LocalPath;

			match ["URL"]      = url_string;
			match ["URI"]      = url_string;
			match ["MimeType"] = hit.MimeType;
			match ["Source"]   = hit.Source;
			match ["Score"]    = Convert.ToString (hit.Score);

			foreach (Property key in hit.Properties) {
				Console.WriteLine ("{0} => {1}", key.Key, key.Value);
				match [key.Key] = key.Value;
			}

			// Type-specific gunk
			switch (hit.Type) {

			case "File":
				match ["Path"] = local_path;
				Console.WriteLine ("{0}", local_path);
				match ["Text"] = local_path.Substring (local_path.LastIndexOf ('/') + 1);
				
				Gtk.IconSize size = (Gtk.IconSize) 48;
				string icon = Beagle.Util.GnomeFu.GetMimeIconPath (hit.MimeType);

				if (icon == null)
					icon = "internal:document.png";
				else
					icon = "file://" + icon;
				match ["Icon"] = icon;
				break;

			case "MailMessage":
				match ["Subject"] = match ["dc:title"];
				match ["Icon"] = "internal:mail.png";
				match ["Flags"] = match ["fixme:flags"];
				match ["SentDate"] = match ["fixme:date"];
				match ["To"] = match ["fixme:to"];
				match ["From"] = match ["fixme:from"];
				break;

			case "Contact":
				if (hit["Photo"] != null)
					match ["Photo"] = hit["Photo"];
				foreach (string key in hit.Properties)
					match [key] = hit [key];
				break;
			}

			Console.WriteLine ("match: {0}", match);

			return match;
		}

		private class QueryHandler {
			private ArrayList clues;
			private ArrayList hits;
			private bool finished;

			private object lock_obj;

			public QueryHandler ()
			{
				this.clues = new ArrayList ();
				this.lock_obj = new Object ();
			}

			private void OnHitAdded (Hit hit)
			{
				if (this.hits == null)
					this.hits = new ArrayList ();

				if (this.hits.Count < 10) {
					this.hits.Add (hit);
				} else if (!this.finished) {
					lock (this.lock_obj)
						Monitor.Pulse (this.lock_obj);
				}
			}

			private void OnHitsAdded (Beagle.HitsAddedResponse response)
			{
				foreach (Hit hit in response.Hits) {
					OnHitAdded (hit);
				}
			}

			private bool StartQuery ()
			{
				Beagle.Query query;

				this.hits = null;
				this.finished = false;

				query = new Beagle.Query ();

				query.AddDomain (Beagle.QueryDomain.Neighborhood);
				query.AddDomain (Beagle.QueryDomain.Global); // Probably want to make this a setting

				query.HitsAddedEvent += OnHitsAdded;

				foreach (Clue clue in this.clues) {
					string clue_text = clue.Text.ToLower ();

					query.AddText (clue_text);
				}

				query.SendAsync ();

				return false;
			}

			public void Start ()
			{
				GLib.Idle.Add (new GLib.IdleHandler (StartQuery));

				lock (this.lock_obj) {
					Monitor.Wait (this.lock_obj);
				}
			}

			public ArrayList Clues {
				get { return this.clues; }
			}

			public ArrayList Hits {
				get { return this.hits; }
			}
		}


		public override BackendResult ProcessCluePacket (CluePacket cp)
		{
			Console.WriteLine ("Got to ProcessCluePacket");

			if (! this.Initialized)
				return null;

			BackendResult result = new BackendResult (this, cp);

			QueryHandler qh = new QueryHandler ();

			foreach (Clue clue in cp.Clues) {
				if (!this.ClueTypeSubscribed (clue))
					continue;

				qh.Clues.Add (clue);
			}

			// This will block until its finished.
			qh.Start();

			foreach (Beagle.Hit hit in qh.Hits) {
				Match match = HitToMatch (hit, cp);
				result.AddMatch (match);
			}

			return result;
		}
	}
}
