using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections;
using System.Runtime.InteropServices;
using System.Xml;
using System.Text.RegularExpressions;

[assembly:Dashboard.BackendFactory ("Dashboard.GaimLogBackend")]

namespace Dashboard {

	class GaimLogBackend : BackendSimple {
		private Hashtable BuddyIndexes = new Hashtable ();

		public override bool Startup ()
		{
			Name = "Instant Messenger Logs";

			int convcount = LoadLogs ();

			Console.WriteLine ("Gaimlog backend loaded {0} conversations",
					   convcount);

			this.SubscribeToClues ("aim_name",
					       "yahoo_name",
					       "msn_name",
					       "icq_name",
					       "jabber_name",
					       "keyword",
					       "word_at_point", 
					       "textblock");

			this.Initialized = true;

			return true;
		}

		protected override ArrayList ProcessClueSimple (Clue clue)
		{
			ArrayList matches = new ArrayList();

			GaimLogIndex idx;

			try {
				idx = (GaimLogIndex) BuddyIndexes [clue.Text.ToLower ()];
			} catch {
				Console.WriteLine ("Could not get buddy index for {0}", clue.Text);
				return null;
			}

			if (idx == null)
				return null;

			idx.UpdateIndex ();

			// Generate matches
			Hashtable BuddyProperties = GetPropertiesOfBuddy (idx.Buddyname);
			string name = (string)BuddyProperties["alias"];
			string icon = (string)BuddyProperties["icon"];

			for (int i = 0; i < idx.Convs.Count && i < 5; i++) {
				Match match = new Match ("IMLog", clue);

				match ["Icon"]  = icon;
				match ["Alias"] = name;
				match ["Date"]  = ((GaimLogIndexEntry) idx.Convs [i]).Date;
				match ["URL"]   = "file://" + GenerateTmpFile (idx, i);
				match ["Title"] = String.Format ("IM Log for {0} at {1}", name, match ["date"]);

				matches.Add (match);
			}

			return matches;
		}

		private string gaim_icon_path = null;

		private string GetGaimIcon ()
		{
			if (gaim_icon_path != null)
				return gaim_icon_path;

			Gnome.IconTheme theme = new Gnome.IconTheme();
			Gnome.IconData icondata = new Gnome.IconData();
			int basesize = 0;

			theme.AllowSvg = true;
			
			gaim_icon_path = theme.LookupIcon ("gaim", 64, icondata, out basesize);

			return gaim_icon_path;
		}

		private ArrayList GetBuddyList ()
		{
			ArrayList Buddys = new ArrayList ();

			string home_dir = Environment.GetEnvironmentVariable ("HOME");
			string path = Path.Combine (home_dir, ".gaim/logs/");

			string [] buddys;

			try {
				buddys = Directory.GetFiles (path, "*.log");
			} catch {
				Console.WriteLine ("GaimLog backend: No gaim logs available");
				return Buddys;  // empty
			}

			foreach (string buddy in buddys) {
				string buddy_name = buddy.Substring (path.Length, buddy.Length - path.Length);
				buddy_name = buddy_name.Substring (0, buddy_name.LastIndexOf ('.'));
				Buddys.Add (buddy_name);
			}

			return Buddys;
		}

		private int LoadLogs ()
		{
			ArrayList Buddys = GetBuddyList ();

			int count = 0;
			foreach (string buddy in Buddys) {
				GaimLogIndex idx = new GaimLogIndex (buddy);
				idx.UpdateIndex ();
				BuddyIndexes.Add (buddy, idx);
				count += idx.Convs.Count;
			}

			return count;
		}


		private Hashtable GetPropertiesOfBuddy (string buddy)
		{
			string home_dir = Environment.GetEnvironmentVariable ("HOME");
			string path = Path.Combine (home_dir, ".gaim/blist.xml");

			Console.WriteLine ("Getting properties for buddy: " + buddy);

			Hashtable BuddyProperties = new Hashtable ();

			string icon_path = "";
			string alias = buddy;

			try {
				XmlDocument blist = new XmlDocument ();
				blist.Load (path);

				XmlNodeList nodes = blist.SelectNodes ("/gaim/blist/group/contact/buddy/name");
				foreach (XmlNode node in nodes) {
					if (String.Compare (node.InnerText.ToLower().Replace(" ",""), buddy.ToLower().Replace(" ","")) == 0) {
						XmlNode icon_node = node.ParentNode.SelectSingleNode ("setting[@name='buddy_icon']");
						if (icon_node != null)
							icon_path = icon_node.InnerText;

						XmlNode alias_node = node.ParentNode.SelectSingleNode ("alias");
						if (alias_node != null)
							alias = alias_node.InnerText;
						
						break;
					}
				}
			} catch {
				Console.WriteLine ("Exception getting properties for buddy");
			}

			if (icon_path == "") {
				icon_path = GetGaimIcon ();
			}

			BuddyProperties.Add ("alias", alias);
			BuddyProperties.Add ("icon", icon_path);

			return BuddyProperties;
		}

		private string GenerateTmpFile (GaimLogIndex idx, int conv_num)
		{
			string home;
			string path;

			home = Environment.GetEnvironmentVariable ("HOME");
			path = Path.Combine (home,
					     String.Format (".dashboard/tmp/{0}-{1}.html", idx.Buddyname, conv_num));

			FileStream s;

			try {
				s = File.Open (path, FileMode.Create);
			} catch {
				return "";
			}

			StreamWriter sw = new StreamWriter (s);

			string html = Regex.Replace (idx.GetLogEntryText (conv_num), "\n", "<br>\n");

			sw.WriteLine ("<html bgcolor=#ffffff><body><h2>Conversation with {0}</h2><h3>{1}</h3>",
				      idx.Buddyname, ((GaimLogIndexEntry) idx.Convs [conv_num]).Date);
			sw.WriteLine (html);
			sw.WriteLine ("</body></html>");
			sw.Flush ();
			sw.Close ();

			return path;
		}
	}

	//
	// Gaim stores all conversation logs with a given buddy
	// in an enormous flat file.  See ~/.gaim/logs.  The purpose
	// of this class is to index those logs so that searching for
	// recent conversations with a given buddy is super fast and
	// so we don't use much memory.
	//
	// IM Log indexes are stored in files with names like:
	//
	//        ~/.dashboard/backend-data/gaimlog/buddyname.idx
	//
	// They contain lines of comma-separated data in this form:
	//
	//        Date,StartOffset,EndOffset
	//
	// Where "date" is the time string for when the conversation
	// began, "StartOffset" is the byte offset into the log for
	// the beginning of the conversation, and "EndOffset" is the
	// offset into the log for the end of the conversation.
	//
	// The last conversation in the file will have an EndOffset of
	// -1, since more lines may be added after it is indexed.  So
	// "-1" means "read to the end of the file."
	//
	// This code currently assumes that gaim's .log files are in
	// chronological order (least to most recent).
	//
	class GaimLogIndexEntry {
		public string Date;
		public long StartOffset;
		public long EndOffset;
	}

	class LogStreamReader : StreamReader {
		public long Offset = -1;

		private StreamReader sr;

		public LogStreamReader (Stream s) : base(s) {
			sr = new StreamReader (s);
			Offset = s.Position;
		}

		public string ReadLineOff () {
			string s;
			try {
				s = sr.ReadLine ();
			} catch {
				s = "INVALID LINE";
			}
			if (s == null)
				return s;

			Offset += s.Length + 1;

			return s;
		}
	}
	
	class GaimLogIndex {

		// The buddy with whom all these conversations took
		// place.
		public string Buddyname;


		// The list of conversations.  We sort them from most
		// recent (Convs[0]) to least recent
		// (Convs[Convs.Count]).
		public ArrayList Convs = new ArrayList ();

		// Whether or not we have loaded the index file.
		private bool IndexLoaded = false;

		// start of conversation
		private const string CONV_START
			= "---- New Conversation @ ";

		// regex for finding date
		private const string REGEX_DATE
			= "Conversation @ (\\S+\\s+\\S+\\s+\\S+\\s+\\S+\\s+\\S+)";

		private Regex dateregex;

		public GaimLogIndex (string buddyname)
		{
			Buddyname = buddyname;
			dateregex = new Regex (REGEX_DATE,
					RegexOptions.IgnoreCase | RegexOptions.Compiled);
		}

		private string GetIndexPath ()
		{
			string home_dir = Environment.GetEnvironmentVariable ("HOME");
			string path = Path.Combine (home_dir, ".dashboard/backend-data/gaimlog/" + Buddyname + ".idx");

			return path;
		}

		private string GetLogPath ()
		{
			string home_dir = Environment.GetEnvironmentVariable ("HOME");
			string path = Path.Combine (home_dir, ".gaim/logs/" + Buddyname + ".log");

			return path;
		}

		public bool IndexExists ()
		{
			Stream s;

			try {
				s = File.Open (GetIndexPath (), FileMode.Open, FileAccess.Read);
			} catch {
				return false;
			}

			s.Close ();

			// Make sure the file is not zero-length.
			FileInfo fi = new FileInfo (GetIndexPath ());

			if (fi.Length == 0)
				return false;

			return true;
		}

		public bool LoadIndex ()
		{

			if (IndexLoaded)
				return true;

			Stream s;
			try {
				s = File.Open (GetIndexPath (), FileMode.Open, FileAccess.Read);
			} catch {
				return false;
			}

			StreamReader sr = new StreamReader (s);

			string idx_line;
			while ((idx_line = sr.ReadLine ()) != null) {
				string [] ary = idx_line.Split (',');

				GaimLogIndexEntry conv = new GaimLogIndexEntry ();
				conv.Date        = ary [0];
				conv.StartOffset = Int32.Parse (ary [1]);
				conv.EndOffset   = Int32.Parse (ary [2]);

				Convs.Insert (0, conv);
			}

			s.Close ();

			IndexLoaded = true;

			return true;
		}

		public bool IndexUpToDate ()
		{

			if (! IndexExists ())
				return false;

			//
			// Load the index if it hasn't been loaded.
			//
			LoadIndex ();

			DateTime index_changed_time = File.GetLastWriteTime (GetIndexPath ());
			DateTime log_changed_time   = File.GetLastWriteTime (GetLogPath ());

			if (DateTime.Compare (log_changed_time, index_changed_time) <= 0)
				return true;

			return false;
		}

		public long GetLastConvStartIndex ()
		{
			if (Convs.Count == 0)
				return 0;

			return ((GaimLogIndexEntry)Convs [0]).StartOffset;
		}

		private string ExtractDateFromLine (string logline)
		{
			System.Text.RegularExpressions.Match m;
			m = dateregex.Match (logline);
			if (m.Success) 
				return m.Groups[1].ToString ();
			else
				return "NO DATE";
		}

		private bool LineMarksConversationStart (string logline)
		{
			// FIXME: Nasty hardcoded crap
			if (logline.IndexOf (CONV_START) >= 0) {
				return true;
			}

			return false;
		}

		private void CreateFirstEntry (LogStreamReader logstreamr, string date)
		{
			if (Convs.Count != 0) {
				Console.WriteLine ("There is already a first entry!");
				return;
			}
			
			GaimLogIndexEntry conv = new GaimLogIndexEntry ();

			conv.Date        = date;
			conv.StartOffset = logstreamr.Offset;
			conv.EndOffset   = -1;

			Convs.Insert (0, conv);
		}

		public string SkipHeaderText (LogStreamReader logstream)
		{
			string logline;

			while ((logline = logstream.ReadLineOff ()) != null) {
				if (LineMarksConversationStart (logline))
					return ExtractDateFromLine (logline);
			}

			Console.WriteLine ("Couldn't find conversation start!");
			return "NO DATE";
		}

		public bool UpdateIndex ()
		{

			if (IndexUpToDate ())
				return true;

			Console.WriteLine ("GaimLog backend: Updating conversation index for {0}", Buddyname);

			//
			// Open the logfile.
			//
			FileStream logstream;
			try {
				logstream = File.OpenRead (GetLogPath ());
			} catch {
				return false;
			}

			//
			// Seek to the beginning of the last conversation.
			//
			logstream.Seek (GetLastConvStartIndex (), SeekOrigin.Begin);
			LogStreamReader logstreamr = new LogStreamReader (logstream);

			long current_index;

			//
			// If there is no index file, we need to skip
			// through the header text before marking the
			// beginning of the first conversation.
			//
			if (! IndexExists ()) {
				string date = SkipHeaderText (logstreamr);
				CreateFirstEntry (logstreamr, date);
			}

			string logline;
			while ((logline = logstreamr.ReadLineOff ()) != null) {
				long curr_offset;

				if (! LineMarksConversationStart (logline))
					continue;

				//
				// Mark the beginning of the new conversation.
				//
				GaimLogIndexEntry conv = new GaimLogIndexEntry ();
				conv.Date = ExtractDateFromLine (logline);
				conv.StartOffset = logstreamr.Offset;
				conv.EndOffset   = -1;

				//
				// Drop it at the front of the array.
				//
				Convs.Insert (0, conv);
			}

			logstream.Close ();

			//
			// Write out a new index.
			//
			return SaveToDisk ();
		}

		public bool CreateIndexDirectory ()
		{
			string home_dir = Environment.GetEnvironmentVariable ("HOME");

			string path1 = Path.Combine (home_dir, ".dashboard/backend-data");
			string path2 = Path.Combine (home_dir, ".dashboard/backend-data/gaimlog");
			try {
				Directory.CreateDirectory (path1);
			} catch {
			}

			try {
				Directory.CreateDirectory (path2);
			} catch {
				Console.WriteLine ("Could not create index directory");
				return false;
			}

			return true;
		}

		public bool SaveToDisk ()
		{
			FileStream s;

			if (!CreateIndexDirectory ())
				return false;

			try {
				s = File.Open (GetIndexPath (), FileMode.Create);
			} catch {
				return false;
			}

			StreamWriter sw = new StreamWriter (s);

			for (int i = (Convs.Count - 1); i >= 0; i --) {
				GaimLogIndexEntry entry = (GaimLogIndexEntry) Convs [i];
				if (i > 0) {
					GaimLogIndexEntry nextent = (GaimLogIndexEntry) Convs [i-1];
					entry.EndOffset = nextent.StartOffset;
				}
				sw.WriteLine (String.Format ("{0},{1},{2}",
							     entry.Date,
							     entry.StartOffset,
							     entry.EndOffset));
			}

			sw.Flush ();
			sw.Close ();

			return true;
		}

		public string GetLogEntryText (int entry_idx)
		{
			GaimLogIndexEntry conv;
			Stream s;

			conv = (GaimLogIndexEntry) Convs [entry_idx];

			try {
				s = File.OpenRead (GetLogPath ());
			} catch {
				Console.WriteLine ("Could not open log file {0} for reading", GetLogPath ());
				return "";
			}

			long conv_length;
			if (conv.EndOffset != -1)
				conv_length = conv.EndOffset - conv.StartOffset;
			else
				conv_length = s.Length - conv.StartOffset;

			byte [] conv_text = new byte [conv_length];

			s.Seek (conv.StartOffset, SeekOrigin.Begin);
			s.Read (conv_text, 0, (int) conv_length);

			s.Close ();

			return Encoding.Default.GetString (conv_text, 0, (int) conv_length);
		}
	}


///	class Driver {
///		static void Main (string [] args)
///		{
///			GaimLog g = new GaimLog ();
///			g.Startup ();
//			GaimLogIndex idx = new GaimLogIndex ("robflynn43");
//
//			idx.UpdateIndex ();
//
//			for (int i = 0; i < idx.Convs.Count; i ++) {
//				Console.WriteLine ("\nEntry #{0} ({1}):", i, ((GaimLogIndexEntry) idx.Convs [i]).Date);
//				Console.WriteLine (idx.GetLogEntryText (i));
//			}
//		}
//	}
}
