// Geosites backend: generates web links to related geographical
// resources
// by Edd Dumbill <edd@usefulinc.com>

using System;
using System.IO;
using System.Net;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Threading;
using System.Runtime.Remoting.Messaging;
using System.Reflection;
using System.Runtime.InteropServices;

using Dashboard.Geo;

[assembly:Dashboard.BackendFactory ("Dashboard.GeoSites")]

namespace Dashboard {

	class GeoSites : Backend {

		private Gazetteer gazetteer;
		private Hashtable us_states;

		public override bool Startup ()
		{
			Console.WriteLine ("GeoSites backend starting");
			Name = "GeoSites";

			Assembly assembly = Assembly.GetExecutingAssembly ();
			System.IO.Stream s = assembly.GetManifestResourceStream ("us_abbr_states.txt");
		
			us_states = new Hashtable (67);
			StreamReader sr = new StreamReader (s);

			string l = sr.ReadLine ();

			while(l != null)
			{
				string [] fields = l.Split ('\t');
				us_states.Add (fields [0], fields [1]);
				l = sr.ReadLine ();
			}

			s.Close ();	

			this.SubscribeToClues ("latlong");
			this.Initialized = true;

			gazetteer = new Gazetteer ();

			return true;
		}

		public Match GetMatch (Clue clue)
		{
			string txt = clue.Text;
			string [] coords = Regex.Split(txt, "\\s*,\\s*");

			if (coords.Length != 2)
				return null;

			string lat = coords[0];
			string lng = coords[1];

			double dlat = Double.Parse (lat);
			double dlng = Double.Parse (lng);

			int intlat = (int) dlat;
			int intlng = (int) dlng;
			
			Point p = gazetteer.NearestCity (dlat, dlng);
			if (p == null)
				return null;

			Match match = new Match ("Location", clue);
			match ["City"] = p.City;
			match ["Admin region"] = p.Adminregion;
			match ["Country"] = p.Country;

			String vicinityurl = String.Format("http://www.vicinity.com/myblast/map.mb?CMD=LFILL&CT={0}:{1}:90000", lat, lng);
			match ["MapBlast"] = vicinityurl;

			String mapquesturl = "http://www.mapquest.com/maps/map.adp?city=" + HttpUtility.UrlEncode (p.City);

			// yawn, special case for mapquest
			if (p.Country == "United States of America")
			{
				p.Country = "United States";
				mapquesturl +=  "&state=" + HttpUtility.UrlEncode ((string)us_states [p.Adminregion]);
			}
			else
				mapquesturl +=  "&state=" + HttpUtility.UrlEncode (p.Adminregion);

			mapquesturl += 	"&country=" + HttpUtility.UrlEncode (p.Country) + "&zoom=5";

			match ["MapQuest"] = mapquesturl;
			
			// yawn, special case for wunderground
			// what is it with US-centricity? ;-p
			String searchstring;
			if (p.Country == "United States")
			{
				searchstring = String.Format("{0}, {1}", p.City, p.Adminregion);
			} else {
				searchstring = String.Format("{0}, {1}", p.City, p.Country);
			}
			String wundergroundurl = String.Format(
			    "http://www.wunderground.com/cgi-bin/findweather/getForecast?query={0}",
			    HttpUtility.UrlEncode(searchstring));
			match ["Weather"] = wundergroundurl;
			
			String degreeconfluenceurl = String.Format(
			    "http://www.confluence.org/confluence.php?lat={0}&lon={1}&visit=1", intlat, intlng);
			match ["Degree Confluence"] = degreeconfluenceurl;

			String terraserverurl = String.Format("http://terraserver.com/coordinates2.asp?y={0}&x={1}", lat, lng);
			match ["Terraserver"] = terraserverurl;

			return match;
		}

		public override BackendResult ProcessCluePacket (CluePacket cp)
		{
			BackendResult result = new BackendResult (this, cp);

			if (! this.Initialized)
				return null;

			foreach (Clue c in cp.Clues) {
				if (c.Text.Length == 0)
					continue;

				result.AddMatch (GetMatch (c));
			}

			return result;
		}

		public static string Header (string title) {
			return String.Format (
					"      <table border=\"0\" height=\"100%\" width=\"100%\" valign=\"center\"> <tr> <td>" + 
            "       <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"100%\">" + 
            "         <tr bgcolor=\"#000000\"> <td>" + 
            "         <table border=\"0\" cellspacing=\"1\" cellpadding=\"0\" width=\"100%\">" + 
            "           <tr bgcolor=\"#f0f0f0\"> <td>" + 
            "           <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">" + 
            "             <tr bgcolor=\"#303030\"> " + 
            "                             <td valign=\"center\" align=\"left\">" + 
            "                     <img valign=\"center\" src=\"internal:globe.png\">" + 
            "                 </td>" + 
            "                             <td valign=\"center\" align=\"left\" width=100%>" + 
            "                     <font color=\"#ffffff\" size=\"+2\">{0}</font>"  + 
            "                 </td>" + 
            "             </tr>" + 
            "           </table>" + 
            "<table border=\"0\" width=\"100%\">",
			HttpUtility.HtmlEncode (title));
		}

		public static string Footer () 
		{
			return "</table></td></tr></table>" + 
                "</td></tr></table></td></tr></table>";
		}

	}

}

