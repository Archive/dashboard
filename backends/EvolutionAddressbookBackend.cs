//
// GNOME Dashboard
//
// EvolutionAddressbookbackend.cs: Evolution 2.0 addressbook backend.
//
// Author:
//   Nat Friedman <nat@nat.org> 
//
//

using System;
using System.Collections;
using GLib;
using Evolution;

[assembly:Dashboard.BackendFactory ("Dashboard.EvolutionAddressbookBackend")]

namespace Dashboard {
	public class EvolutionAddressbookBackend : Backend {

		Evolution.Book addressbook;
		bool lazy_initted = false;

		public override bool Startup ()
		{
			Name = "Evolution Addressbook";

			this.SubscribeToClues ("full_name", 
					       "email", 
					       "org", 
					       "url", 
					       "phone", 
					       "address", 
					       "rss", 
					       "aim_name", 
					       "yahoo_name", 
					       "jabber_name", 
					       "msn_name", 
					       "icq_name", 
					       "keyword");

			this.Initialized = true;

			return true;
			
		}

		private void LazyInit ()
		{
			addressbook = new Evolution.Book ();
			addressbook.LoadLocalAddressbook ();

			lazy_initted = true;
		}

		public override BackendResult ProcessCluePacket (CluePacket cp)
		{
			BackendResult result = new BackendResult (this, cp);

			if (! lazy_initted)
				LazyInit ();

			foreach (Clue c in cp.Clues)
				this.ProcessClue (result, c);

			return result;
		}

		private void ProcessClue (BackendResult result, Clue c)
		{
			ArrayList contacts;

			Console.WriteLine ("Evolution 2.0 addressbook backend got clue: " + c.ToString ());

			contacts = FindContacts (c);

			foreach (Contact contact in contacts) {
				result.AddMatch (GenerateMatchForContact (contact, c));
			}
			GenerateClues (result, contacts, c);
		}

		private ArrayList FindContacts (Clue c)
		{

			BookQuery query = BuildQuery (c);

			return new ArrayList (addressbook.GetContacts (query));
		}

		private void GenerateResult (BackendResult result, ArrayList contacts, Clue c)
		{
		}

		private void GenerateClues (BackendResult result, ArrayList contacts, Clue c)
		{
			ArrayList clues = new ArrayList ();

			foreach (Contact contact in contacts)
				GenerateCluesForContact (clues, contact, c);

			result.AddChainedClues (clues);
		}

		private void GenerateCluesForContact (ArrayList clues, Contact contact, Clue c)
		{
			AddClue (clues, c, contact.GivenName + " " + contact.FamilyName,     "full_name");
			AddClue (clues, c, contact.Email1,       "email");

			if (contact.ImAim.Length > 0)
				AddClue (clues, c, contact.ImAim [0],        "aim_name");

			if (contact.ImMsn.Length > 0)
				AddClue (clues, c, contact.ImMsn [0],        "msn_name");

			if (contact.ImIcq.Length > 0)
				AddClue (clues, c, contact.ImIcq [0],        "icq_name");

			if (contact.ImJabber.Length > 0)
				AddClue (clues, c, contact.ImJabber [0],     "jabber_name");

			if (contact.ImYahoo.Length > 0)
				AddClue (clues, c, contact.ImYahoo [0],      "yahoo_name");
			
			//AddClue (clues, c, contact.HomepageUrl,  "url");
			//AddClue (clues, c, contact.BlogUrl,      "rss");
		}

		private void AddClue (ArrayList clues, Clue c, string field, string field_name)
		{
			if (field != null && field != "")
				clues.Add (new Clue (field_name, field, 10, c));
		}

		private Match GenerateMatchForContact (Contact contact, Clue c)
		{
			Match result = new Match ("Contact", c);

			if (contact.GivenName != null && contact.GivenName != "")
				result["Name"] = contact.GivenName;
			
			if (contact.FamilyName != null && contact.FamilyName != "")
				result["Name"] += " " + contact.FamilyName;

			if (contact.Email1 != null && contact.Email1 != "")
				result["EMail"] = contact.Email1;

			//if (contact.PrimaryPhone != null && contact.PrimaryPhone != "")
			//	result["Phone"] = contact.PrimaryPhone;

			//FIXME: To use the default renderer
			result["Text"] = result["Name"];

			return result;
		}

		private BookQuery BuildQuery (Clue c)
		{
			if (c.Type == "*" || c.Type == "keyword")
				return BookQuery.AnyFieldContains (c.Text);

//			if (c.Type == "phone")
//				return BookQuery.Andv (BookQuery.FieldTest (ContactField.PhonePrimary, BookQueryTest.Is, Clue.Text),
//						       BookQuery.FieldTest (ContactField.PhoneBusiness, BookQueryTest.Is, Clue.Text),
//						       BookQuery.FieldTest (ContactField.PhoneHome, BookQueryTest.Is, Clue.Text),
//						       BookQuery.FieldTest (ContactField.PhoneHome2, BookQueryTest.Is, Clue.Text),
//						       BookQuery.FieldTest (ContactField.PhoneHomeFax, BookQueryTest.Is, Clue.Text),
//						       BookQuery.FieldTest (ContactField.PhoneMobile, BookQueryTest.Is, Clue.Text),
//						       BookQuery.FieldTest (ContactField.PhonePager, BookQueryTest.Is, Clue.Text));

			ContactField field = MapClueTypeToField (c);

			return BookQuery.FieldTest (MapClueTypeToField (c), BookQueryTest.Is, c.Text);
		}

		private ContactField MapClueTypeToField (Clue c)
		{
			switch (c.Type) {
			case "email":
				return ContactField.Email;
			//case "url":
				//return ContactField.HomepageUrl;
			case "aim_name":
				return ContactField.ImAim;
			case "icq_name":
				return ContactField.ImIcq;
			case "msn_name":
				return ContactField.ImMsn;
			case "yahoo_name":
				return ContactField.ImYahoo;
			case "jabber_name":
				return ContactField.ImJabber;
			// FIXME: this is the main blog URL, not the RSS URL
			case "rss":
				return ContactField.BlogUrl;
			}

			return ContactField.FileAs;
		}

	}
}

