//
// GNOME Dashboard
//
// backend-textchain.cs: Produces no matches, but extracts a few
// simple clues from textblocks and chains them into the engine.
//
// Author:
//    Nat Friedman <nat@nat.org>
//

using System;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;
using Gtk;

[assembly:Dashboard.BackendFactory ("Dashboard.TextChainBackend")]

namespace Dashboard {

	class TextChainBackend : Backend {

		private const string REGEX_RSS
			= "((http|https://).*rss.*)";
            
		private const string REGEX_URL
			= "((((http|ftp|https)://)|(www|ftp)[-A-Za-z0-9]*\\.)[-A-Za-z0-9\\.]+(:[0-9]*)?(/)?)";

		// this regex found at http://www.dotnetforums.net/t49640.html
		private const string REGEX_EMAIL
			= "([\\w\\.\\-]+@[a-zA-Z0-9\\-]+(\\.[a-zA-Z0-9\\-]{1,})*(\\.[a-zA-Z]{2,3}){1,2})";

		private const string REGEX_PHONE_US
			= "(\\(?[2-9][0-9]{2}\\)?[-. ][1-9][0-9]{2}[-. ][0-9]{4})";

		private const string REGEX_PHONE
			= "([\\+\\(]?\\d(\\d|\\)|[\\-\\(\\. ]\\d){6,})[\\s$]";

		private const string REGEX_BUGZILLA
			= "#([1-9][0-9]*)";

		// some regexes to match dates. these probably won't match all dates,
		// or non-english dates, but it's a start.
		private const string MONTHS
			= "(?<month>(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\w*)";
		private const string REGEX_DATE1
			= "((?<day>\\d{1,4})(\\-|\\/)(?<month>\\d{1,2})(\\-|\\/)(?<year>\\d{1,4}))";
		private const string REGEX_DATE2
			= "((?<day>\\d{1,2})\\w*\\s"+MONTHS+"\\s((?<year>\\d{2}){1,2}))";
		private const string REGEX_DATE3
			= "("+MONTHS+"\\s(?<day>\\d{1,2})[\\s,]+(\\d\\d:{0,1}){0,3}\\D*(?<year>\\d{4}))";

		
		public override bool Startup ()
		{
			Name = "TextChain";

			SubscribeToClues ("textblock",
					  "htmlblock",
					  "content",
					  "sentence_at_point");
			Initialized = true;

			return true;
		}

		class TextBlockReceiver {
			public string Textblock = "";

			public bool ReceiveText (IntPtr obj, string data)
			{
				Textblock += data;
				return true;
			}
		}

		// convert htmlblocks to textblocks (if a translation does not
		// exist already) for allowing backends that don't know how to
		// handle html to process the text.
		void ConvertHtmlToText (CluePacket cp, ArrayList newclues)
		{
			ArrayList htmlblocks = cp.GetClues ("htmlblock");

			// remove textblocks already converted from htmlblocks
			// (as marked by the TriggeringClues field).
			foreach (Clue c in cp.GetClues ("textblock")) {
				if (c.TriggeringClues.Count == 1) {
					Clue src_c = (Clue) c.TriggeringClues [0];
					if (src_c.Type == "htmlblock")
						htmlblocks.Remove (src_c);
				}
			}

			foreach (Clue c in htmlblocks) {
				Gtk.HTML html2txt = new Gtk.HTML (c.Text);
				TextBlockReceiver recv = new TextBlockReceiver();

				html2txt.Export ("text/plain", new HTMLSaveReceiverFn (recv.ReceiveText));

				if (recv.Textblock.Length > 0) {
					newclues.Add (new Clue ("textblock", 
								recv.Textblock.TrimEnd ('\n'), 
								c.Relevance, 
								c));
				}
			}
		}

		public override BackendResult ProcessCluePacket (CluePacket cp)
		{
			BackendResult result = new BackendResult (this, cp);

			foreach (Clue clue in cp.Clues) {
				if (! ClueTypeSubscribed (clue))
					continue;

				// we normalize url clues to have their
				// URI schemes on the front
				
				ArrayList urlclues = new ArrayList ();
				urlclues.AddRange (GetPatternMatches (clue, "url", REGEX_URL));

				foreach (Clue c in urlclues) {
					Clue r = null;

					if (String.Compare (c.Text, 0, "www.", 0, 4, true) == 0) {
						r = new Clue (c.Type, "http://" + c.Text, c.Relevance, c);
					} else if (String.Compare (c.Text, 0, "ftp.", 0, 4, true) == 0) {
						r = new Clue (c.Type, "ftp://" + c.Text, c.Relevance, c);
					}

					if (r == null)
						result.AddChainedClue (c);
					else
						result.AddChainedClue (r);
				}

				ArrayList newclues = new ArrayList ();
				newclues.AddRange (GetPatternMatches (clue, "rss",      REGEX_RSS));
				newclues.AddRange (GetPatternMatches (clue, "email",    REGEX_EMAIL));
				newclues.AddRange (GetPatternMatches (clue, "phone",    REGEX_PHONE));
				newclues.AddRange (GetPatternMatches (clue, "bugzilla", REGEX_BUGZILLA));
				newclues.AddRange (GetPatternMatches (clue, "date", 	REGEX_DATE1));
				newclues.AddRange (GetPatternMatches (clue, "date", 	REGEX_DATE2));
				newclues.AddRange (GetPatternMatches (clue, "date", 	REGEX_DATE3));

				// convert htmlblocks to textblocks
				ConvertHtmlToText (cp, newclues);

				// Add new clues to match set
				result.AddChainedClues (newclues);
			}

			return result;
		}

		// convert dates to YYYY-MM-DD format. handle ambiguous dates by generating additional datestrings
		private String [] NormalizeDateString (Regex rx, System.Text.RegularExpressions.Match m) {
			String day   = m.Groups["day"].Value;
			String month = m.Groups["month"].Value;
			String year  = m.Groups["year"].Value;
			String [] results = new String[2];

			// swap day & year if they're (unambiguously) the wrong way round
			// useful for dealing with e.g. 02/05/78 and 78/05/02
			if (((day.Length == 4) && (year.Length <= 2)) ||
				((System.Convert.ToInt32 (day) > 31) && (System.Convert.ToInt32 (year) <= 31)) ||
				((year.Length == 1) && (day.Length == 2)) ) {
				day  = year;
				year = m.Groups["day"].Value;
			}
			
			// convert month to a number
			if (month.Length > 3)
				month = month.Substring (0, 3).ToLower ();
			if (month == "jan") month = "01";
			if (month == "feb") month = "02";
			if (month == "mar") month = "03";
			if (month == "apr") month = "04";
			if (month == "may") month = "05";
			if (month == "jun") month = "06";
			if (month == "jul") month = "07";
			if (month == "aug") month = "08";
			if (month == "sep") month = "09";
			if (month == "oct") month = "10";
			if (month == "nov") month = "11";
			if (month == "dec") month = "12";

			if (year.Length == 2 && System.Convert.ToInt32 (year) <  50)  year  = "20" + year; // totally arbitrary.
			if (year.Length == 2 && System.Convert.ToInt32 (year) >= 50)  year  = "19" + year; // oh well.
			if (month.Length == 1) month = "0" + month;
			if (day.Length == 1)   day   = "0" + day;

			results[0] = year + "-" + month + "-" + day;
			results[1] = null;
		
			// handle dates like 05/02/1994 - could be 5th Feb or 2nd May 1994
			// if ambiguous generate a second date clue
			if (rx.ToString () == REGEX_DATE1)
				if ((System.Convert.ToInt32 (day) <= 12) && (System.Convert.ToInt32 (month) <= 12))
					results[1] = year + "-" + day + "-" + month;

			Console.WriteLine("--------------------------");
			Console.WriteLine("{0} -> {1}", m.Groups[1].ToString (), results[0]);
			if (results[1] != null)
			Console.WriteLine("{0} -> {1}", m.Groups[1].ToString (), results[1]);
			Console.WriteLine("--------------------------");

			return results;
		}


		private ArrayList GetPatternMatches (Clue clue, string type, string regex)
		{
			Regex r;
			System.Text.RegularExpressions.Match m;

			r = new Regex (regex, RegexOptions.IgnoreCase | RegexOptions.Compiled);

			ArrayList newclues = new ArrayList ();
			for (m = r.Match (clue.Text); m.Success; m = m.NextMatch ()) {
				Console.WriteLine ("TextChainer chaining to type " + type + ": " + m.Groups[1].ToString ());
				if (type == "date")
					foreach (String d in NormalizeDateString (r, m)) {
						if (d != null)
							newclues.Add (new Clue (type, d, 10, clue));
					}
				else
					newclues.Add (new Clue (type, m.Groups[1].ToString (), 10, clue));
			}

			return newclues;
		}
	}
}
