//
// GNOME Dashboard
//
// This is now defunct, thanks to Dewey.
//
// EvolutionMailBackend.cs
//
// Author:
//    Nat Friedman <nat@nat.org>
//

using System;
using System.IO;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using Gnome;

[assembly:Dashboard.BackendFactory ("Dashboard.EvolutionMailBackend")]

namespace Dashboard {

	class EvolutionMailBackend : BackendSimple
	{
		Camel.Summary summary;

		string summary_path;

		public override bool Startup ()
		{
			Name = "Evolution Mail";

			summary_path = GetDefaultMailSummaryPath ();
			if (summary_path == null)
				return false;

			try {
				summary = Camel.Summary.load (summary_path);
			} catch (Exception e) {
				Console.WriteLine ("Could not load camel summary: " + summary_path + e);
				return false;
			}

			this.SubscribeToClues ("email", "full_name");

			this.Initialized = true;

			return true;
		}

		string GetDefaultMailSummaryPath ()
		{
			GConf.Client client = new GConf.Client ();
			string path = "";

			path = (string) client.Get ("/apps/evolution/shell/default_folders/mail_uri");

			if (path == "")
				return null;

			if (path.IndexOf ("file://") == 0)
				return Path.Combine (path.Substring (7), "mbox.ev-summary");

			return path;
		}

		protected override ArrayList ProcessClueSimple (Clue clue)
		{
			Console.WriteLine ("MAIL BACKEND GOT A CLUE");
			ArrayList mails = GetMatchingMails (clue.Text); 
			if (mails == null || mails.Count == 0)
				return null;

			return GenerateMatches (mails, clue);
		}

		// Takes an email addr or a full_name
		ArrayList GetMatchingMails (string text)
		{
			ArrayList mails = new ArrayList ();

			foreach (Camel.MessageInfo mi in summary.messages) {

				if (mi.from.IndexOf (text) == -1)
					continue;

				mails.Add (mi);
			}

			int n = Math.Min (mails.Count, 10);
			return mails.GetRange (mails.Count - n, n);
		}

		ArrayList GenerateMatches (ArrayList mails, Clue clue)
		{
			ArrayList matches = new ArrayList();

			foreach (Camel.MessageInfo mi in mails) {
					Match match = new Match ("MailMessage", clue);
					match ["Icon"]     = "internal:mail.png";
					match ["Title"]    = mi.subject;
					match ["Subject"]  = mi.subject;
					match ["From"]     = mi.from;
					match ["To"]       = mi.to;
					match ["SentDate"] = mi.sent;
					match ["UID"]      = mi.uid;
					match ["Flags"]    = mi.flags;

					matches.Add(match);
			}

			return matches;
		}

	}
}
