//
// backend-stockquotes.cs - a backend for obtaining stock quotes
// given an organisation ticker
//
// Written by Jim McDonald (Jim@mcdee.net)
//
// Cluepackets handled:
//   - org
//   - org_ticker
//   - *
//

using System;
using System.IO;
using System.Collections;
using System.Xml;
using System.Runtime.InteropServices;
using System.Net;

namespace Dashboard {

	//
	// NASDAQ have a stock quote system available that provides nice
	// friendly XML.  For example, the URL
	// 
	// http://quotes.nasdaq.com/quote.dll?page=xml&mode=stock&symbol=gs
	//
	// gives the stock information for Goldman Sachs
	//
	// Unfortunately there isn't anything quite so nice for translating
	// a company name into a ticker, so we do some HTML scraping in the
	// case where we have an organisation's name.
	
	class StockQuoteBackend : BackendSimple {

		private const string baseurl = "http://quotes.nasdaq.com/quote.dll?page=xml&mode=stock&symbol=";
		public override bool Startup ()
		{
			Name = "Stock Quotes";

			Console.WriteLine ("Stock quote module started");

			this.SubscribeToClues ("org", "org_ticker");

			this.Initialized = true;

			return true;
		}

		protected override ArrayList ProcessClueSimple (Clue clue)
		{
			Console.WriteLine ("Stock quote backend received a clue: {0},{1}", clue.Text, clue.Type);

			ArrayList tickers = new ArrayList();
			ArrayList matches = new ArrayList();

			if ((clue.Type == "org_ticker") ||
			    (clue.Type == "*"))
			{
				// Attempt to get a quote from this org ticker
				XmlDocument xml = GetQuoteXml(clue.Text);
				if (xml != null)
				{
					Match result = XmlQuoteToMatch(xml);
					if (result != null)
					{
						// Found a quote against the ticker, don't bother to go through org as well
						matches.Add(result);
						return matches;
					}
				}
			}
			if (clue.Type != "org_ticker") {
				ArrayList arrayres = GetTickersFromName(clue.Text);
				if (arrayres != null) {
					foreach (string ticker in arrayres) {
						// Avoid duplication
						if (ticker.ToLower() != clue.Text.ToLower())
							tickers.Add(ticker);
					}
				}
			}

			if (tickers.Count == 0)
			{
				return null;
			}

			Console.WriteLine("Have {0} ticker(s)", tickers.Count);

			foreach (string ticker in tickers)
			{
				XmlDocument xml = this.GetQuoteXml (ticker);
				if (xml == null)
				{
					continue;
				}
				matches.Add(XmlQuoteToMatch(xml));
				// output.AddBody (this.XmlQuoteToHtml (xml));
			}

			return matches;
		}

		// Take a ticker and get a suitable quote
		private XmlDocument GetQuoteXml (string ticker)
		{

			XmlDocument xml = new XmlDocument ();

			HttpWebRequest req = null;
			WebResponse resp = null;
			StreamReader input = null;
			string quotexml = null;
			try
			{
				req = (HttpWebRequest)WebRequest.Create (String.Format ("{0}{1}", baseurl, ticker));
				req.UserAgent = "GNOME Dashboard";
				resp = req.GetResponse ();
				input = new StreamReader (resp.GetResponseStream ());
				quotexml = input.ReadToEnd ();
				resp.Close();
				input.Close();
			} catch (Exception e) {
				// Probably timed out
				if (resp != null)
					resp.Close();

				if (input != null)
					input.Close();

				if (req != null)
					req.Abort();

				return null;
			}

			int startidx = quotexml.IndexOf ("<nasdaqamex-dot-com");
			if (startidx < 0)
			{
				return null;
			}

			quotexml = quotexml.Substring (startidx);

			try
			{
				xml.LoadXml (quotexml);
			}
			catch
			{
				Console.WriteLine ("Quote XML is not well-formed!");
				return null;
			}

			/* Confirm we got something useful back */
			XmlNode node = xml.SelectSingleNode("//issue-name");
			if (node == null)
			{
				return null;
			}

			return xml;
		}

		// Transform the XML in a quote into a backend result
		private Match XmlQuoteToMatch (XmlDocument xml)
		{
			string issue_name, ticker, last_sale, abs_chg, rel_chg, website;
			try
			{
				issue_name = this.GetXmlText (xml, "//issue-name");
				ticker     = this.GetXmlAttr (xml, "//equity-quote", "symbol");
				last_sale  = this.GetXmlText (xml, "//last-sale-price");
				abs_chg    = this.GetXmlText (xml, "//net-change-price");
				rel_chg    = this.GetXmlText (xml, "//net-change-pct");
				website    = this.GetXmlText (xml, "//issuer-web-site-url");
			}
			catch
			{
				Console.WriteLine ("Could not get quote fields");
				return null;
			}

			if (issue_name == null)
			{
				return null;
			}

			string chgclr = GetChgColour(abs_chg);

			Match result = new Match ("StockQuote");

			result.AddItem(new MatchItem("Name", issue_name, "text", "html", website));
			result.AddItem(new MatchItem("Ticker", ticker, "text", "plain"));
			result.AddItem(new MatchItem("Price", last_sale, "number", "plain"));
			string abschangestr = String.Format("<font color=\"{0}\">{1}</font>", chgclr, abs_chg);
			result.AddItem(new MatchItem("Absolute change", abschangestr, "number", "html"));
			string relchangestr = String.Format("<font color=\"{0}\">{1}</font>", chgclr, rel_chg);
			result.AddItem(new MatchItem("Percentage change", relchangestr, "number", "html"));

			return result;
		}

		// Transform the XML in a quote to HTML
		private string XmlQuoteToHtml (XmlDocument xml)
		{
			string issue_name, ticker, last_sale, abs_chg, rel_chg, website;

			try
			{
				issue_name = this.GetXmlText (xml, "//issue-name");
				ticker = this.GetXmlAttr (xml, "//equity-quote", "symbol");
				last_sale = this.GetXmlText (xml, "//last-sale-price");
				abs_chg = this.GetXmlText (xml, "//net-change-price");
				rel_chg = this.GetXmlText (xml, "//net-change-pct");
				website = this.GetXmlText (xml, "//issuer-web-site-url");
			}
			catch
			{
				Console.WriteLine ("Could not get quote fields");
				return null;
			}

			if (issue_name == null)
			{
				return null;
			}

			string chgclr = GetChgColour(abs_chg);

			string html = String.Format ("<tr>" +
			                             "  <th valign=\"center\" align=\"left\" colspan=\"4\">" +
			                             "    <a href=\"{0}\">{1}</a>" +
			                             "  </th>" +
			                             "</tr>" +
			                             "<tr>" +
			                             "  <td valign=\"center\" align=\"left\">" +
			                             "    {2}" +
			                             "  </td>" +
			                             "  <td valign=\"center\" align=\"right\">" +
			                             "    {3}" +
			                             "  </td>" +
			                             "  <td valign=\"center\" align=\"right\">" +
			                             "    <font color=\"{4}\">{5}</font>" +
			                             "  </td>" +
			                             "  <td valign=\"center\" align=\"right\">" +
			                             "    <font color=\"{4}\">{6}</font>" +
			                             "  </td>" +
			                             "</tr>",
			                             website, issue_name, ticker, last_sale, chgclr, abs_chg, rel_chg);
			return html;
		}

		// Helper function to get data from an XML element
		private string GetXmlText (XmlDocument xml, string tag)
		{
			XmlNode node;
			node = xml.SelectSingleNode (tag);

			if (node == null)
			{
				return null;
			}

			return node.InnerText;
		}

		// Helper function to get attribute data from an XML element
		private string GetXmlAttr (XmlDocument xml, string tag, string attrname)
		{
			XmlNode node;
			node = xml.SelectSingleNode (tag);

			if (node == null)
			{
				return null;
			}

			XmlAttributeCollection attrs = node.Attributes;
			if (attrs == null)
			{
				return null;
			}

			XmlAttribute attr = attrs[attrname];
			if (attr == null)
			{
				return null;
			}

			return attr.InnerXml;
		}

		// Traditional green good/red bad colour selection
		private string GetChgColour(string chg)
		{
			if ((chg == null) ||
			    (chg == "0.00") ||
			    (chg == "unch"))
			{
				return "#000000";
			}
			else if (chg.Substring (0, 1) == "-")
			{
				return "#ff0000";
			}
			else
			{
				return "#00ff00";
			}
		}

		// No nice XML for this so it's back to HTML scraping
		private ArrayList GetTickersFromName (string name)
		{
			ArrayList tickers = new ArrayList();

			string basereq = "http://www.nasdaq.com/asp/HomeSymLookup.asp?mode=stock&srch=name&contain=0&mode=stock&key=";
			string transhtml;
			try
			{
				HttpWebRequest req = (HttpWebRequest)WebRequest.Create (String.Format ("{0}{1}", basereq, name));
				req.UserAgent = "GNOME Dashboard";
				WebResponse resp = req.GetResponse ();
				StreamReader input = new StreamReader (resp.GetResponseStream ());
				transhtml = input.ReadToEnd ();

			}
			catch (Exception e)
			{
				// Probably timed out
				return null;
			}

			while (true)
			{
				int curidx = transhtml.IndexOf ("AddSymbol");
				if (curidx < 0)
				{
					break;
				}

				int length = transhtml.Substring(curidx+11).IndexOf("'");
				if (length < 0)
				{
					break;
				}

				string ticker = transhtml.Substring(curidx+11, length);

				// Get stock type information.  Info is 2 TDs along from
				// where we are now
				curidx += transhtml.Substring(curidx).IndexOf("<TD>") + 4;
				curidx += transhtml.Substring(curidx).IndexOf("<TD>") + 4;
				length = transhtml.Substring(curidx).IndexOf("</");
				string tickertype = transhtml.Substring(curidx, length);

				// Shift HTML marker along
				transhtml = transhtml.Substring(curidx);

				// Only take tickers for common stock
				if (tickertype != "Common Stock")
				{
					continue;
				}
				tickers.Add(ticker);
				Console.WriteLine ("Added {0} to ticker list", ticker);

			}
			return tickers;
		}

	}
}
