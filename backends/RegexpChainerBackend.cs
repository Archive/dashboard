/*Copyright (C) 2003 Pierre ANDREWS
*
*   Permission is hereby granted, free of charge, to any person obtaining a copy 
*   of this software and associated documentation files (the "Software"), to
*   deal in the Software without restriction, including without limitation the
*   rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
*   sell copies of the Software, and to permit persons to whom the Software is 
*   furnished to do so, subject to the following conditions:
*
*   The above copyright notice and this permission notice shall be included in 
*   all copies or substantial portions of the Software.
*
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
*   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
*   IN THE SOFTWARE.
*/

//RegexpChainer
//
//Backend for Dashboard
//Author: Pierre Andrews <mortimer.pa@urbanet.ch>
//
//Read the file 'backends/backends-config.xml' looking for the configuration:
//a list of 'Chain' node with two attributes:
//'Type' which describe to which type of clues the regexp will apply to
//'Regex' which is a regular expression in Perl5 format, 
// each group (between '(' and ')') will generate a keyword clue
//'Ignore' is a ',' separated list of group in the regexp to ignore. (this one is optional)

//You can also write a more complex chain with the attributes if you do not specify 'Type':
//'From' type of the incomming clue
//'To' type of the outcomming clue

//'Type' and 'From' can take a list of type separated by ','

// NOTE: you have to escape the characters (&,>,<) in these two fields for the XML parser to work.

//For example:
//<Backend Name="backend-regexpchainer">
//<Chain Type="url" Regex="http://www.google.com/search\?q=(.+)"/>
//</Backend>
// will generate keywords "test" for simple google query: 
// http://www.google.com/search?q=test taken from an URL clue.

using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Xml;
using System.IO;

namespace Dashboard {

  class RegexpChainerBackend : Backend {

	struct RegExpression {
	  public Regex re;
	  public ArrayList ignore_groups;
	  public string to;
	}

	//count each clue type that have reexp associated 
	protected Hashtable cluetype_count = new Hashtable(); //string cluetype->int cnt
	//a cluetype map to 0 to N regexp
	protected Hashtable regexp4cluetype = new Hashtable(); //string cluetype+ID -> RegExpression

	//configuration file info
	protected DateTime lastModified;
	protected string path;

	public override bool Startup ()
	{
	  Name = "RegexpChainer";

	  // Find out where to find this file.
		path  = ConfigPath("config.xml");
    		    
	  this.Initialized = ReadRegexp();
	  return this.Initialized;
	}
		
	protected bool ReadRegexp() {
	  try {
		XmlDocument doc = new XmlDocument ();
		doc.Load (path);

		lastModified = File.GetLastWriteTime(path);

		//get the right configuration node
		XmlNode config = doc.SelectSingleNode("//Backend[@Name='backend-regexpchainer']");
		//get all chain node
		XmlNodeList chains = config.SelectNodes("//Chain");

		ArrayList registered_type = new ArrayList();
		foreach(XmlNode node in chains) {
		  string regex = node.Attributes ["Regex"].InnerText;
		  XmlAttribute att = node.Attributes ["Type"];
		  string types;
		  string to = "keyword";
		  
		  //if we do not find 'Type' attribute, then we expect 'From' and 'To'
		  if (att != null) {
			types = att.InnerText.ToLower();
		  } else {
			types = node.Attributes["From"].InnerText.ToLower();
			to = node.Attributes["To"].InnerText.ToLower();
		  }

		  //try to find the Ignore list
		  XmlAttribute tmp = node.Attributes["Ignore"];
		  string[] ignore_groups;
		  if (tmp!=null) {
			ignore_groups = tmp.InnerText.Split(',',';');
		  } else {
			ignore_groups = new string[0];
		  }

		  //add the regex to all the 'From' types.
		  string[] types_tk = types.Split(',');
		  foreach(string type in types_tk) {
			int v;
			registered_type.Add(type);
			Object o = cluetype_count[type];
			if(o == null)
			  v = 0;
			else
			  v = (int)o;
			v++;
			
			//update type count
			cluetype_count[type] = v;
			
			//create the ID = type+cnt
			string id = string.Format("{0}{1}",type,v-1);
			RegExpression rexp = new RegExpression();
			rexp.re = new Regex(regex,RegexOptions.IgnoreCase | RegexOptions.Compiled);
			rexp.to = to;
			rexp.ignore_groups = new ArrayList();
			rexp.ignore_groups.AddRange(ignore_groups);
			
			//store the struct
			regexp4cluetype[id] = rexp;
		  }
		}
		//subscribe only to the interesting clues.
		//FIXME their is surely a cleaner way to get string type.
		this.SubscribeToClues ((string[])(registered_type.ToArray("".GetType())));
		/*Console.Write("RC--");
		  foreach(object s in registered_type)
		  Console.Write(" {0}",s);
		  Console.WriteLine();*/
		return true;
	  } catch (Exception ex) {
		Console.WriteLine("Regexchainer Exception");
		Console.WriteLine(ex.Message);
		return false;
	  }
	}

	public override BackendResult ProcessCluePacket (CluePacket cp)
	{
		if (!this.Initialized)
			return null;

		ArrayList newclues = new ArrayList();

	  //check if configuration has changed.
	  if (lastModified != File.GetLastWriteTime(path)) {
		cluetype_count.Clear();
		regexp4cluetype.Clear();
		ReadRegexp();
	  }
	  ArrayList chain = new ArrayList ();

		BackendResult result = new BackendResult (this, cp);

	  //process all clues
	  foreach (Clue c in cp.Clues) {
		  if (c.Text.Length == 0)
			  continue;

		result.AddChainedClues(ProcessClue(c), c);
	  }

	  return result;
	}
		
	protected IList ProcessClue(Clue c)
	{
	  ArrayList newClues = new ArrayList();
	  string type = c.Type;
	  Object o = cluetype_count[type];

	  //if the clue is not registered, return.
	  if(o == null)
		return newClues;

	  int cnt = (int)o;

	  //for all regex register for that type
	  for(int i=0;i<cnt;i++) {
		string tp = string.Format("{0}{1}",type,i);
		o = regexp4cluetype[tp];
		//if the ID is wrong (why?) ignore
		if(o == null)
		  continue;

		RegExpression rexp = (RegExpression)o;
		Regex re = rexp.re;
		ArrayList ignore = rexp.ignore_groups;
		System.Text.RegularExpressions.Match matches = re.Match(c.Text);
		int[] gnums = re.GetGroupNumbers();
		//check all matches
		while(matches.Success) {
		  //for each groups
		  for(int j=1;j<gnums.Length;j++) {
			//check if we have to ignore this group
			if (!ignore.Contains(string.Format("{0}",j)))
			  //for each capture of the group in the match (for example when you have '(abc)*')
			  foreach(Capture capt in matches.Groups[gnums[j]].Captures) {
				if(capt.ToString().Length >0)
				  newClues.Add (new Clue (rexp.to, capt.ToString(), 10, c));
			  }
		  }
		  matches = matches.NextMatch();
		} 
	  }
	  return newClues;
	}
  }
}
