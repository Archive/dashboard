using System;
using System.IO;
using System.Collections;

using Dashboard.Index;

namespace Dashboard {

	class HTMLIndexBackend : Backend {
		private IndexManager mgr;

		public override bool Startup ()
		{

			Console.WriteLine ("HTMLIndex backend starting up...");

			Name = "HTMLIndex";

			try {
				mgr = new IndexManager ("remote_html");
			}
			catch {
				Console.WriteLine ("HTMLIndex backend: ERROR LOADING.");

				this.Initialized = false;

				return false;
			}

			this.SubscribeToClues ("url", "htmlblock", "content");
			this.Initialized = mgr.Initialized;
			
			return this.Initialized;
		}

		public override Match ProcessCluePacket (CluePacket cp)
		{
			if (! this.Initialized) {
				Console.WriteLine ("not initialized, dude");

				return null;
			}

			if (! cp.Frontend.Equals ("Web Browser"))
				return null;

			Clue url = null;
			Clue content = null;

			foreach (Clue c in cp.Clues) {
				switch (c.Type) {
				case "url":
					url = c;
					break;
				case "content":
				case "htmlblock":
					content = c;
					break;
				}
			}

			if (url == null || content == null)
				return null;

			Console.WriteLine ("HTMLIndex: wishes to index {0}", url.Text);

			ArrayList clues;

			ArrayList matches = mgr.IndexAndRetrieve (new StringReader (content.Text), url.Text, "text/html", out clues);

			Console.WriteLine ("HTMLIndex: Got from query: {0}\n", matches.Count);

			if (matches.Count == 0)
				return null;

			string html = String.Format ("<table border=0 width=100%><tr><td><h2><u>Web History</u></h2></td></tr></table>");

			foreach (IndexMatch match in matches) {
				string mime = Gnome.VFS.Mime.GetMimeType (match.Source.URI);
				string icon = "internal:bookmark.png";

				if (mime == "text/html") 
					icon = "internal:bookmark.png";
				//				else {
				//					icon = Gnome.Icon.LookupByURI (match.Source.URI);
				//				}

				html = String.Format (
"<table width=100% border=0><tr>" +
"    <td valign=center align=left>" +
"        <a href=\"{1}\"><img src=\"{2}\" border=0></a>" +
"    </td><td>&nbsp;&nbsp;</td>" +
"    <td valign=top align=left>" +
"        <a href=\"{1}\" style=\"text-decoration: none;\">{0}<br>" +
"        <font size=-1 color=#666666>{1}</font></a>" +
"    </td>" +
"</tr></table>", String.Format ("{0} (Rel: {1})", PathToFilename (match.Source.URI), match.Relevance), match.Source.URI, icon);
			}

			html += "</table>";

			ArrayList match_matches = new ArrayList ();

			match_matches.Add (new Match (this, html, 0, Name, url, content));

			return new Match (match_matches, null);
		}

		private string PathToFilename (string path)
		{
			string[] split = path.Split ('/');
			return split [split.Length - 1];
		}
	}
}
