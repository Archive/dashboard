// FOAF backend
// by Edd Dumbill <edd@usefulinc.com>


// FIXME: implement basedNear stuff so we can chain latlong clues

using System;
using System.IO;
using System.Net;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Threading;
using System.Runtime.Remoting.Messaging;


using Dashboard.Rdf.Foaf;

namespace Dashboard {

	class FoafFileBackend : Backend {

		public override bool Startup ()
		{
			Console.WriteLine ("FoafFile backend starting");

			this.SubscribeToClues ("rdfurl", "foafid");
			this.Initialized = true;

			Name = "FoafFile";

			return true;
		}

		private string stripquotes (string txt)
		{
			// remove leading and closing '
			// replace \' with '
			// replace \\ with \
			if (txt.StartsWith ("'"))
				txt = txt.Substring (1);
			if (txt.EndsWith ("'"))
				txt = txt.Substring (0, txt.Length - 1);

			txt = txt.Replace ("\\'", "'");
			return txt.Replace ("\\\\", "\\");
		}

		public void GetFoafMatches (BackendResult result, Clue file,
																ArrayList refs)
		{
			ArrayList matches = new ArrayList ();
			ArrayList newclues = new ArrayList ();
			Store foaf;
			Regex wordregex = new Regex
					("^foaf:(\\w+)\\s+('?.*'?)$");

			try {
					foaf = new Store (file.Text);
					Console.WriteLine ("Retrieved RDF file {0}", file.Text);
			} catch {
					Console.WriteLine ("Ignoring invalid RDF file {0}", file.Text);
					return;
			}
			foreach (Clue c in refs) {
					String s = c.Text;

					System.Text.RegularExpressions.Match m =
							wordregex.Match (s);
					
					if (! m.Success)
							continue;

					string prop = Properties.NAMESPACE_URI + 
							m.Groups[1].ToString ();
					string val = m.Groups[2].ToString ();
					if (val.StartsWith ("'"))
							val = stripquotes(val);
					
					Console.WriteLine ("Person identified by {0} {1}",
							   prop, val);

					Match res = foaf.GenerateMatch (prop, val, c);
					if (res != null)
							result.AddMatch (res, c);

					result.AddChainedClues (foaf.GenerateClues (prop, val, c));
			}
		}

		public override BackendResult ProcessCluePacket (CluePacket cp)
		{
			BackendResult result = new BackendResult (this, cp);

			ArrayList files = new ArrayList ();
			ArrayList refs = new ArrayList ();

			if (! this.Initialized)
				return null;

			foreach (Clue c in cp.Clues) {
					if (c.Text.Length == 0)
							continue;

					// FIXME: this hack assumes that url always comes before
					// content in a block from a browser frontend.  really
					// we ought to examine all the clue content first.
					if (ClueTypeSubscribed (c)) {
							if (c.Type == "foafid")
									refs.Add (c);                                          
							
							if (c.Type == "rdfurl")
									files.Add (c);
					}
			}

			if (refs.Count > 0) {
					foreach (Clue file in files)
							GetFoafMatches (result, file, refs);
			} else {
					Console.WriteLine("FoafFile: No entries in refs, nothing to look for.");
			}

			return result;
		}
	}
}

