//
// backend-bugzilla.cs: A backend which displays bugzilla bugs based
// on their bug numbers.
//
// Nat Friedman <nat@nat.org>
//

using System;
using System.IO;
using System.Collections;
using System.Xml;
using System.Runtime.InteropServices;
using System.Net;
using System.Text.RegularExpressions;
using Gnome;

[assembly:Dashboard.BackendFactory ("Dashboard.BugzillaBackend")]

namespace Dashboard {

	class BugzillaBackend : BackendSimple {

		private string bugzilla_host = "http://bugzilla.ximian.com";

		public override bool Startup ()
		{
			Name = "Bugzilla";
		
			GConf.Client client = new GConf.Client ();
			try {
				string host = (string) client.Get ("/apps/dashboard/backends/bugzilla/host");
				if (host != "")
					bugzilla_host = host;
				else
					Console.WriteLine ("The GConf Bugzilla host key is empty !");
			}
			catch (GConf.NoSuchKeyException) {
				client.Set ("/apps/dashboard/backends/bugzilla/host","http://bugzilla.ximian.com");
				Console.WriteLine ("Setting default Bugzilla host");
			}
			
			this.SubscribeToClues ("bugzilla");
			this.Initialized = true;

			return true;
		}

		protected override ArrayList ProcessClueSimple (Clue clue)
		{
			ArrayList matches = new ArrayList ();

			XmlDocument xml = this.GetBugzillaXml (clue.Text);
			if (xml == null)
				return null;

			Match match = XmlBugToMatch (xml, clue);
			if (match != null)
			{
				matches.Add (match);
			}

			return matches;
		}

		private XmlDocument GetBugzillaXml (string bug) {

			// Confirm that the text we have been passed looks
			// like a bug ID (i.e. is numeric)
			Regex bugregex = new Regex ("[0-9]+", RegexOptions.Compiled);
		  	if (!bugregex.Match (bug).Success)
			{
				return null;
			}

			XmlDocument xml = new XmlDocument ();

			HttpWebRequest req = (HttpWebRequest)
				WebRequest.Create (String.Format ("{0}/xml.cgi?id={1}", bugzilla_host, bug));
									       
			req.UserAgent = "GNOME Dashboard";

			StreamReader input;
			try {
				WebResponse resp = req.GetResponse ();
				input = new StreamReader (resp.GetResponseStream ());
			} catch {
				return null;
			}

			string bugxml;

			try {
				bugxml = input.ReadToEnd ();
			} catch {
				return null;
			}

			int startidx = bugxml.IndexOf ("<bugzilla");
			if (startidx < 0)
				return null;
			
			bugxml = bugxml.Substring (startidx);

			try {
				xml.LoadXml (bugxml);
			} catch {
				Console.WriteLine ("Bugzilla XML is not well-formed!");
				return null;
			}
			
			return xml;
		}

		private Match XmlBugToMatch (XmlDocument xml, Clue c)
		{
			string bug_num, product, summary, owner, status;

			// see if the bug was even found. If there wasn't a bug, there will be
			// an error attribute on the /bug element that says NotFound if one didn't exist.
			if (!IsValidBug (xml))
				return null;
				
			try { 
				bug_num = this.GetXmlText (xml, "//bug_id");
				product = this.GetXmlText (xml, "//product");
				summary = this.GetXmlText (xml, "//short_desc");
				summary = summary.Substring (0, Math.Min (summary.Length, 50));
				owner   = this.GetXmlText (xml, "//assigned_to");
				status  = this.GetXmlText (xml, "//bug_status");
			} catch {
				Console.WriteLine ("Could not get bug fields");
				return null;
			}

			string bug_url = String.Format ("{0}/show_bug.cgi?id={1}", bugzilla_host, bug_num);

			Match match = new Match ("BugzillaBug", c);
			match ["Number"]  = bug_num;
			match ["Product"] = product;
			match ["Owner"]   = owner;
			match ["Summary"] = summary;
			match ["Status"]  = status;

			return match;
		}

		private string GetXmlText (XmlDocument xml, string tag)
		{
			XmlNode node;

			node = xml.SelectSingleNode (tag);
			if (node == null)
				return "???";

			return node.InnerText;
		}

		// Determines if the bug is valid or if we searched on some number
		// that we thought might have been a bugzilla number.
		// Rather than return all '???' let's just ignore them
		private bool IsValidBug (XmlDocument xml)
		{
			try {			
				XmlNode node;
				node = xml.SelectSingleNode ("/bugzilla/bug");
              	
				// if we can't find the "bug" element, then it's not valid
				if (node == null)
					return false;				

				XmlNode attrib;
				attrib = node.Attributes.GetNamedItem ("error");

				// if there's no error attribute, it's legit
				// Note: I don't know what possible values for "error" are.
				// I know if the bug isn't there, you will get 'NotFound' so I'm assuming that any
				// error attribute is bad
				
				if (attrib == null)
					return true;					
			} catch {
				// on error, assume it's not a bug:
				return false;
			}
			return false;
		}                                                                      
	}

} // namespace Dashboard
