//
// backend-ephybookmarks.cs: Epiphany bookmarks backend.  Reads from
// ~/.gnome2/epiphany/bookmarks.rdf.
//
// Authors:
//     Jonas Heylen <jonas.heylen@pandora.be> 
//


using System;
using System.IO;
using System.Xml;
using System.Collections;

[assembly:Dashboard.BackendFactory ("Dashboard.EpiphanyBookmarks")]

namespace Dashboard {

	class EpiphanyBookmarks : BackendSimple {
		XmlDocument         doc;
		XmlNodeList         items;
		XmlNamespaceManager nsm;

		XmlDocument         favicondoc;
		
		public override bool Startup ()
		{
			Name = "Epiphany bookmarks";

			string home_dir = Environment.GetEnvironmentVariable ("HOME");
			string path;

			try {
				path = Path.Combine (home_dir, ".gnome2/epiphany/bookmarks.rdf");

				if (! File.Exists (path))
					return false;

				doc = new XmlDocument ();
				doc.Load (path);

				nsm = new XmlNamespaceManager (doc.NameTable);
				nsm.AddNamespace ("rss", "http://purl.org/rss/1.0/");

				items = doc.SelectNodes ("//rss:item", nsm);

        path = Path.Combine (home_dir, ".gnome2/epiphany/ephy-favicon-cache.xml");
        if (File.Exists (path)) {
					favicondoc = new XmlDocument ();
					favicondoc.Load (path);
				}

				// FIXME: need to subscribe to clues that we
				// should try matching against. Probably
				// textblock and sentence_at_point:
				// this.SubscribeToClues ("textblock", "sentence_at_point");

				this.Initialized = true;

				return true;
			} catch {
				return false;
			}
		}

		protected override ArrayList ProcessClueSimple (Clue clue)
		{

			if (! this.Initialized)
				return null;

			if (clue.Text.Length == 0)
				return null;

			string clue_text = clue.Text.ToLower ();

			// Match list
			ArrayList matches = new ArrayList();

			int match_count = 0;

			foreach (XmlNode node in items) {

				string title = node.SelectSingleNode ("rss:title", nsm).InnerText;
				string lower = title.ToLower();
				
				if (lower.IndexOf (clue_text) == -1)
					continue;
				
				string uri = node.SelectSingleNode ("rss:link", nsm).InnerText;

				string iconuri = this.GetIconPath (uri);

				// Construct match set
				Match match = new Match ("WebLink", clue);
				match ["Emblem-Icon"] = iconuri;
				match ["Title"] = title;
				match ["URL"]   = uri;

				matches.Add(match);
				match_count++;
			}

			if (match_count == 0) {
				return null;
			} else {
				return matches;
			}
		}

		private string GetIconPath (string url)
		{
			if (favicondoc == null)
					return null;

			int index = url.IndexOf ("/", 7);
			if (index > -1)
			   url = url.Substring (0, index);

			string xpath = "descendant::node[starts-with(child::property[1]/child::text(), '" + url + "')]";
			XmlNode fav_node = favicondoc.SelectSingleNode (xpath);

			if (fav_node != null) {
				xpath = "child::property[position()=2]";
				XmlNode favicon = fav_node.SelectSingleNode (xpath);
				string home_dir = Environment.GetEnvironmentVariable ("HOME");
				string path = Path.Combine (home_dir, ".gnome2/epiphany/favicon_cache");
				return Path.Combine (path, favicon.InnerText);
			}

			return null;
		}
	}
}


