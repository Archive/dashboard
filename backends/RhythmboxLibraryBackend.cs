//
// backend-rhythmboxlibrary.cs: Rhythmbox Library backend.
//
// Authors:
//     Lee Willis <lee@leewillis.co.uk>
//
// Reads from ~/.gnome2/rhythmbox/rhythmdb.xml
// Uses Amazon Web Services for retrieval of album images, and for
// linking through to product information. If you change this file
// significantly please update the devtag in the amazon retrieval
// code - see http://www.amazon.com/gp/aws/landing.html/002-4684085-2369658
// for more information

using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Net;

namespace Dashboard {

	class SongMatch {
		public string title;
		public string album;
		public string track_number;
		public string artist;
	}

	class AlbumMatch {
		public string title;
		public string artist;
		public SortedList songs;
	}

	class AlbumWebInfo {
		public string art_url;
		public string album_url;
	}

	class RhythmboxLibraryBackend : BackendSimple {
		XmlDocument         doc;
		XmlNodeList         songs;
		
		public override bool Startup ()
		{
			Name = "Rhythmbox Library";

			string home_dir = Environment.GetEnvironmentVariable ("HOME");
			string path;

			try {
				path = Path.Combine (home_dir, ".gnome2/rhythmbox/rhythmdb.xml");

				if (!File.Exists (path)){
					return false;
				}

				doc = new XmlDocument ();
				doc.Load (path);

				songs = doc.SelectNodes ("/rhythmdb[@version=\"1.0\"]/entry");

				// Subscribe to artist clues
				this.SubscribeToClues ("artist");

				this.Initialized = true;

				return true;
			} catch {
				return false;
			}
		}

		protected override ArrayList ProcessClueSimple (Clue clue)
		{

			if (! this.Initialized)
				return null;

			if (clue.Text.Length == 0)
				return null;

			ArrayList matches = new ArrayList();
			SortedList AlbumArray = new SortedList();
			string clue_text = clue.Text.ToLower ();

			if (clue.Type == "artist") {

				// Find matching songs
				XmlNodeList ArtistMatches = doc.SelectNodes (String.Format("//entry[artist=\"{0}\"]",clue.Text));

				if (ArtistMatches.Count > 0) {

					foreach (XmlNode Song in ArtistMatches) {

						AlbumMatch thisAlbum;
						SongMatch thisMatch = new SongMatch();

						thisMatch.artist = clue.Text;
						thisMatch.album = Song ["album"].InnerText;
						thisMatch.title = Song ["title"].InnerText;
						thisMatch.track_number = Song["track-number"].InnerText;

						// See if there is an entry in AlbumArray for this album
						if (AlbumArray.ContainsKey(thisMatch.album)) {
							// If so then grab the AlbumMatch entry
							thisAlbum = (AlbumMatch)AlbumArray[thisMatch.album];
						} else {
							// 	Create a blank AlbumMatch entry
						 	thisAlbum = new AlbumMatch();
							thisAlbum.artist = thisMatch.artist;
							thisAlbum.title = thisMatch.album;
							thisAlbum.songs = new SortedList();
							AlbumArray.Add (thisAlbum.title, thisAlbum);
						}

						// Add the song to the AlbumMatch entry
						// FIXME This breaks if you have two tracks on the same album, claiming the same track number :(
						thisAlbum.songs.Add(Convert.ToInt32(thisMatch.track_number), thisMatch);
					}

				}
			}
			
			// Now we have a nicely sorted dataset we can create the match sets
			for (int i=0; i < AlbumArray.Count; i++) {
				AlbumMatch thisAlbum = (AlbumMatch)AlbumArray.GetByIndex(i);

				// Create a backendmatch
				Match AlbumMatch = new Match ("Song");

				AlbumWebInfo albumwebinfo = GetAlbumArtURLFromAlbumDetails(thisAlbum.artist, thisAlbum.title);

				if (albumwebinfo != null) {
					AlbumMatch.AddItem(new MatchItem("Icon", albumwebinfo.art_url, "image", "plain", albumwebinfo.album_url));
				}

				AlbumMatch.AddItem(new MatchItem("AlbumTitle", thisAlbum.title, "text", "plain"));

				// For each song on album
			 	for (int j=0; j < thisAlbum.songs.Count; j++) {

					// Create a backend sub match
					Match songMatch = new Match ("Song");

			 		SongMatch thisSong = (SongMatch)thisAlbum.songs.GetByIndex(j);

					songMatch.AddItem(new MatchItem("TrackNumber", thisSong.track_number, "number", "plain"));
					songMatch.AddItem(new MatchItem("TrackTitle", thisSong.title, "text", "plain"));

					// Add it the album match
					AlbumMatch.AddSubMatch(songMatch);
				}
				// Add the album match to the match set
				 matches.Add(AlbumMatch);
			}

			if (matches.Count > 0) {
				return matches;
			} else {
				return null;
			}
		}
	
		private AlbumWebInfo HMV_GetAlbumArtURLFromAlbumDetails (string artist, string album_title) {

			// Try to get the album image
			string baseurl = "http://www2.hmv.co.uk/hmvweb/advancedSearch.do?searchType=2&pGroupID=1&defaultPrimaryID=1%3A2&catalogue=&format=&primaryID=1&secondaryID=-1&label=&x=0&y=0";
			string response_html;
			int length;
			int jump_value;
			AlbumWebInfo match = new AlbumWebInfo();

			try
			{
				HttpWebRequest req = (HttpWebRequest)WebRequest.Create (String.Format ("{0}&artist={1}&title={2}", baseurl, artist, album_title));
				req.UserAgent = "GNOME Dashboard";
				WebResponse resp = req.GetResponse ();
				StreamReader input = new StreamReader (resp.GetResponseStream ());
				response_html = input.ReadToEnd ();
			}
			catch (Exception e)
			{
				return null;
			}
	
			// Skip to search matches
			int curidx = response_html.IndexOf ("<!-- search matches //-->");
			if (curidx < 0) {
				return null;
			}

			// Skip to the first match
			jump_value = response_html.Substring(curidx).IndexOf("href=");
			if (jump_value < 0) {
				return null;
			} else {
				curidx += jump_value + 6;
				length = response_html.Substring(curidx).IndexOf("\"");
				if (length > 0) {
					match.album_url = response_html.Substring(curidx,length);
				}
			}

			// Skip to the album image tag
			jump_value = response_html.Substring(curidx).IndexOf("img src");
			if (jump_value < 0) {
				return null;
			} else {
				curidx += jump_value + 9;
			}

			length = response_html.Substring(curidx).IndexOf("\"");
			if (length < 0) {
				return null;
			}
			match.art_url= response_html.Substring(curidx, length);

			return match;
	
		}
		private AlbumWebInfo Amazon_GetAlbumArtURLFromAlbumDetails (string artist, string album_title) {
			
			//Create Instance of Proxy Class
			AmazonSearchService searchService = new AmazonSearchService();
			
			// Handle multi-page matches
			int TotalPages = 1;
			int CurrentPage = 1;
			
			//Create Encapsulated Request
			ArtistRequest asearch = new ArtistRequest();
			asearch.devtag = "INSERT DEV TAG HERE";
			asearch.artist = artist;
			asearch.type = "heavy";
			asearch.mode = "music";
			asearch.tag = "webservices-20";
			
			//Console.WriteLine ("Trying to fetch image from Amazon");
			while (CurrentPage <= TotalPages) {

				asearch.page = Convert.ToString(CurrentPage);
				ProductInfo pi;
				try {
					pi = searchService.ArtistSearchRequest(asearch);
				} catch {
					return null;
				}
			
				int NumMatchs = pi.Details.Length;
				TotalPages = Convert.ToInt32(pi.TotalPages);

				// Work out how many matches are on this page
				if (NumMatchs < 1) {
					return null;
				}

				AlbumWebInfo match = new AlbumWebInfo();
				for (int i=0; i < (NumMatchs-1); i++) {
					//Console.WriteLine ("Comparing album name to {0}", pi.Details[i].ProductName);
					if (pi.Details[i].ProductName.ToLower() != album_title.ToLower()) {
						continue;
					}
					//Console.WriteLine ("Found ...");
					match.art_url = pi.Details[i].ImageUrlMedium;
					match.album_url = pi.Details[i].Url;
					return match;
				
				}

				CurrentPage++;
			}
			return null;

		}
		private AlbumWebInfo GetAlbumArtURLFromAlbumDetails (string artist, string album_title) {
			return Amazon_GetAlbumArtURLFromAlbumDetails (artist, album_title);
		}
	}

}
