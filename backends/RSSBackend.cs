//
// backend-rss.cs: A backend which displays recent entries from an RSS
// feed.
//
// Nat Friedman <nat@nat.org>
// Edd Dumbill <edd@usefulinc.com>
// 
// Continuing Display Cleanup and Other Fixes
// Ryan Skadberg <skadz@stigmata.org>
//

using System;
using System.IO;
using System.Collections;
using System.Xml;
using System.Runtime.InteropServices;
using System.Net;

[assembly:Dashboard.BackendFactory ("Dashboard.RSSBackend")]

namespace Dashboard {

	class RSSBackend : BackendSimple {

		public override bool Startup ()
		{
			Name = "RSS";

			Console.WriteLine ("RSS Backend starting up");

			this.SubscribeToClues ("rss");
			this.Initialized = true;

			return true;
		}

		protected override ArrayList ProcessClueSimple (Clue clue)
		{
			ArrayList matches = new ArrayList();

			XmlDocument xml = this.GetRSS (clue.Text);
			if (xml == null)
				return matches;

			// Initialize namespaces for parsing an RSS 1.0 feed
			XmlNamespaceManager ns = 
				new XmlNamespaceManager(xml.NameTable);
			ns.AddNamespace ("rss", 
					"http://purl.org/rss/1.0/");
			ns.AddNamespace ("rdf", 
					"http://www.w3.org/1999/02/22-rdf-syntax-ns#");
			ns.AddNamespace ("dc", 
					"http://purl.org/dc/elements/1.1/");

			string title;
			string description;
			string link;
			string guid;
			ArrayList items;

			// check whether we're processing RSS 1.0 or not
			if (xml.SelectSingleNode ("/rdf:RDF/rss:channel", ns) != null) {
				// yes, rdf:RDF/rss:channel is there
				Console.WriteLine ("Found an RSS 1.0 feed.");
				description = this.GetXmlNodeText (xml, 
						"/rdf:RDF/rss:channel/rss:description", ns);
				title = this.GetXmlNodeText (xml, 
						"/rdf:RDF/rss:channel/rss:title", ns);
				link  = this.GetXmlNodeText (xml,
						"/rdf:RDF/rss:channel/rss:link", ns);
				guid  = this.GetXmlNodeText (xml,
						"/rdf:RDF/rss:channel/rss:guid", ns);
				items = this.GetRecentRSSItems (xml, ns);
			} else {
				// no, we assume RSS 0.91 or 2.0
				Console.WriteLine ("Found an RSS 0.91/2.0 feed.");
				title = this.GetXmlNodeText (xml, "//title");
				description = this.GetXmlNodeText (xml, "//description");
				link  = this.GetXmlNodeText (xml, "//link");
				guid  = this.GetXmlNodeText (xml, "//guid");
				items = this.GetRecentRSSItems (xml);
			}

			if (items.Count == 0)
				return matches;

			foreach (RSSItem item in items) {
				if (item.Link == null || item.Link.Length == 0)
					continue;

				// Some items don't have titles.  In that case, we attach the URL
				// to the description.  Unless the item doesn't have a description,
				// in which case we give up (it stops becoming interesting without
				// anything to tell what it actually is)
				int linktodescription;
				if (item.Title == null || item.Title.Length == 0) {
					linktodescription = 1;
					if (item.Description == null || item.Description.Length == 0)
						continue;
				} else
					linktodescription = 0;

				Match match = new Match ("RSS", clue);
				match ["Title"] = title;
				match ["Description"] = description;

				if (item.Title != null && item.Title.Length > 0)
					match ["ItemTitle"] = item.Title;

				if (item.Description != null && item.Description.Length > 0)
					match ["Description"] = item.Description;

				match ["WebLink"] = item.Link;

				if (guid != null)
					match ["GUID"] = guid;

				matches.Add (match);
			}

			return matches;
		}

		private XmlDocument GetRSS (string url) {

			if (url == null || url.Length == 0 || url.IndexOf ("://") < 0)
				return null;

			HttpWebRequest req = (HttpWebRequest) WebRequest.Create (url);
			req.UserAgent = "GNOME Dashboard";
			WebResponse resp = req.GetResponse ();
			StreamReader input = new StreamReader 
				(resp.GetResponseStream ());

			string rss = input.ReadToEnd ();

			XmlDocument xml = new XmlDocument ();
			
			try {
				xml.LoadXml (rss);
			} catch (Exception e) {
				Console.WriteLine ("RSS Parsing Exception {0}", e);
				return null;
			}

			return xml;
		}

		private ArrayList GetRecentRSSItems (XmlDocument xml) {
			ArrayList items = new ArrayList ();
			XmlNodeList items_xml = xml.SelectNodes ("//item");
			foreach (XmlNode item_xml in items_xml) {
Console.WriteLine("1");
				string guid    = this.GetXmlNodeText (item_xml, "guid");
				string link    = this.GetXmlNodeText (item_xml, "link");
				string title   = this.GetXmlNodeText (item_xml, "title");
				string description   = this.GetXmlNodeText (item_xml, "description");
				string pubdate = this.GetXmlNodeText (item_xml, "pubDate");

				items.Add (new RSSItem (guid, link, title, description, pubdate));
			}

			return items;
		}

		// RSS 1.0 specific way to get items
		// Added in some support for GUIDs, which is how some RSS 2.0 feeds do it (like mine)
		private ArrayList GetRecentRSSItems (XmlDocument xml,
				XmlNamespaceManager ns) {

			ArrayList items = new ArrayList ();
			XmlNodeList items_xml = xml.SelectNodes ("/rdf:RDF/rss:item", ns);
			foreach (XmlNode item_xml in items_xml) {
				string guid    = this.GetXmlNodeText (item_xml, "rss:guid", ns);
				string link    = this.GetXmlNodeText (item_xml, "rss:link", ns);
				string title   = this.GetXmlNodeText (item_xml, "rss:title", ns);
				string description   = this.GetXmlNodeText (item_xml, "rss:description", ns);
				string pubdate = this.GetXmlNodeText (item_xml, "dc:date", ns);

				items.Add (new RSSItem (guid, link, title, description, pubdate));
			}

			return items;
		}

		private string GetXmlNodeText (XmlNode xml, string tag,	XmlNamespaceManager ns)
		{
			XmlNode node;

			node = xml.SelectSingleNode (tag, ns);
			if (node == null)
				return "";

			return node.InnerText;
		}

		private string GetXmlNodeText (XmlNode xml, string tag)
		{
			XmlNode node;

			node = xml.SelectSingleNode (tag);
			if (node == null)
				return "";

			return node.InnerText;
		}

		private string StripHTML (string myhtml) {
			System.Text.RegularExpressions.Regex tags = new System.Text.RegularExpressions.Regex(@"<[^>]+>|</[^>]+>");
			string mytext = tags.Replace(myhtml, "");
			return mytext;
		}

	} // class RSS

	class RSSItem {
		public string Guid;
		public string Link;
		public string Title;
		public string Description;
		public string PubDate;

		public RSSItem (string Guid, string Link, string Title, string Description, string PubDate) {
			this.Guid        = Guid;
			this.Link        = Link;
			this.Title       = Title;
			this.Description = Description;
			this.PubDate     = PubDate;
		}
	}


} // namespace Dashboard
