using System;
using System.IO;
using System.Xml;
using System.Net;
using System.Web;
using System.Text;
using System.Collections;

// HTTPBridge for Dashboard
// by Edd Dumbill <edd@usefulinc.com>
//
// cobbled together from other backends, the C# docs, and
// the dark recesses of my mind.
// thanks to Martijn van Beers for his assistance.
//
// enables an HTTP backend to be plugged into Dashboard
// the backend sends two queries to the target url,
// with the clue sent as XML like this (for instance):
// <Question><Clue Type="phone">+447786123456</Clue></Question>
//
// this is sent form encoded (like the contents of a <textarea>
// to a posted form (hint: this is an easy way to test your backend)
//
// if the parameter name is 'query', then you should send the HTML
// which goes to dashboard
//
// if the parameter name is 'chain' then you should send cluechaining
// instructions.  they look like this:
// <Chain>
//  <Match>
//	<Clue Type="phone" Relevance="10">+447786123456</Clue>
//  </Match>
// </Chain>
//
// each <Match> container causes a CluePacket to be constructed.
//
// in either 'query' or 'chain' case, returning a 0-length
// response means no match was found
//
// all communication should use the UTF-8 character encoding
// 
// TODO: 
//     configurability so more than one HTTPBridge can be used.
//     parameters are likely {baseURI, [cluetype list]} pair
//
// FIXME:
//     Server is hardwired to localhost 8001 right now.
//
//	   if the network request fails, the empty match should
//	   probably not be cached


namespace Dashboard {

	class HTTPBridgeBackend : Backend {

		string BaseURI;
		int active_queries=0;

		public override bool Startup ()
		{

			Console.WriteLine ("HTTPBridge backend starting");

			this.Initialized = true;
			this.BaseURI = "http://localhost:8001/dashboard";

			Name = "HTTPBridge-" + BaseURI;

			return true;
		}

		public ArrayList GenerateNewClues (string chainpacket)
		{
			ArrayList clues = new ArrayList ();

			XmlDocument xml = new XmlDocument();
			xml.LoadXml(chainpacket);

			XmlNodeList matches = xml.SelectNodes("//Match");

			foreach (XmlNode mnode in matches) {
				XmlNodeList xclues =
					mnode.SelectNodes("//Clue");

				foreach (XmlNode xclue in xclues) {
					clues.Add (new Clue (
					xclue.Attributes ["Type"].InnerText,
					xclue.InnerText,
					10));   
				}
			}
			return clues;
		}

		public string MakeRequest (string rtype, string question)
		{
			string postData = rtype+"="+HttpUtility.UrlEncodeUnicode(question);

			WebRequest myHttpWebRequest = WebRequest.Create(
					"http://localhost:8001/dashboard");

			UTF8Encoding encoding=new UTF8Encoding();
			byte[]  byte1=encoding.GetBytes(postData);
			// Set the content type of the data being posted.
			myHttpWebRequest.ContentType=
				"application/x-www-form-urlencoded";
			// Set the content length of the string being posted.
			myHttpWebRequest.ContentLength=postData.Length;
			myHttpWebRequest.Method="POST";

			string html = "";

			try {
				Stream newStream=myHttpWebRequest.GetRequestStream();
				newStream.Write(byte1,0,byte1.Length);
				newStream.Close();
				WebResponse resp=myHttpWebRequest.GetResponse();
				Stream ReceiveStream = resp.GetResponseStream();
				Encoding encode = Encoding.GetEncoding("utf-8");
				StreamReader readStream = new StreamReader(
						ReceiveStream, encode);
				Char[] read = new Char[256];
				int count = readStream.Read(read, 0, 256);
				while (count > 0) {
					html = html + new String(read, 0, count);
					count = readStream.Read(read, 0, 256);
				}
				readStream.Close();
				resp.Close();
			} catch (Exception e) {
				// we normally get here because either there's nothing
				// to read, or there's a network error.
				// Console.WriteLine ("HTTP backend request failed, {0}", e);
				html = "";
			}

			return html;
		}

		public override Match ProcessCluePacket (CluePacket cp)
		{
			ArrayList matches = new ArrayList ();
			ArrayList newclues = new ArrayList ();

			if (! this.Initialized)
				return null;

			StringBuilder question = new StringBuilder ();
			int clues = 0;

			question.Append ("<Question>\n");

			foreach (Clue c in cp.Clues) {
				if (c.Text.Length == 0)
					continue;

				question.Append (String.Format
						("<Clue Type=\"{0}\">{1}</Clue>\n",
						 c.Type, HttpUtility.HtmlEncode(c.Text)));
				clues++;
			}

			if (clues > 0) {
				question.Append("</Question>\n");

				//				Console.WriteLine (
				//					String.Format("HTTPBridge asking question {0}",
				//						question));

				string html = MakeRequest("query", question.ToString ());

				//				Console.WriteLine (
				//					String.Format("HTTPBridge got answer {0}",
				//						html));

				if (html.Length > 0) {
					string newcluexml = MakeRequest
						("chain", question.ToString ());

					Console.WriteLine (
						String.Format("HTTPBridge got chained clues {0}",
							newcluexml));

					newclues.AddRange (GenerateNewClues (newcluexml));

					matches.Add (new Match (this, html, 0,
							  "HTTPBridge backend matches"));
				}
			}

			return new Match (matches, newclues);
		}

	}

}
