// HTML chainer
// by Edd Dumbill <edd@usefulinc.com>
//
// sniffs tags from the <head> of HTML documents and chains clues
// based on what it finds
//
// currently supported:
//   <link rel="alternate" type="application/rdf+xml" title="RSS"
//       href="http://usefulinc.com/edd/blog/rss" />
//   <meta name="description" content="yada yada yada" />
//   <meta name="keywords" content="word1, word2, ... wordN " />
//   <meta name="DC.Creator" content="Full Name" />
//   <meta name="DC.Contributor" content="Full Name" />
//   <link rel="made" href="mailto:foo@bar.com" />
//   <link rel="made" href="http://www.bar.com/" />
//
// todo:
//
//   <meta name="foaf:maker" content="foaf:mbox mailto:foo@bar.com" />
//   <meta name="ICBM" content="51.374852, -2.382584" />
using System;
using System.IO;
using System.Net;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Threading;
using System.Runtime.Remoting.Messaging;

namespace Dashboard {

	class HtmlChainer : Backend {

		private static string REGEX_LINK =
			"<link\\s+((\\S+\\s*=\\s*\"[^\"]+\"\\s*)+)/?>";

		private static string REGEX_META =
			"<meta\\s+((\\S+\\s*=\\s*\"[^\"]+\"\\s*)+)/?>";

		private static string REGEX_TITLE =
			"title\\s*=\\s*\"([^\"]+)\"";

		private static string REGEX_REL =
			"rel\\s*=\\s*\"([^\"]+)\"";

		private static string REGEX_HREF =
			"href\\s*=\\s*\"([^\"]+)\"";

		private static string REGEX_TYPE =
			"type\\s*=\\s*\"([^\"]+)\"";

		private static string REGEX_NAME =
			"name\\s*=\\s*\"([^\"]+)\"";

		private static string REGEX_CONTENT =
			"content\\s*=\\s*\"([^\"]+)\"";

		public override bool Startup ()
		{
			Console.WriteLine ("HtmlChainer backend starting");

			this.SubscribeToClues ("url", "content", "htmlblock");
			this.Initialized = true;

			Name = "HtmlChainer";
			Category = "Chainer";
			IconPath = null;

			return true;
		}

		// Simple routine to resolve relative links
		public string GroundURI (string path, Uri baseuri)
		{
			// are we already qualified?
			if (baseuri == null || path.IndexOf("://") > 0)
				return path;

			Uri u = new Uri (baseuri, path);
			return u.ToString ();
		}

		public ArrayList GetClues (string txt, Uri baseuri, Clue c)
		{
			ArrayList clues = new ArrayList ();
			
			// set up regexen
			Regex regex_link = new Regex (REGEX_LINK,
				RegexOptions.IgnoreCase | RegexOptions.Compiled |
				RegexOptions.Multiline);

			Regex regex_meta = new Regex (REGEX_META,
				RegexOptions.IgnoreCase | RegexOptions.Compiled |
				RegexOptions.Multiline);

			Regex regex_title = new Regex (REGEX_TITLE,
				RegexOptions.IgnoreCase | RegexOptions.Compiled |
				RegexOptions.Multiline);

			Regex regex_rel = new Regex (REGEX_REL,
				RegexOptions.IgnoreCase | RegexOptions.Compiled |
				RegexOptions.Multiline);

			Regex regex_href = new Regex (REGEX_HREF,
				RegexOptions.IgnoreCase | RegexOptions.Compiled |
				RegexOptions.Multiline);

			Regex regex_type = new Regex (REGEX_TYPE,
				RegexOptions.IgnoreCase | RegexOptions.Compiled |
				RegexOptions.Multiline);

			Regex regex_name = new Regex (REGEX_NAME,
				RegexOptions.IgnoreCase | RegexOptions.Compiled |
				RegexOptions.Multiline);

			Regex regex_content = new Regex (REGEX_CONTENT,
				RegexOptions.IgnoreCase | RegexOptions.Compiled |
				RegexOptions.Multiline);

			System.Text.RegularExpressions.MatchCollection links;

			links = regex_link.Matches (txt);

			foreach (System.Text.RegularExpressions.Match m in links) {
				// Console.WriteLine ("Got match {0}",
				//		m.Groups[1].ToString ());
				System.Text.RegularExpressions.Match rel =
					regex_rel.Match (m.Groups[1].ToString ());
				System.Text.RegularExpressions.Match href =
					regex_href.Match (m.Groups[1].ToString ());
				System.Text.RegularExpressions.Match title =
					regex_title.Match (m.Groups[1].ToString ());
				System.Text.RegularExpressions.Match type =
					regex_type.Match (m.Groups[1].ToString ());

				if (! (rel.Success))
					continue;

				string reltext = rel.Groups[1].ToString ().ToLower ();

				string typetext = null;
				if (type.Success)
					typetext = type.Groups[1].ToString ().ToLower ();

				string titletext = null;
				if (title.Success)
					titletext = title.Groups[1].ToString ().ToLower ();

				string hreftext = null;
				if (href.Success)
					hreftext = href.Groups[1].ToString ();
				
				if (reltext == "alternate" && 
						(typetext == "application/rss+xml" || 
					 	 titletext == "rss")) {
					clues.Add (new Clue ("rss",
								GroundURI (hreftext, baseuri),
								10, c));
					continue;
				}

				if (reltext == "made" && hreftext != null) {
					if (hreftext.StartsWith("mailto:"))
						clues.Add (new Clue ("email",
								hreftext.Substring (7), 10, c));
					else
						clues.Add (new Clue ("url", 
									GroundURI (hreftext, baseuri),
									10, c));
				}

				if (reltext == "meta" &&
						typetext == "application/rdf+xml" &&
						hreftext != null) {
					clues.Add (new Clue ("rdfurl",
								GroundURI (hreftext, baseuri),
								10, c));
					// foaffiles.Add (GroundURI (hreftext, baseuri));
				}
			}

			System.Text.RegularExpressions.MatchCollection metas;
			metas = regex_meta.Matches (txt);

			foreach (System.Text.RegularExpressions.Match m in metas) {
				// Console.WriteLine ("Got match {0}",
				//		m.Groups[1].ToString ());

				System.Text.RegularExpressions.Match name =
					regex_name.Match (m.Groups[1].ToString ());

				System.Text.RegularExpressions.Match content =
					regex_content.Match (m.Groups[1].ToString ());

				if (! (name.Success && content.Success))
					continue;

				string nametext = name.Groups[1].ToString ().ToLower ();
				string contenttext = content.Groups[1].ToString ();

				switch (nametext) {
				case "dc.creator":
					clues.Add (new Clue ("full_name", contenttext, 10, c));
					break;
				case "dc.contributor":
					clues.Add (new Clue ("full_name", contenttext, 10, c));
					break;
				case "description":
					clues.Add (new Clue ("textblock", contenttext, 10, c));
					break;
				case "keywords":
					// FIXME: limit this to 4 or so, any more can cause
					// a DoS on CPU time with Dashboard :)
					string [] words = Regex.Split (contenttext, "\\s*,\\s*");
					foreach (string w in words)
						clues.Add (new Clue ("keyword", w, 10, c));
					break;
				case "foaf:maker":
					clues.Add (new Clue ("foafid", contenttext.Trim (), 10, c));
					break;
				case "icbm":
				case "geo.position":
					// latitude, longitude hints
					string [] coords = Regex.Split
						(contenttext, "\\s*[;,]\\s*");
					// FIXME: implement validation on these values
					clues.Add (new Clue ("latlong",
								String.Format("{0},{1}", coords[0],
									coords[1]), 10, c));
					break;
				}
			}

			return clues;
		}

		public override BackendResultSet ProcessCluePacket (CluePacket cp)
		{
			if (! this.Initialized)
				return null;

			ArrayList newclues = new ArrayList ();
			Uri baseuri = null;

			BackendResultSet resultset = new BackendResultSet(this, cp);

			foreach (Clue c in cp.Clues) {
				if (c.Text.Length == 0)
					continue;

				// FIXME: this hack assumes that url always comes before
				// content in a block from a browser frontend.  really
				// we ought to examine all the clue content first.
				if (c.Type == "url")
					baseuri = new Uri (c.Text);

				if (c.Type != "content" &&
				    c.Type != "htmlblock")
				{
					continue;
				}

				int endhead=0;

				// find end of the head, a really long body
				// seems to upset the regex engine
				endhead = c.Text.IndexOf("</head>");

				if (endhead < 0)
					endhead = c.Text.IndexOf("</HEAD>");

				if (endhead >= 0) {
					resultset.AddChainedClues(GetClues (c.Text.Substring (0, endhead), baseuri, c), c);
				}
			}

			return resultset;
		}

		// Main () method is for testing use outside of
		// Dashboard. Compile to an executable and run with
		// no arguments.
		//   mcs backend-htmlchainer.cs -r:System.Web \
		//   -r:../engine/dashboard.exe
		// run with
		//   MONO_PATH=../engine/ ./backend-htmlchainer.exe
		//
		// For fidelity with the way it gets called in Dashboard,
		// we test inside a thread.
		
		static AsyncCallback extract_done;
		delegate void ExtractLinksHandler (HtmlChainer chainer);

		static void Main (string [] args)
		{
			HtmlChainer chainer = new HtmlChainer ();

			extract_done = new AsyncCallback (ExtractLinksDone);

			ExtractLinksHandler extract_links = new ExtractLinksHandler (ExtractLinks);
			extract_links.BeginInvoke (chainer, extract_done, null);
			
			// ExtractLinks (chainer);
		}

		static void ExtractLinks (HtmlChainer chainer)
		{
			ArrayList urls = new ArrayList();
			// urls.Add ("http://www.diveintomark.org/");
			urls.Add ("http://usefulinc.com/edd/blog");
			urls.Add ("http://pants.heddley.com/logica/");

			foreach (string url in urls) {
			
				HttpWebRequest req = (HttpWebRequest) WebRequest.Create (url);
				req.UserAgent = "GNOME Dashboard";
				WebResponse resp = req.GetResponse ();
				StreamReader input = new StreamReader 
					(resp.GetResponseStream (),
					 Encoding.GetEncoding ("utf-8"));

				string html = input.ReadToEnd ();

				Console.WriteLine ("Fetched " + url);


				ArrayList cl = chainer.GetClues (html, new Uri (url), null);

				Console.WriteLine ("Found these clues");

				foreach (Clue c in cl) {
					Console.WriteLine (c.ToString ());
				}
			}
		}

		static void ExtractLinksDone (IAsyncResult ar)
		{
			ExtractLinksHandler extract_links = (ExtractLinksHandler)
				(((AsyncResult)ar).AsyncDelegate);
			Console.WriteLine ("ExtractLinksDone called");
			try {
				extract_links.EndInvoke (ar);
			} catch (Exception e) {
				Console.WriteLine ("Exception: {0}", e);
			}
		}

	}
}
