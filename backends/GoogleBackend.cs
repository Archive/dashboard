//
// backend-google.cs: Google backend.
//
// Authors:
//     Don Smith <donsmith77@optonline.net>
//

using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Net;

[assembly:Dashboard.BackendFactory ("Dashboard.GoogleBackend")]

namespace Dashboard {

	class GoogleBackend : BackendSimple {

		// Set your key below.  You can get a Google API key
		// at http://www.google.com/apis/.
		string google_api_key = "INSERT DEVTAG HERE"; 
		
		public override bool Startup ()
		{
			Name = "Google";

			if (google_api_key.Length == 0)
				return false;

			if (google_api_key == "INSERT DEVTAG HERE"){
                                Console.WriteLine ("Please put in a Google Dev Tag");
				return false;
                        }

			try {
				// Subscribe to artist clues
				this.SubscribeToClues ("keyword");

				this.Initialized = true;

				return true;
			} catch {
				return false;
			}
		}

		protected override ArrayList ProcessClueSimple (Clue clue)
		{
                        int max_results = 10;
                        int start_result = 0;
                        bool dupe_filter = true;
                        bool safe_search = true;

                        if (! this.Initialized)
				return null;

			if (clue.Text.Length == 0)
				return null;

			ArrayList results = new ArrayList ();
                        GoogleSearchResult gsr;
                        GoogleSearchService searchService = new GoogleSearchService (); 

                        try {
                        	gsr = searchService.doGoogleSearch
					(google_api_key,
					 clue.Text,
					 start_result,
					 max_results,
					 dupe_filter,
					 "",
					 safe_search,
					 "", "", ""); 
                        } catch {
                        	return null;
                        }

                        for (int i = 0; i < max_results; i++) {
                        	Match GoogleMatch = new Match ("WebLink", clue);
				GoogleMatch ["Title"] =	gsr.resultElements [i].title;
				GoogleMatch ["URL"] = gsr.resultElements [i].URL;
				GoogleMatch ["Icon"] = "internal:bookmark.png";
                        	results.Add (GoogleMatch); 
                        }

			if (results.Count > 0)
				return results;
			else
				return null;
                }	
	}
}
