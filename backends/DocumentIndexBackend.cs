using System;
using System.IO;
using System.Collections;

using Gnome;
using Dashboard.Index;

[assembly:Dashboard.BackendFactory ("Dashboard.DocumentIndexBackend")]

namespace Dashboard {

	class DocumentIndexBackend : Backend {
		private static double DECAY = 0.3;

		private IndexManager mgr;

		public override bool Startup ()
		{
			Console.WriteLine ("DocumentIndex backend starting up...");

			Name = "DocumentIndex";

			try {
				mgr = new IndexManager ("local_file");
			} catch {
				Console.WriteLine ("DocumentIndex backend: ERROR LOADING.");

				this.Initialized = false;

				return false;
			}

			SubscribeToClues ("textblock");
			Initialized = mgr.Initialized;

			return Initialized;
		}

		public override BackendResult ProcessCluePacket (CluePacket cp)
		{
			if (! this.Initialized)
				return null;

			BackendResult result = new BackendResult (this, cp);

			ArrayList match_matches = new ArrayList ();

			foreach (Clue clue in cp.Clues) {
				if (! ClueTypeSubscribed (clue))
					continue;

				string clue_text = clue.Text.ToLower ();

				Console.WriteLine ("DocumentIndex: searching for {0}", clue_text);

				ArrayList match_clues; // FIXME: do something with these
				ArrayList matches = mgr.Retrieve (new StringReader (clue_text), 
								  "text/plain", 
								  out match_clues);

				Console.WriteLine ("Got from query: {0}\n", matches.Count);

				if (matches.Count == 0)
					continue;

				foreach (IndexMatch match in matches) {
					// find time decay factor (old files get weighted less)

					long t_1 = DateTime.Now.ToFileTime ();
					long t_0 = match.Source.LastWriteTime;

					// FIXME: keep an eye out for clock skew

					long distance = Math.Abs (t_1 - t_0);

					// convert from 100 ns increments to days

					distance /= 10L * 1000L * 1000L * 60L * 60L * 24L;

					double age_factor = Math.Exp (- DECAY * distance);

					match.Relevance *= age_factor;
				}

				matches.Sort ();

				foreach (IndexMatch match in matches) {
					Match indexmatch = new Match ("File", clue);
					string icon = Icon.LookupByURI (match.Source.URI);
					if (icon == null)
						icon = "internal:document.png";
					indexmatch["Icon"] = icon;
					indexmatch["Path"] = match.Source.URI;
					result.AddMatch (indexmatch);
				}
			}

			return result;
		}

		private string PathToFilename (string path)
		{
			string[] split = path.Split ('/');
			return split [split.Length - 1];
		}
	}
}
