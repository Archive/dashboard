/*Copyright (C) 2003 Pierre ANDREWS
*
*   Permission is hereby granted, free of charge, to any person obtaining a copy 
*   of this software and associated documentation files (the "Software"), to
*   deal in the Software without restriction, including without limitation the
*   rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
*   sell copies of the Software, and to permit persons to whom the Software is 
*   furnished to do so, subject to the following conditions:
*
*   The above copyright notice and this permission notice shall be included in 
*   all copies or substantial portions of the Software.
*
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
*   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
*   IN THE SOFTWARE.
*/

//MozillaBookmarks
//
//Backend for Dashboard
//Author: Pierre Andrews <mortimer.pa@urbanet.ch>
//
//read the Mozilla or Thunderbird bookmarks

//FIXME: application directory and profile should not be "hard coded".

using System;
using System.IO;
using System.Web;
using System.Collections;
using System.Text.RegularExpressions;

[assembly:Dashboard.BackendFactory ("Dashboard.MozillaBookmarks")]

namespace Dashboard {

	class MozillaBookmarks : BackendSimple {

		const string browser_dirname = ".phoenix";
		const string mozilla_dirname = ".mozilla";
		const string profile_name = "default";

		ArrayList bookmarks = new ArrayList (); 

		public override bool Startup ()
		{
			Name = "Mozilla";

			string home_dir = Environment.GetEnvironmentVariable ("HOME");
			string path;

			try {
				path = Path.Combine (Path.Combine (home_dir, browser_dirname), profile_name);

				if (!Directory.Exists (path)) {
                    path = Path.Combine (Path.Combine (home_dir, mozilla_dirname), profile_name);
                    if (!Directory.Exists (path)) {
                        Console.WriteLine (string.Format ("Bookmarks-Mozilla: cannot find {0}",path));
                        return false;
                    }
				}

				string[] dirs = Directory.GetDirectories (path);
				foreach (string dir in dirs) {
					if (dir.IndexOf (".slt") != -1) {
						path = Path.Combine (Path.Combine (path, dir), "bookmarks.html");
						break;
					}  else {
						Console.WriteLine ("Bookmarks-Mozilla: no way to find an .slt directory");
						return false;
					}
				}

				ExtractBookmarks (path);
		

				this.SubscribeToClues ("textblock", "keyword", "word_at_point");
		
				this.Initialized = true;

				return true;
			} catch (Exception ex) {
				Console.WriteLine ("Bookmarks-Mozilla: Exception");
				Console.WriteLine (ex.Message);
				return false;
			}
		}

		// FIXME - should use xpath, should get icon if possible
		private void ExtractBookmarks (string path) {
			string pat = "<A HREF=\"([^\"]+)\"[^>]*>([^<]*)</A>";
			string text = File.OpenText (path).ReadToEnd ();
	  
			Regex re = new Regex (pat, RegexOptions.None);
			int[] gnums = re.GetGroupNumbers ();
			System.Text.RegularExpressions.Match m = re.Match (text);
			while (m.Success) {
				Bookmark bk = new Bookmark ();
				bk.name = m.Groups[2].Captures[0].ToString ();
				bk.lname = m.Groups[2].Captures[0].ToString ().ToLower ();
				bk.url = m.Groups[1].Captures[0].ToString ();
				bookmarks.Add (bk);
		  
				m = m.NextMatch ();
			}  
			Console.WriteLine ("Mozilla-Bookmarks: loaded {0} bookmarks", bookmarks.Count);
		}

		protected override ArrayList ProcessClueSimple (Clue clue)
		{
			if (! this.Initialized)
				return null;

			ArrayList matches = new ArrayList ();

			string clue_text = clue.Text.ToLower ();

			foreach (Bookmark bk in bookmarks) {
				//FIXME: INDEX ME INDEX ME INDEX ME :)
				if (bk.lname.IndexOf (clue_text) == -1)
					continue;

				Match match = new Match ("WebLink", clue);
				match ["Icon"] = "internal:bookmark.png";
				match ["Title"] = bk.name;
				match ["Url"]   = bk.url;

				matches.Add (match);
			}

			return matches;
		}

		struct Bookmark {
			public string name;
			public string lname;
			public string url;
		}

	}
}
