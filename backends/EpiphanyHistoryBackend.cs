//
// backend-ephyhistory.cs: Epiphany history backend.  Reads from
// ~/.gnome2/epiphany/ephy-history.xml
//
// Authors:
//     Lee Willis <lee@leewillis.co.uk>
//
// Based on backend-bookmarks.cs


using System;
using System.IO;
using System.Xml;
using System.Collections;

[assembly:Dashboard.BackendFactory ("Dashboard.EpiphanyHistoryBackend")]

namespace Dashboard {

	class EpiphanyHistoryBackend : BackendSimple {
		XmlDocument         doc;
		XmlNodeList         titles;
		XmlDocument         favicondoc;
		
		public override bool Startup ()
		{
			Name = "Browser History";

			string home_dir = Environment.GetEnvironmentVariable ("HOME");
			string path;

			try {
				path = Path.Combine (home_dir, ".gnome2/epiphany/ephy-history.xml");

				if (!File.Exists (path)){
					return false;
				}

				doc = new XmlDocument ();
				doc.Load (path);

				titles = doc.SelectNodes ("/ephy_history[@version=\"1.0\"]/node");
				Console.WriteLine ("Epiphany History backend loaded {0} items.", titles.Count);


				// Load favicons data...
				path = Path.Combine (home_dir, ".gnome2/epiphany/ephy-favicon-cache.xml");
				if (File.Exists (path)) {
					this.favicondoc = new XmlDocument ();
					this.favicondoc.Load (path);
				}																						
				// FIXME: need to subscribe to clues that we
				// should try matching against. Probably
				// textblock and sentence_at_point:
				// this.SubscribeToClues ("textblock", "sentence_at_point");

				this.Initialized = true;
				return true;
			} catch {
				return false;
			}
		} // Startup()

		protected override ArrayList ProcessClueSimple (Clue clue)
		{
			ArrayList matches = new ArrayList();

			if (! this.Initialized)
				return null;

			if (clue.Text.Length == 0)
				return null;

			string clue_text   = clue.Text.ToLower ();
			int    match_count = 0;

			// For each history item
			foreach (XmlNode node in titles) {
				string title = "";
				string iconurl = "";
				
				iconurl = "internal:bookmark.png";
				title   = node.SelectSingleNode("property[@id=2]").InnerText;

				if (title.ToLower().IndexOf (clue_text) != -1) {
					//PageURI   = new Uri(node.SelectSingleNode("property[@id=3]").InnerText);
					//Visits    = node.SelectSingleNode("property[@id=4]").InnerText;
					//LastTime  = node.SelectSingleNode("property[@id=5]").InnerText;
					string url = node.SelectSingleNode ("property[@id=3]").InnerText;
					iconurl = GetIconPath (url);
					
					Match match = new Match ("WebLink", clue);
					match ["Emblem-Icon"] = iconurl;
					match ["Title"] = title;
					match ["URL"] = url;

					matches.Add(match);
					match_count ++;
				}
			}

			if (match_count == 0)
				return null;

			return matches;
		}

		private string GetIconPath (string url)
		{
			if (favicondoc == null)
					return null;

			int index = url.IndexOf ("/", 7);
			//Console.WriteLine (String.Format ("url is {0}; index is {1}", url, index));
			if (index > -1)
			   url = url.Substring (0, index);
			//Console.WriteLine ("url base: " + url);

			string xpath = "descendant::node[starts-with(child::property[1]/child::text(), '" + url + "')]";
			//Console.WriteLine ("xpath expression: " + xpath);
			XmlNode fav_node = favicondoc.SelectSingleNode (xpath);

			if (fav_node != null) {
				xpath = "child::property[position()=2]";
				XmlNode favicon = fav_node.SelectSingleNode (xpath);
				string home_dir = Environment.GetEnvironmentVariable ("HOME");
				string path = Path.Combine (home_dir, ".gnome2/epiphany/favicon_cache");
				return Path.Combine (path, favicon.InnerText);
			}

			return null;
		}
	} // Class
} // Namespace
