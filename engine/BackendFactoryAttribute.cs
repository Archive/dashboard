//
// BackendFactoryAttribute.cs - Assembly level attribute to specify
//                              the renderer
//
// Author: Todd Berman
//
// (C) Todd Berman <tberman@sevenl.net>

using System;

namespace Dashboard {

	[AttributeUsage (AttributeTargets.Assembly)]
	public class BackendFactoryAttribute : Attribute {

		string factoryClass;

		public BackendFactoryAttribute (string factoryClass)
		{
			this.factoryClass = factoryClass;
		}

		public string FactoryClass {
			get { return factoryClass; }
		}

	}

}
