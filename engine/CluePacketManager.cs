//
// GNOME Dashboard
//
// cluepacket-manager.cs: Dispatches clues to backends as they come in
// from frontends.
//
// Authors:
//    Nat Friedman <nat@nat.org>
//    Miguel de Icaza <miguel@ximian.com>
//    Joe Shaw <joe@assbarn.com>
//    Jim McDonald <Jim@mcdee.net>
//

using System;
using System.Collections;
using System.IO;
using System.Threading;
using System.Runtime.Remoting.Messaging;

namespace Dashboard {

	public delegate void BackendWorkerCallback (BackendWorkerResult b);

	// Holds information on outstanding backend workers
	public class ActiveBackendWorker
	{
		public Thread  thread;
		public Backend backend;

		public ActiveBackendWorker (Thread t, Backend b)
		{
			thread  = t;
			backend = b;
		}
	}

	// Class that holds the output of a backend worker
	public class BackendWorkerResult
	{
		private BackendResult Result;
		private Backend Backend;

		public BackendWorkerResult (Backend backend, BackendResult result)
		{
			Backend = backend;
			Result  = result;
		}

		public Backend GetBackend ()
		{
			return Backend;
		}

		public BackendResult GetResult ()
		{
			return Result;
		}
	}

	// The backend worker takes a cluepacket and a backend and
	// generates a BackendResult, containing matches and clues.
	public class BackendWorker
	{
		private Backend backend;
		private CluePacket cp;
		private BackendResult parent;
		private BackendWorkerCallback callback;

		public BackendWorker (Backend b, CluePacket c, BackendResult p, BackendWorkerCallback cb)
		{
			this.backend = b;
			this.cp = c;
			this.parent = p;
			this.callback = cb;
		}

		// Ask the backend for matches and new clues based on
		// the current cluepacket.
		public void QueryBackend ()
		{
			try {
				BackendResult result = null;

				// Send the cluepacket to the backend.
				try {
					Console.WriteLine ("--cluepacket--> " + backend.Name);

					result = backend.ProcessCluePacket (cp);
					if (result == null) {
						Console.WriteLine ("<-- done (no match)-- " + backend.Name);
						callback (new BackendWorkerResult (backend, result));
						return;
					}
				} catch (ThreadAbortException tae) {
					Console.WriteLine ("Worker thread caught abort (1)");
					return;
				} catch (Exception e) {
					Console.WriteLine ("ERROR in '{0}': {1}", backend.Name, e);

					if (result != null)
						result.Parent = this.parent;
					callback (new BackendWorkerResult (backend, result));

					return;
				}

				// Notification
				Console.WriteLine ("<-- matches -- " + backend.Name + " (" +
						result.GetStats () + ")");
				result.Parent = this.parent;
				callback (new BackendWorkerResult (backend, result));

				return;
			} catch (ThreadAbortException tae) {
				// We've been told to abort, probably because a
				// new cluepacket has come in and superceded the
				// work we're doing
				Console.WriteLine ("Worker thread caught abort (2)");
				Thread.ResetAbort ();
			}
		}
	}

	public class CluePacketManager
	{
		// Our parent dashboard.
		Dashboard dashboard;
		
		// Keep track of backendworkers that are currently running
		ArrayList backend_workers = new ArrayList ();

		// Our callback invoked when a backend query is done,
		// to check for errors.
		AsyncCallback query_backend_done;

		// Reference to last cluepacket that entered
		CluePacket oldcp = null;

		public CluePacketManager (Dashboard dashboard)
		{
			this.dashboard = dashboard;
		}

		// This function gets called whenever a cluepacket enters the system
		public void ProcessCluePacket (CluePacket cp, BackendResult parent)
		{
			if (cp == null)
				return;

			if (! cp.Focused)
				return;

			RewriteWildCard (cp);

			lock (dashboard.backends) {
				foreach (Backend backend in dashboard.backends) {
					if (! backend.Initialized)
						continue;

					// Don't send chained cluepackets to the backend
					// that generated the chained clues.
					if (parent != null && parent.Backend == backend)
						continue;

					if (! backend.AcceptCluePacket (cp))
						continue;

					Console.WriteLine ("--sending--> " + backend.Name);

					// Process each of the backends in a new thread
					BackendWorker worker = new BackendWorker (backend, cp, parent, new BackendWorkerCallback (BackendWorkerDone));
					Thread worker_thread = new Thread (new ThreadStart (worker.QueryBackend));
					lock (this.backend_workers) {
						backend_workers.Add (new ActiveBackendWorker (worker_thread, backend));
					}
					worker_thread.Start ();
				}
			}
		}

		// If the only clue in this cluepacket has a wildcard type,
		// rewrite it into all the supported types.
		void RewriteWildCard (CluePacket cp)
		{
			if (cp.Clues.Count == 0)
				return;

			if (((Clue) cp.Clues [0]).Type != "*")
				return;

			ArrayList types = new ArrayList ();
			foreach (Backend backend in dashboard.backends) {
				if (backend.SubscribedClueTypes == null || backend.Initialized == false)
					continue;

				foreach (string type in backend.SubscribedClueTypes)
					if (! types.Contains (type))
						types.Add (type);
			}

			Clue wildclue = (Clue) cp.Clues [0];
			foreach (string type in types)
				cp.Clues.Add (new Clue (type, wildclue.Text, wildclue.Relevance));

		}

		// Called when a backend thread finishes
		void BackendWorkerDone (BackendWorkerResult res)
		{
			BackendResult result = res.GetResult ();
			lock (this.backend_workers) {

				if (backend_workers == null || backend_workers.Count == 0)
					return;
				
				// Remove this from the list of active backend workers
				foreach (ActiveBackendWorker worker in backend_workers) {
					if (worker.backend == res.GetBackend ()) {
						backend_workers.Remove (worker);
						// Console.WriteLine ("Removed completed worker {0}, {1} left", res.GetBackend ().Name, backend_workers.Count);
						break;
			  		}
			  	}
			}

			if (result == null) {
				// Nothing back, leave
				return;
			}

			// Send the result to the engine
			Console.WriteLine ("Incorporating backend result into engine result");
			dashboard.ems.AddBackendResult (result);

			// Process any chained clue packets.  This
			// will do nothing if there are no chained
			// clues.
			if (result.ChainedCluePacket != null) {
				Console.WriteLine ("About to chain...");
				ProcessCluePacket (result.ChainedCluePacket, result);
			}
		}
	}
}
