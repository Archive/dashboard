//
// GNOME Dashboard
//
// dashboard.cs: Startup, some GUI code and some control flow.  A
// little messy.
//
// Authors:
//   Nat Friedman <nat@nat.org>
//   Joe Shaw <joe@assbarn.com>
//   Miguel de Icaza <miguel@ximian.com>
//

using System;
using System.Collections;
using System.IO;
using System.Threading;
using System.Reflection;

using Gtk;
using Gnome;
using GtkSharp;

namespace Dashboard {
	public delegate void GuiMatchNotifyDelegate (BackendResult result);
	public delegate void GuiClearDelegate ();
	
	class Driver {
		static void Main (string [] args)
		{
			Program dboard = new Program ("Dashboard", "0.0", Modules.UI, args);
			Thread.CurrentThread.Name = "Main thread";

			Dashboard d = new Dashboard ();

			dboard.Run ();
		}
	}

	public class Dashboard {
		public  CluePacketManager cpm;
		public  EngineMatchSet    ems;
		private string            dashdir;
		private DashboardWindow   window;
		private TcpServer         server;
		private Thread            t;
		public  ArrayList         backends;
		private Grapher           grapher;

		public  Hashtable         renderers;
		public  MatchRenderer     default_renderer;
		
		public Dashboard ()
		{
			Console.WriteLine ("Dashboard starting up");

			// Create directories we need
			SetupDirectories ();

			// Load the backends			
			LoadBackends ();

			// Load the renderers
			LoadRenderers ();

			// Create the engine match set
			ems = new EngineMatchSet ();
			ems.UpdateNotify += new UpdateDelegate (UpdateGuiNotify);

			// Create the MatchFilter and CluePacket Manager
			cpm = new CluePacketManager (this);

			// Start the TCP server.
			StartupServer ();

			// Create the window.
			SetupGUI ();

			// Create the grapher.
			grapher = new Grapher (ems);
		}

		// Search the backends directory for DLLs that are
		// Dashboard backends and start them up.
		private void LoadBackends ()
		{
			backends = new ArrayList ();

			string backends_dir = Environment.GetEnvironmentVariable ("DASHBOARD_BACKENDS_PATH");
			if (backends_dir == null || backends_dir == "")
				backends_dir = "../backends";

			string [] files = Directory.GetFiles (backends_dir, "*.dll");

			foreach (string file in files) {
				try {

					Assembly a = Assembly.LoadFrom (file);

					BackendFactoryAttribute attr = (BackendFactoryAttribute) a.GetCustomAttributes (typeof (BackendFactoryAttribute), false)[0];
					
					if (attr == null)
						continue;

					Console.WriteLine ("Loading backend: " + file);

					Backend backend = (Backend) Activator.CreateInstance (a.GetType (attr.FactoryClass));

					ThreadPool.QueueUserWorkItem (new WaitCallback (this.LaunchBackend), backend);
				} catch {
					//
					// Ignore an error if the file is not really an assembly
					//
				}
			}
		}

		// In a separate thread
		private void LaunchBackend (object o)
		{
			if (o == null)
				return;

			try {
				Backend b = (Backend) o;
				
				if (b.Startup ()) {
					lock (backends) {
						backends.Add (b);
					}
				} else {
					Console.WriteLine ("{0} NOT loaded", b.Name);
				}
			} catch (Exception e) {
				Console.WriteLine ("Received exception while loading Backend:");
				Console.WriteLine ("   Backend type: {0}\n" + o.GetType ().ToString ());
				Console.WriteLine ("Exception is:\n{0}", e);
			}
		}

		private void LoadRenderers ()
		{
			renderers = new Hashtable ();

			string renderers_dir = Environment.GetEnvironmentVariable ("DASHBOARD_RENDERERS_PATH");
			if (renderers_dir == null || renderers_dir == "")
				renderers_dir = "../renderers";

			string [] files = Directory.GetFiles (renderers_dir, "*.dll");

			foreach (string file in files) {
				try {

					Assembly a = Assembly.LoadFrom (file);
					
					MatchRendererFactoryAttribute attr = (MatchRendererFactoryAttribute) a.GetCustomAttributes (typeof (MatchRendererFactoryAttribute), false)[0];
					
					if (attr == null)
						continue;

					Console.WriteLine ("Loading render: " + file);
					MatchRenderer renderer = (MatchRenderer) Activator.CreateInstance (a.GetType (attr.FactoryClass));

					ThreadPool.QueueUserWorkItem (new WaitCallback (this.LaunchRenderer), renderer);
				} catch {
					//
					// Ignore an error if the file is not really an assembly
					//
				}
			}
		}

		// In a separate thread
		private void LaunchRenderer (object o)
		{
			if (o == null)
				return;

			try {
				MatchRenderer r = (MatchRenderer) o;
				
				r.Startup ();

				lock (renderers) {
					renderers[r.Type] = r;
					if (r.Type == "Default")
						default_renderer = r;
				}

			} catch (Exception e) {
				Console.WriteLine ("Received exception while loading renderer:");
				Console.WriteLine ("Exception is:\n{0}", e);
			}
		}

		//
		// Notify the GUI to update
		//
		private void UpdateGuiNotify()
		{
			if (this.window != null)
				this.window.Display (ems);
		}

		//
		// Invoked by the server when it gets a new cluepacket
		// from some frontend.
		//
		private void CluePacketNotifyCallback (CluePacket cp)
		{
			Console.WriteLine ("Got a new cluepacket");

			// Replace the entry text with the new clues
			string text = "";
			foreach (Clue c in cp.Clues) {
				if (text != "")
					text += ", ";

				text += c.Text;
			}
			this.window.QueryEntry.Text = text;

			// Kick off the cluepacket processing
			cpm.ProcessCluePacket(cp, null);
		}

		private void SetupDirectories ()
		{
			string home_dir = Environment.GetEnvironmentVariable ("HOME");
			string path = Path.Combine (home_dir, ".dashboard");

			dashdir = path;

			try {
				Directory.CreateDirectory (Path.Combine (path, "tmp"));
			} catch {
				Console.WriteLine ("Unable to create dashboard config directories.");
			}
		}

		//
		// Startup code
		//
		private void StartupServer ()
		{
			server = new TcpServer ();
			server.CluePacketNotify += new CluePacketNotifyDelegate (CluePacketNotifyCallback);
			t = new Thread (new ThreadStart (server.Run));
			t.Name = "Dashboard Server";
			t.Start ();
		}

		//
		private void SetupGUI ()
		{
			Console.WriteLine ("Setting up GUI...");

			this.window = new DashboardWindow ("Dashboard", this);
			this.window.DeleteEvent += new DeleteEventHandler (WindowDelete);

			EventHandler handler = new EventHandler (SendQuery);

			this.window.QueryEntry.Activated += handler;
			this.window.QueryButton.Clicked += handler;

			EventHandler graph_handler = new EventHandler (ShowGraph);
			this.window.GraphButton.Clicked += graph_handler;

			this.window.ShowAll();
		}

		private void WindowDelete (object o, DeleteEventArgs args)
		{
			Application.Quit ();
			server.Stop ();
			t.Join ();
			args.RetVal = true;
			Environment.Exit (0);
		}

		private void SendQuery (object o, EventArgs e) {
			CluePacket cp = new CluePacket ("Dashboard", "Default", true);
			cp.Clues.Add (new Clue ("*", this.window.QueryEntry.Text, 10));
			CluePacketNotifyCallback(cp);
		}

		private void ShowGraph (object o, EventArgs e) {
			grapher.ToggleWindow ();
		}
	}
}
