//
// GNOME Dashboard
//
// BackendResult.cs: A result returned by a backend.  It contains a
// list of matches and a list of new clues.
//
// Please see COPYING.
//

using System;
using System.Collections;

namespace Dashboard {

	public class BackendResult : ICloneable {

		// Input
		private readonly Backend backend;
		private readonly CluePacket cluepacket;
		private BackendResult parent;

		// Internal
		private readonly DateTime timestamp;

		// Output
		private ArrayList matches;
		private CluePacket chainedcluepacket;

		public BackendResult (Backend backend, CluePacket cp)
		{
			this.backend  = backend;
			this.cluepacket  = cp;
			this.parent  = null;
			this.timestamp = new DateTime ();
			this.matches  = new ArrayList ();
			this.chainedcluepacket = null;
		}

		// Private constructor used for cloning, keeps timestamp the same
		private BackendResult (Backend backend, CluePacket cp, DateTime timestamp)
		{
			this.backend  = backend;
			this.cluepacket  = cp;
			this.parent  = null;
			this.timestamp = timestamp;
			this.matches  = new ArrayList ();
			this.chainedcluepacket = null;
		}

		public ArrayList Matches {
			get { return this.matches; }
		}

		public void AddMatch (Match match)
		{
			if (match == null)
				return;

			// Avoid duplicates
			if (this.matches.Contains (match))
				return;

			this.matches.Add (match);
		}

		public Backend Backend {
			get { return this.backend; }
		}

		public BackendResult Parent {
			get { return this.parent; }
			set { this.parent = value; }
		}

		public CluePacket CluePacket {
			get { return this.cluepacket; }
		}

		public void AddChainedClue (Clue clue)
		{
			if (clue == null)
				return;

			// Don't allow circular chains.
			if (this.cluepacket.Clues.Contains (clue))
				return;

			if (chainedcluepacket == null)
				this.chainedcluepacket = new CluePacket (cluepacket);

			// Add chained clue - AddClue checks to see if the Clue is already
			// in the CluePacket.
			this.chainedcluepacket.AddClue (clue);
		}

		public void AddChainedClues (IList clues)
		{
			if (clues == null)
				return;

			foreach (Clue clue in clues) {
				if (clue != null)
					this.AddChainedClue (clue);
			}
		}

		public CluePacket ChainedCluePacket {
			get {
				if (this.chainedcluepacket == cluepacket)
					return null;

				return this.chainedcluepacket;
			}
		}

		// This operator does not consider the variables marked 'input' or 'internal' above
		public static bool operator== (BackendResult s1, BackendResult s2)
		{
			if (Object.ReferenceEquals (s1, null) && Object.ReferenceEquals (s2, null))
				return true;

			if (Object.ReferenceEquals (s1, null) || Object.ReferenceEquals (s2, null))
				return false;

			if ((s1.matches != null) || (s2.matches != null)) {
				if ((s1.matches == null || s2.matches == null))
					return false;

				if (s1.matches.Count != s2.matches.Count)
					return false;

				foreach (Match match in s1.matches)
					if (!s2.matches.Contains (match))
						return false;
			}

			if (s1.chainedcluepacket != s2.chainedcluepacket)
				return false;

			return true;
		}

		// This operator does not consider the variables marked 'input' or 'internal' above
		public static bool operator!= (BackendResult s1, BackendResult s2)
		{
			return ! (s1 == s2);
		}

		public override bool Equals (object obj)
		{
			if (! (obj is BackendResult))
			{
				return false;
			}

			return this == (BackendResult)obj;
		}

		public override int GetHashCode ()
		{
			int hashcode = this.backend.GetHashCode () ^
				       this.cluepacket.GetHashCode () ^
				       this.matches.GetHashCode ();

			if (this.parent != null)
				hashcode += this.parent.GetHashCode ();

			if (this.chainedcluepacket != null)
				hashcode += this.chainedcluepacket.GetHashCode ();

			return hashcode;
		}

		public object Clone ()
		{
			BackendResult clone = new BackendResult (this.backend, this.cluepacket, this.timestamp);
			clone.parent = this.parent;
			clone.matches = this.matches;
			clone.chainedcluepacket = this.chainedcluepacket;
			return clone;
		}

		public String GetStats ()
		{
			int matchcount = 0;
			if (this.matches != null)
				matchcount = this.matches.Count;

			int chainedcount = 0;
			if (this.chainedcluepacket != null)
				chainedcount = this.chainedcluepacket.Count ();

			return "Matches: " + matchcount + ", New Clues: "
			                   + chainedcount;

		}
	}
}
