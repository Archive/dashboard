//
// simple-elevator.cs: a match-at-a-time elevator
//
// Copyright (c) 2004 Jim McDonald
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify,
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
// THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections;

namespace Dashboard
{
	public class SimpleMatchElevator : IMatchElevator
	{
		// Constructor
		public SimpleMatchElevator (ElevatorManager em)
		{
			em.AddClockTickNotify (new EMClockTickDelegate (ClockTick));
			em.AddReceivedNewMatchNotify (new EMReceivedNewMatchDelegate (ReceivedNewMatch));
			em.AddReceivedDuplicateMatchNotify (new EMReceivedDuplicateMatchDelegate (ReceivedDuplicateMatch));
//			em.AddClickMatchNotify (new EMClickMatchDelegate (receivedClickMatch));
			em.AddReceivedCluePacketNotify (new EMReceivedCluePacketDelegate (ReceivedCluePacket));
		}

		// Tick of the clock
		public void ClockTick (EngineMatchSet ems)
		{
			Elevate (ems, -1);
		}

		// Receipt of a new cluepacket
		public void ReceivedCluePacket (EngineMatchSet ems, CluePacket cp)
		{
			Elevate (ems, -5);
		}

		// Receipt of a new match
		public void ReceivedNewMatch (EngineMatchSet ems, Match match)
		{
			Elevate (match, 60);
		}

		// Receipt of a duplicate match
		public void ReceivedDuplicateMatch (EngineMatchSet ems, Match match)
		{
			Elevate (match, 60);
		}

		// Elevate a single match
		public void Elevate (Match match, int mod)
		{
			int score = match.Score;
			score += mod;
			if (score < 0)
				score = 0;
			match.Score = score;
		}

		// Elevate multiple matches in the same category
		public void Elevate (DictionaryEntry category, int mod)
		{
			foreach (Match match in (IList) category.Value)
			{
				Elevate (match, mod);
			}
		}

		// Elevate an entire match set
		public void Elevate (EngineMatchSet matchset, int mod)
		{
			foreach (DictionaryEntry category in matchset.GetCategories ())
			{
				Elevate (category, mod);
			}
		}
	}
}
