// 
// GNOME Dashboard
//
// MatchRenderer.cs: Base class for the plugins that turn match data
// into pretty visualizations.
//
// Authors:
//    Nat Friedman <nat@nat.org>
//

using System.Collections;

namespace Dashboard {

	public abstract class MatchRenderer {
		// The match type this renderer knows how to handle.
		// See doc/matchtypes.txt.
		//
		// Specify "Default" to get all matches for which
		// there is no type-specific renderer.
		public string Type;


		// Called when the renderer plugin is loaded.  Sets
		// the match type that this renderer will accept.
		public abstract void Startup ();

		//
		// This function takes a list of matches and renders
		// them into a group, returning an HTML string.  All
		// of the matches must be of a type the renderer can
		// handle.
		//
		public abstract string HTMLRenderMatches (ArrayList matches);

		static Hashtable imageCacheHack = new Hashtable ();
		static public void AddImageHack (string key, byte[] data)
		{
			imageCacheHack [key] = data;
		}

		static public byte[] GetImageHack (string key)
		{
			return (byte[]) imageCacheHack [key];
		}
	}
}
