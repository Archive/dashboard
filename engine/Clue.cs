//
// GNOME Dashboard
//
// clue.cs: The class which represents a clue.
//
// Authors:
//    Nat Friedman <nat@nat.org>
//    Joe Shaw <joe@assbarn.com>
//

using System;
using System.Collections;
using System.Xml.Serialization;

namespace Dashboard {

	public class Clue : ICloneable {

		// See doc/cluetypes.txt
		[XmlAttribute("Type")]
		public readonly string Type;

		// The text of the clue
		[XmlText()]
		public readonly string Text;

		// The relevance of the clue to what the user's doing (1-10)
		[XmlAttribute("Relevance")]
		public readonly int Relevance;

		// True if from a frontend CP, not true if from a cluechain
		[XmlIgnore()]
		public bool Toplevel = false;

		// If cluechained from a match, contains the match
		// that triggered this clue
		[XmlIgnore()]
		public Match TriggeringMatch;

		// If cluechained directly from clues, contains the
		// clues that triggered this one
		[XmlIgnore()]
		public ArrayList TriggeringClues = null;

		// XmlSerialization requires a constructor that takes
		// no arguments
		public Clue ()
		{
		}

		public Clue (string type, string text, int relevance, params Clue[] triggering_clues)
		{
			this.Type           = type;
			this.Text           = text;
			this.Relevance      = relevance;
			this.TriggeringClues = new ArrayList ();

			foreach (Clue c in triggering_clues) {
				if (c != null)
					this.TriggeringClues.Add (c);
			}
		}

		public Clue (string type, string text, int relevance, Match triggering_match)
		{
			this.Type           = type;
			this.Text           = text;
			this.Relevance      = relevance;

			this.TriggeringMatch = triggering_match;
		}

		public override string ToString ()
		{
			return String.Format ("Clue Text: {0} Type: {1} Relevance: {2}",
					      this.Text, this.Type, this.Relevance);
		}

		public string ToShortOpaqueString ()
		{
			int hash;

			hash = this.GetClueHash ();
			foreach (Clue c in this.TriggeringClues)
				hash ^= c.GetClueHash ();

			return String.Format ("c{0}", Math.Abs (hash));
		}

		public int GetClueHash () {
			return this.Text.GetHashCode() ^ this.Type.GetHashCode() ^ this.Relevance.GetHashCode();
		}

		public override int GetHashCode() {
			return this.GetClueHash();
		}

		public object Clone ()
		{
			return new Clue (this.Type,
					 this.Text,
					 this.Relevance);
		}

		// We state that two Clues are equal iff all fields are the same.
		// We may want to ignore Relevance, but at this stage I'll keep it.
		public static bool operator == (Clue c1, Clue c2) {
			// Check the simple stuff first.
			if (Object.ReferenceEquals (c1, null) &&
			    Object.ReferenceEquals (c2, null)) {

				// Well, if they're both null then this is true...
				return true;

			} else if (! (Object.ReferenceEquals (c1, null) ||
				      Object.ReferenceEquals (c2, null))) {

				return (c1.Text == c2.Text &&
					c1.Type == c2.Type &&
					c1.Relevance == c2.Relevance);

			} else {
				return false;
			}
		}

		public static bool operator!=(Clue c1, Clue c2) {
			return ! (c1 == c2);
		}

		public override bool Equals(object obj) {
			if (!(obj is Clue))
				return false;

			return this == (Clue) obj;
		}

		// Provide an easy way to determine if this Clue contains anything.
		public bool Exists() {
		  Console.WriteLine ("Checking a Clue: {0}", this.ToString());
			return this.Text != "";
		}
	}
}
