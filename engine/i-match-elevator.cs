//
// imatch-elevator.cs: the match elevator interface
//
// Copyright( c) 2004 Jim McDonald
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files( the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify,
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be included in all copies
// or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
// THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections;

namespace Dashboard
{
	/*
	 * The IMatchElevator class is the interface for all match elevators.
	 */
	public interface IMatchElevator
	{
		// Clock tick
		void ClockTick( EngineMatchSet ers);

		// Act on reciept of a new cluepacket
		void ReceivedCluePacket( EngineMatchSet ers, CluePacket cp);

		// Act on reciept of a new match
		void ReceivedNewMatch( EngineMatchSet ers, Match match);

		// Act on reciept of a duplicate match
		void ReceivedDuplicateMatch( EngineMatchSet ers, Match match);

		// Elevate a single match
		void Elevate( Match match, int mod);

		// Elevate multiple matchs in the same category
		void Elevate( DictionaryEntry category, int mod);

		// Elevate an entire match set
		void Elevate( EngineMatchSet matchset, int mod);
	}
}
