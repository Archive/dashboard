//
// GNOME Dashboard
//
// cluepacket.cs: The CluePacket object.  Please see
// doc/cluepacket.txt for information about cluepackets.
//
// Authors: 
//    Nat Friedman <nat@nat.org>
//    Joe Shaw <joe@assbarn.com>
//

using System;
using System.Collections;
using System.Xml.Serialization;

namespace Dashboard {
	public delegate void CluePacketNotifyDelegate (CluePacket m);
		
	public class CluePacket : ICloneable {
		public readonly CluePacket Parent;
	
		// The name of the app that sent the cluepacket
		public readonly string Frontend;

		// The context inside the app that sent the cluepacket
		public readonly string Context;

		// Whether or not the triggering context was focused
		public readonly bool Focused;

		// The Xid generatign this cluepacket
		public readonly int Xid;

		[XmlElement(ElementName="Clue",
			    Type=typeof(Clue))]
		public ArrayList Clues = new ArrayList ();

		// XmlSerialization requires a constructor that takes
		// no arguments
		public CluePacket ()
		{
		}

		public CluePacket (CluePacket parent) 
			: this (parent.Frontend, parent.Context, parent.Focused, parent.Xid)
	        {
			this.Parent = parent;
			this.Clues  = new ArrayList (parent.Clues);
		}

		public CluePacket (string frontend, string context, bool focused) {
			this.Frontend = frontend;
			this.Context  = context;
			this.Focused  = focused;
		}

		public CluePacket (string frontend, string context, bool focused, int xid) 
			: this (frontend, context, focused)
		{
			this.Xid = xid;
		}

		public bool IsParent (CluePacket child)
		{
			if (child.Parent == this)
				return true;
			else if (child.Parent != null)
				return IsParent (child.Parent);
			else
				return false;
		}

		public ArrayList GetClues (params string[] clue_types)
		{
			if (clue_types == null) 
				return null;
			else if (clue_types.Length == 0)
				return this.Clues;
			else {
				ArrayList ret = new ArrayList ();
				foreach (Clue c in this.Clues) {
					if (((IList) clue_types).Contains (c.Type))
						ret.Add (c);
				}
				return ret;
			}
		}

		public CluePacket ToplevelCluePacket
		{
			get {
				CluePacket current = this;

				while (current.Parent != null)
					current = current.Parent;

				return current;
			}
		}

		public override string ToString ()
		{
			string s;

			s = String.Format ("Frontend: [{0}]  Context: [{1}]  Focused: [{2}]\n" +
					   "Clues:\n", this.Frontend, this.Context, this.Focused);
			for (int i = 0; i < Clues.Count; i ++)
				s += String.Format ("    {0}\n", this.Clues[i].ToString ());

			return s;
		}

		public object Clone ()
		{
			CluePacket copy = new CluePacket (this.Frontend,
							  this.Context,
							  this.Focused,
							  this.Xid);

			foreach (Clue c in this.Clues)
				copy.Clues.Add (c);

			return copy;
		}

		// It's going to be useful to be able to check two CluePackets for
		// equality.

		// I'm not sure where Xid is coming from, or if it is actually
		// every set (I'm not convinced that it is).  But we'll check
		// that as well for equality.
		public static bool operator == (CluePacket c1, CluePacket c2)
		{

			// Check the simple stuff first.
			if (Object.ReferenceEquals (c1, null) &&
			    Object.ReferenceEquals (c2, null)) {

				// Well, if they're both null then this is true...
				return true;

			} else if (! (Object.ReferenceEquals(c1, null) || Object.ReferenceEquals(c2, null)) &&
				   c1.Frontend == c2.Frontend && c1.Context == c2.Context &&
				   c1.Focused  == c2.Focused  && c1.Xid     == c2.Xid &&
				   c1.Clues.Count == c2.Clues.Count) {
				// Both c1 and c2 have to NOT be null.

				// We're guranteed that each Clue in Clues is going to be unique.
				// And that the arrays are same length.  So, as soon as we find a
				// Clue in c1 which doesn't exist in c2 they're not equal.  If we
				// get to the end then they must be the same arrays.
				foreach (Clue c in c1.Clues) {
					if (! c2.Clues.Contains(c))
						return false;
				}

				return true;
			}

			return false;
		}

		public static bool operator != (CluePacket c1, CluePacket c2)
		{
			return ! (c1 == c2);
			
		}

		public override bool Equals (object obj)
		{
			if (!(obj is CluePacket))
				return false;

			return this == (CluePacket) obj;
		}

		public override int GetHashCode()
		{
			int hash = this.Frontend.GetHashCode () + this.Context.GetHashCode () + this.Xid;
			
			if (this.Focused)
				hash += 1;
		
			foreach (Clue c in this.Clues)
				hash += c.GetHashCode ();

			return hash;
		}

		// Add a clue to the array.  This allows us to enforce having only one
		// copy of a Clue in a CluePacket.
		//
		// Once debugging is complete the redundant return falses, can be
		// collapsed.
		public bool AddClue (Clue c)
		{
			if (! c.Exists ())
				return false;

			if (! this.Clues.Contains (c)) {
				Console.WriteLine ("Add a Clue to a CluePacket.");
				this.Clues.Add (c);
					
				return true;
			} else {
				Console.WriteLine ("Clue already exists in a CluePacket.");
				
				return false;
			}
		}

		public bool Exists ()
		{
			if (this.Frontend == null || this.Frontend != "")
				return false;

			return true;
		}

		public int Count ()
		{
			return this.Clues.Count;
		}
	}
}	
