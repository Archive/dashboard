//
// elevator-manager.cs: manage all elevators
//
// Copyright (c) 2004 Jim McDonald
//

using System;

namespace Dashboard
{
	// Delegates
	public delegate void EMClockTickDelegate (EngineMatchSet ems);
	public delegate void EMClickMatchDelegate (EngineMatchSet ems, Match match);
	public delegate void EMReceivedCluePacketDelegate (EngineMatchSet ems, CluePacket cp);
	public delegate void EMReceivedNewMatchDelegate (EngineMatchSet ems, Match match);
	public delegate void EMReceivedDuplicateMatchDelegate (EngineMatchSet ems, Match match);

	public class ElevatorManager
	{
		// Delegate holders
		private EMClockTickDelegate EMClockTickNotify;
		private EMClickMatchDelegate EMClickMatchNotify;
		private EMReceivedCluePacketDelegate EMReceivedCluePacketNotify;
		private EMReceivedNewMatchDelegate EMReceivedNewMatchNotify;
		private EMReceivedDuplicateMatchDelegate EMReceivedDuplicateMatchNotify;

		// Notification for updating the GUI
		UpdateGuiDelegate updateGuiNotify;

		// Constructor
		public ElevatorManager (UpdateGuiDelegate updateGuiNotify)
		{
			this.updateGuiNotify = updateGuiNotify;

			return;
		}

		// Add a clock tick notification
		public void AddClockTickNotify (EMClockTickDelegate del)
		{
			EMClockTickNotify += del;
		}

		// Add a click match notification
		public void AddClickMatchNotify (EMClickMatchDelegate del)
		{
			EMClickMatchNotify += del;
		}

		// Add a cluepacket received notification
		public void AddReceivedCluePacketNotify (EMReceivedCluePacketDelegate del)
		{
			EMReceivedCluePacketNotify += del;
		}

		// Add a new match received notification
		public void AddReceivedNewMatchNotify (EMReceivedNewMatchDelegate del)
		{
			EMReceivedNewMatchNotify += del;
		}

		// Add a duplicate match received notification
		public void AddReceivedDuplicateMatchNotify (EMReceivedDuplicateMatchDelegate del)
		{
			EMReceivedDuplicateMatchNotify += del;
		}

		// Obtain cluepacket received notifications
		public EMReceivedCluePacketDelegate GetReceivedCluePacketNotifies ()
		{
			return EMReceivedCluePacketNotify;
		}

		// Obtain new match notifications
		public EMReceivedNewMatchDelegate GetReceivedNewMatchNotifies ()
		{
			return EMReceivedNewMatchNotify;
		}

		// Obtain duplicate match notifications
		public EMReceivedDuplicateMatchDelegate GetReceivedDuplicateMatchNotifies ()
		{
			return EMReceivedDuplicateMatchNotify;
		}

		// Obtain click tick notifications
		public EMClockTickDelegate GetClockTickNotifies ()
		{
			return EMClockTickNotify;
		}
	}
}
