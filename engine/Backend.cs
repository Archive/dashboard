//
// GNOME Dashboard
//
// backend.cs: Base class for dashboard backends.
//
// Authors:
//    Nat Friedman <nat@nat.org>
//    Joe Shaw <joe@assbarn.com>
//

using System;
using System.Collections;
using System.IO;

namespace Dashboard {

	public abstract class Backend {
		// All backends must implement a 'startup' method which
		// gets called when the dashboard starts and loads them.
		//
		// When this function is finished, the backend must set
		// this.Initialized to true before any queries will be
		// sent to it.
		public abstract bool  Startup ();

		public IList SubscribedClueTypes = null;

		protected void SubscribeToClues (params string[] clue_types_supported) 
		{
			SubscribedClueTypes = clue_types_supported;
		}

		protected bool ClueTypeSubscribed (Clue c)
		{
			if (SubscribedClueTypes == null)
				return true;

			return SubscribedClueTypes.Contains (c.Type);
		}

		public virtual bool AcceptCluePacket (CluePacket cp)
		{
			// If the backend hasn't specified the clue_types its
			// interested in, always accept the CluePacket.
			if (SubscribedClueTypes == null)
				return true;

			foreach (Clue clue in cp.Clues) {
				if (this.ClueTypeSubscribed (clue))
					return true;
			}

			return false;
		}

		// This is the core backend function which processes a
		// cluepacket to:
		//     - search for matches
		//     - generate new clues
		//     - anything else it wants to do, like logging
		//       cluepackets to a file
		public abstract BackendResult ProcessCluePacket (CluePacket cp);

		// To handle clicks for non-URLs and non-local-paths.  
		// Default implementation does nothing.
		public virtual void LinkClicked (string user_data)
		{
		}

		// The backend's name.
		public string Name;

		// Any credits to be displayed (E.g. copyright notices)
		public string Credits;

		// Whether or not the backend is ready to accept clues.
		public bool Initialized;

		public override string ToString ()
		{
			return Name;
		}

		// The path to this backends system configuration directory.
		protected string ConfigSystemPath ()
		{
			// Surely a Path.Combine which can take an arbitary
			// number of arguements would have made sense?
			return Path.Combine (Path.Combine (Environment.GetEnvironmentVariable
							 ("DASHBOARD_BACKEND_PATH"),
							 "backend-config"),
					    Name);
		}

		// The path to this backends user configuration directory.
		protected string ConfigUserPath ()
		{
			return Path.Combine (Path.Combine (Environment.GetEnvironmentVariable ("HOME"),
							 ".dashboard"),
					    Path.Combine ("backend-data", Name));
		}

		// Use either a system installed file, or one from the backends directory
		// in the users home directory.
		protected string ConfigPath (string Filename)
		{
			if (File.Exists (Path.Combine (ConfigUserPath (), Filename))) {
				return Path.Combine (ConfigUserPath (), Filename);
			} else {
				return Path.Combine (ConfigSystemPath (), Filename);
			}
		}
	}

	public abstract class BackendSimple : Backend {
		// This implementation calls ProcessClueSimple for each clue in
		// a cluepacket.  ProcessSimpleClue must be implemented by
		// subclasses.
		public override BackendResult ProcessCluePacket (CluePacket cp)
		{
			BackendResult result = new BackendResult (this, cp);

			foreach (Clue c in cp.Clues) {

				if ( !ClueTypeSubscribed (c) || c.Text.Length == 0)
					continue;

				ArrayList matches = this.ProcessClueSimple (c);
				if (matches == null || matches.Count == 0)
					continue;

				foreach (Match match in matches)
					result.AddMatch (match);
			}

			return result;
		}

		protected abstract ArrayList ProcessClueSimple (Clue c);
	}

}
