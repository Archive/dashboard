//
// GNOME Dashboard
//
// Grapher.cs: Draws a graph of the clues and matches.
//
// Author:
//   Nat Friedman <nat@nat.org>
//

using System;
using System.IO;
using System.Diagnostics;
using System.Text;
using Gtk;
using Gdk;
using GtkSharp;

namespace Dashboard {

	public class Grapher {

		private EngineMatchSet ems;
		private bool window_shown;
		private bool window_created;
		private Gtk.Window window;
		private Gtk.Image image;

		public Grapher (EngineMatchSet ems)
		{
			this.ems = ems;

			this.ems.UpdateNotify += new UpdateDelegate (UpdateGraph);
		}

		private void CreateWindow ()
		{
			window = new Gtk.Window (Gtk.WindowType.Toplevel);

			window.DefaultWidth = 800;
			window.DefaultHeight = 600;
			window.Title = "Dashboard Cluegraph";

			window.DeleteEvent += new DeleteEventHandler (this.WindowDeleted);

			ScrolledWindow sw = new ScrolledWindow ();
			sw.SetPolicy (PolicyType.Automatic, PolicyType.Automatic);
			window.Add (sw);

			image = new Gtk.Image ();
			sw.AddWithViewport (image);
		}

		private void WindowDeleted (object obj, DeleteEventArgs args)
		{
			this.window_shown = false;
			this.window = null;

			args.RetVal = false;
		}

		public void ShowWindow ()
		{
			if (this.window == null)
				CreateWindow ();

			this.window_shown = true;
			
			this.window.ShowAll ();
			UpdateGraph ();
		}

		public void HideWindow ()
		{
			this.window_shown = false;

			this.window.Hide ();
		}

		public void ToggleWindow ()
		{
			if (this.window_shown)
				HideWindow ();
			else
				ShowWindow ();
		}

		public void UpdateGraph ()
		{
			if (! window_shown)
				return;

			lock (this) {
				string graph_text = CreateGraphText ();

				string home = Environment.GetEnvironmentVariable ("HOME");
				string dot_file_path = Path.Combine (home, ".dashboard/tmp/cluegraph.dot");
				WriteGraphText (dot_file_path, graph_text);

				string png_path = Path.Combine (home, ".dashboard/tmp/cluegraph.png");
				GeneratePng (dot_file_path, png_path);

				Gdk.Pixbuf pixbuf = CreatePixbufFromPng (png_path);

				// Show the new graph.
				if (pixbuf != null) {
					image.Pixbuf = pixbuf;
				} else {
					Console.WriteLine ("NULL PIXBUF");
				}
			}
		}

		private void WriteGraphText (string path, string graph_text)
		{
			FileStream s;

			try {
				s = File.Open (path, FileMode.Create);
			} catch {
				return;
			}

			StreamWriter sw = new StreamWriter (s);

			sw.Write (graph_text);
			sw.Flush ();
			sw.Close ();
		}

		private void GeneratePng (string dot_file_path, string png_path)
		{
			// Run dot to convert this to a .png.
			Process dot = new Process ();
			dot.StartInfo.FileName = "dot";
			dot.StartInfo.Arguments = String.Format ("-Tpng {0} -o {1}", dot_file_path, png_path);
			dot.StartInfo.UseShellExecute = true;

			try {
				dot.Start ();
				dot.WaitForExit ();
			} catch {
				return;
			}

		}

		private Gdk.Pixbuf CreatePixbufFromPng (string png_path)
		{
			FileStream s;

			try {
				s = File.Open (png_path, FileMode.Open);
			} catch {
				return null;
			}

			Gdk.Pixbuf pb;
			try {
				pb = new Gdk.Pixbuf (s);
			} catch {
				Console.WriteLine ("Error loading png");
				return null;
			}

			return pb;
		}

		private string CreateGraphText ()
		{
			string graph;

			graph = GraphHeader ();

			lock (ems.matches) {

				graph += GraphCluePacketNode (ems.ToplevelCluePacket);

				foreach (Clue c in ems.ToplevelCluePacket.Clues)
					graph += GraphCluePacketClueArc (ems.ToplevelCluePacket, c);


				foreach (Match m in ems.matches) {
					graph += GraphMatchNode (m);

					foreach (Clue c in m.TriggeringClues) {
						graph += GraphClueNode (c);
						graph += GraphClueMatchArc (c, m);
					}
				}

				foreach (Clue c in ems.chained_clues) {
					graph += GraphClueNode (c);

					if (c.TriggeringClues != null) {
						foreach (Clue tc in c.TriggeringClues) {
							graph += GraphClueNode (tc);
							graph += GraphClueClueArc (tc, c);
						}
					}

					if (c.TriggeringMatch != null)
						graph += GraphMatchClueArc (c.TriggeringMatch, c);
				}
			}

			graph += GraphFooter ();

			return graph;
		}

		private string GraphHeader ()
		{
			return "digraph \"cluechain\" {\n";
		}

		private string GraphFooter ()
		{
			return "}\n";
		}

		private string GraphCluePacketNode (CluePacket cp)
		{
			return String.Format
				("    cp{0} [ label=\"Original\\nCluePacket\" color=\"limegreen\" shape=\"triangle\" style=\"filled\" ];\n",
				 Math.Abs (cp.GetHashCode ()));
		}

		private string GraphMatchNode (Match m)
		{
			string label = LineSplit (String.Format ("Match: Type: {0} Title: {1}", m.Type, m ["title"]),
						  40, "\\n");
						  

			return String.Format
				("    m{0} [ label=\"{1}\" shape=\"box\" style=\"filled\" color=\"indianred\" ];\n",
				 Math.Abs (m.GetHashCode ()),
				 label);
		}

		private string GraphClueNode (Clue c)
		{
			String text = c.Text;

			// We don't want really long labels.  They look ugly.
			if (text.Length > 40)
				 text = text.Substring (0, 40) + "...";

			// dot doesn't like labels with line breaks in them.
			text = text.Replace ("\n", "\\n");

			// dot also doesn't like unescaped quotes in a label.
			// Need the double step to make sure we don't escape a
			// pre-existing escape.
			text = text.Replace ("\\\"", "\"");
			text = text.Replace ("\"", "\\\""); 

			return String.Format
				("    c{0} [ label=\"Clue: Type: {1} Text: {2}\" color=\"dodgerblue\" style=\"filled\" ];\n",
				 Math.Abs (c.GetHashCode ()),
				 c.Type,
				 text);
		}

		private string GraphCluePacketClueArc (CluePacket cp, Clue c)
		{
			return String.Format
				("    cp{0} -> c{1} [ style=\"dashed\" ];\n",
				 Math.Abs (cp.GetHashCode ()),
				 Math.Abs (c.GetHashCode ()));
		}

		private string GraphClueMatchArc (Clue c, Match m)
		{
			return String.Format
				("    c{0} -> m{1};\n",
				 Math.Abs (c.GetHashCode ()),
				 Math.Abs (m.GetHashCode ()));
		}

		private string GraphClueClueArc (Clue tc, Clue c)
		{
			return String.Format
				("    c{0} -> c{1} [ style=\"dashed\" ];\n",
				 Math.Abs (tc.GetHashCode ()),
				 Math.Abs (c.GetHashCode ()));
		}

		private string GraphMatchClueArc (Match m, Clue c)
		{
			return String.Format
				("   m{0} -> c{1} [ style=\"dashed\" ];\n",
				 Math.Abs (m.GetHashCode ()),
				 Math.Abs (c.GetHashCode ()));
		}

		private static string LineSplit (string line, int width, string linebreak)
		{
			string str = line.Trim ();

			if (str.Length <= width)
				return str;

			StringBuilder sb = new StringBuilder ();
			int n;

			if (width < str.Length && str [width] == ' ')
				n = width;
			else
				n = str.LastIndexOf (' ', width);

			if (n == -1)
				sb.Append (str);
			else {
				sb.Append (str.Substring (0, n));
				sb.Append (linebreak);
				sb.Append (LineSplit (str.Substring (n+1), width, linebreak));
			}

			return sb.ToString ();
		}

	}
}
