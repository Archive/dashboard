//
// GNOME Dashboard
//
// gui.cs: User interface routines
//
// Authors:
//    Nat Friedman <nat@nat.org>
//    Joe Shaw <joe@assbarn.com>
//    Miguel de Icaza <miguel@ximian.com>
//

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Reflection;
using System.Web;
using System.Net;
using System.Xml;

using Gtk;
using Gnome;
using GtkSharp;

namespace Dashboard {

	public class DashboardWindow : Gtk.Window {

		private Dashboard      dashboard;
		private Gtk.Entry      entry;
		private Gtk.Button     button;
		private Gtk.Button     graph_button;
		private Gtk.HTML       html;
		private Gtk.HTMLStream stream;

		private Queue          match_queue;

		private EngineMatchSet matchset;
		private CluePacketManager cpm;

		public DashboardWindow (string title, Dashboard dashboard) : base (WindowType.Toplevel)
		{
			this.Title         = title;
			this.dashboard     = dashboard;
			this.DefaultWidth  = 279;
			this.DefaultHeight = 640;

			Box hbox = new HBox (false, 0);

			this.entry = new Gtk.Entry ();
			hbox.PackStart (entry, false, false, 2);
			
			this.button = new Button ("Query");
			hbox.PackStart (button, false, false, 2);

			this.graph_button = new Button ("Graph");
			hbox.PackStart (graph_button, false, false, 2);

			this.html = new Gtk.HTML ();
			this.html.LinkClicked += new LinkClickedHandler (LinkClicked);
			this.html.UrlRequested += new UrlRequestedHandler (UrlRequested);

			ScrolledWindow view = new ScrolledWindow ();
			view.SetPolicy (PolicyType.Never, PolicyType.Automatic);
			view.Add (this.html);
			
			Box vbox = new VBox (false, 0);
			vbox.PackStart (hbox, false, false, 0);
			vbox.PackStart (view, true, true, 0);

			this.Add (vbox);
		      
			this.match_queue = new Queue ();
			Gravity = Gdk.Gravity.NorthEast;
			Move (Screen.Width - DefaultWidth, 0);
		}

		private void LinkClicked (object o, LinkClickedArgs args)
		{
			// If it's backend-specific, call into the
			// backend.
			if (args.Url.StartsWith ("backend:"))
				NotifyBackendLinkClicked (args.Url);

			else if (args.Url.StartsWith ("exec:")) {
				string cmd = args.Url.Substring ("exec:".Length);

				Process p = new Process ();
				p.StartInfo.UseShellExecute = false;
				if (cmd.IndexOf (' ') == -1)
					p.StartInfo.FileName = cmd;
				else {
					p.StartInfo.FileName = cmd.Substring (0, cmd.IndexOf (' '));
					p.StartInfo.Arguments = cmd.Substring (cmd.IndexOf (' '));
				}

				Console.WriteLine ("FileName: " + p.StartInfo.FileName);
				Console.WriteLine ("Args: " + p.StartInfo.Arguments);

				try {
					p.Start ();
				} catch {
				}

				return;
			}

			// Otherwise, open with the proper MIME
			// handler.
			try {
			    //Gnome.MimeApplication.Exec (Gnome.VFS.Mime.GetMimeType (args.Url), args.Url);
				Gnome.Url.Show (args.Url);
			} catch {
			}	
		}

		private void NotifyBackendLinkClicked (string url)
		{
			string backend_name = null;
			string[] split;

			try {
				split = url.Split (':');
				backend_name = split [1];
			} catch {
			Console.WriteLine ("Could not get backend name from link URL");
				return;
			}

			if (backend_name == null || backend_name == "") {
				Console.WriteLine ("Could not get backend name from link URL");
				return;
			}

			string user_data = url.Substring (backend_name.Length + "backend::".Length);

			Console.WriteLine ("LinkClicked [{0}] [{1}]", backend_name, user_data);

			lock (dashboard.backends)
			{
				foreach (Backend b in dashboard.backends)
					if (b.Name == backend_name) {
						b.LinkClicked (user_data);
						return;
					}
			}
		}

		private Stream GetImage (string name)
		{
			Assembly assembly = System.Reflection.Assembly.GetCallingAssembly ();
			System.IO.Stream s = assembly.GetManifestResourceStream (name);

			return s;
		}

		private Stream GetFile (string uri)
		{
			string path = uri;

			Stream s = null;

			try {
				s = File.Open (path, FileMode.Open, FileAccess.Read);
			} catch {
				Console.WriteLine ("Unable to open {0}", path);
			}

			return s;
		}

		//
		// Provides data for urls requested (images).  Things prefixed
		// with `internal:' we pull for one of the embedded streams
		//
		private void UrlRequested (object o, UrlRequestedArgs args)
		{
			Stream s = null;

			if (args.Url.IndexOf ("/") == 0) {
				s = GetFile (args.Url);
			} else {
				if (args.Url.StartsWith ("imagehack:")) {
					string key = args.Url.Substring ("imagehack:".Length);
					byte[] img = MatchRenderer.GetImageHack (key);
					args.Handle.Write (img, img.Length);
					return;
				} else if (args.Url.StartsWith ("internal:")) {
					try {
						s = GetImage (args.Url.Substring (args.Url.IndexOf (':') + 1));
					} catch {
						Console.WriteLine ("Could not find image: " + args.Url);
						return;
					}
				} else {
					try {
						HttpWebRequest req = (HttpWebRequest)WebRequest.Create (args.Url);
						req.UserAgent = "GNOME Dashboard";
						WebResponse resp = req.GetResponse ();
						s = resp.GetResponseStream ();
					} catch (Exception e) {
						Console.WriteLine ("Do not know how to handle " + args.Url);
						return;
					}
				}
			}

			if (s == null) {
				Console.WriteLine ("Could not obtain image " + args.Url);
				return;
			}

			byte [] buffer = new byte [8192];
			int n;
			
			while ( (n = s.Read (buffer, 0, 8192)) != 0)
				args.Handle.Write (buffer, n);
		}

		// Called in the main loop (in the main thread) when threads
		// have queued work for us.
		private bool UpdateGUI ()
		{
			this.stream = this.html.Begin ();
			this.stream.Write ("<html><body>");
			String output = EngineMatchSetToHtmlGroupByType (matchset);
			if (output != "")
				this.stream.Write (output);

			this.stream.Write ("</body></html>");
			return false;
		}

		new public void Display (EngineMatchSet matchset)
		{
			if (matchset.matches != null && matchset.matches.Count > 0) {
				this.matchset = matchset;
				GLib.Idle.Add (new GLib.IdleHandler (UpdateGUI));
			}
		}

		public Gtk.Entry QueryEntry {
			get { return this.entry; }
		}

		public Gtk.Button QueryButton {
			get { return this.button; }
		}

		public Gtk.Button GraphButton {
			get { return this.graph_button; }
		}

		private IDictionary GroupMatchesByType (ArrayList matches)
		{
			IDictionary groups;

			groups = new ListDictionary ();

			foreach (Match m in matches) {
				IList group = (IList) groups [m.Type];
				if (group == null) {
					group = new ArrayList ();
					groups [m.Type] = group;
				}

				group.Add (m);
			}

			return groups;
		}

		private string EngineMatchSetToHtmlGroupByType (EngineMatchSet matchset)
		{
			if (matchset == null) {
				Console.WriteLine ("Null matchset!");
				return "";
			}

			String html = "";

			lock (matchset.matches) {
				IDictionary groups = GroupMatchesByType (matchset.matches);

				foreach (DictionaryEntry group in groups) {
					ArrayList matches = new ArrayList ();

					foreach (Match match in (IList) group.Value)
						matches.Add (match);
					
					html += MatchesToHtml (matches);
				}
			}

			return html;
		}

		private string MatchesToHtml (ArrayList matches)
		{

			if (matches.Count == 0)
				return "";

			MatchRenderer r = FindRendererForMatches (matches);
			if (r == null) {
				Console.WriteLine ("No renderer found!");
				return "";
			}

			return r.HTMLRenderMatches (matches);
		}

		private MatchRenderer FindRendererForMatches (ArrayList matches)
		{
			string t = ((Match) matches [0]).Type;

			if (dashboard.renderers.ContainsKey (t))
				return (MatchRenderer) dashboard.renderers[t];

			return dashboard.default_renderer;
		}
	}
}
