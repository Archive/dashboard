//
// GNOME Dashboard
//
// server.cs: The base class that listens on a network connection for
// cluepackets from frontends.  There is one implementation right now,
// in tcpservers.cs
//
// Authors:
//   Joe Shaw <joe@assbarn.com>
//   Nat Friedman <nat@nat.org>
//

using System;
using System.Xml;

namespace Dashboard {

	public abstract class Server {
		public CluePacketNotifyDelegate CluePacketNotify;
		
		public abstract void Run ();
		public abstract void Stop ();

		private int tries = 0;

		protected void HandleBuffer (string xml) {
			XmlDocument cluepacket_xml = new XmlDocument ();
			
			try {
				cluepacket_xml.LoadXml (xml);
			} catch {
				Console.WriteLine ("CluePacket XML is not well-formed!");
				return;
			}

			XmlNode frontend_xml  = cluepacket_xml.SelectSingleNode ("//Frontend");
			XmlNode context_xml   = cluepacket_xml.SelectSingleNode ("//Context");
			XmlNode focused_xml   = cluepacket_xml.SelectSingleNode ("//Focused");
			XmlNode additive_xml  = cluepacket_xml.SelectSingleNode ("//Additive");
			XmlNodeList clues_xml = cluepacket_xml.SelectNodes      ("//Clue");

			if (frontend_xml == null || clues_xml == null) {
				Console.WriteLine ("CluePacket XML is not valid!");
				return;
			}

			string context = "Default";
			if (context_xml != null)
				context = context_xml.InnerText;

			string frontend = "UNKNOWN";
			if (frontend_xml != null)
				frontend = frontend_xml.InnerText;

			bool focused = false;
			if (focused_xml != null)
				focused = (focused_xml.InnerText.ToLower () == "true");

			CluePacket cluepacket = new CluePacket (frontend, context, focused);

			Console.WriteLine ("CluePacket contains {0} clue(s).", clues_xml.Count);

			foreach (XmlNode node in clues_xml) {
				String text      = node.InnerText;
				String type      = String.Empty;
				int    relevance = -1;

				if (node.Attributes ["Relevance"] != null)
					relevance = Int32.Parse (node.Attributes ["Relevance"].InnerText);

				if (node.Attributes ["Type"] != null)
					type = node.Attributes ["Type"].InnerText;

				Clue c = new Clue (type, text, relevance);

				cluepacket.Clues.Add (c);
			}

			this.CluePacketNotify (cluepacket);
		}

	}
}
