//
// GNOME Dashboard
//
// engine-match-set.cs: The class for all matches within the Dashboard
// engine
//
// Copyright (c) 2004 Jim McDonald
//

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Threading;

namespace Dashboard
{
	public delegate void UpdateDelegate ();

	public class EngineMatchSet : ICloneable
	{
		// The current toplevel cluepacket.  When this changes, the
		// EngineMatchSet resets itself.
		private CluePacket toplevel_cluepacket;

		// All the matches
		public ArrayList matches;

		// The chained clues
		public ArrayList chained_clues;

		// Notification when something changes
		public UpdateDelegate UpdateNotify;

		// Constructor
		public EngineMatchSet ()
		{
		}

		private void Clear ()
		{
			Console.WriteLine ("Clearing out old data");
			this.matches = new ArrayList ();
			this.chained_clues = new ArrayList ();
		}

		public bool IsEmpty ()
		{
			Console.WriteLine ("matches: {0}  chained clues: {1}",
					   this.matches.Count, this.chained_clues.Count);

			if ((this.matches == null || this.matches.Count == 0) &&
			    (this.chained_clues == null || this.chained_clues.Count == 0))
				return true;
			else
				return false;
		}

		// Add a backend match set's information to the engine
		public void AddBackendResult (BackendResult result)
		{
			CluePacket toplevel, parent;

			if (result.ChainedCluePacket == null)
				parent = result.CluePacket;
			else
				parent = result.ChainedCluePacket;
			
			toplevel = parent.ToplevelCluePacket;

			if (this.toplevel_cluepacket == null ||
			    ! Object.ReferenceEquals (this.toplevel_cluepacket, toplevel)) {
				this.Clear ();
				this.toplevel_cluepacket = toplevel;
			}

			lock (this.matches) {
				// Add each match in turn, if it's not a duplicate.
				foreach (Match match in result.Matches) {
					if (! this.matches.Contains (match))
						this.matches.Add (match);
				}
			}

			// FIXME: need locking here too?
			foreach (Clue clue in parent.Clues) {
				if (! this.chained_clues.Contains (clue))
					this.chained_clues.Add (clue);
			}

			if (this.UpdateNotify != null)
				this.UpdateNotify ();
		}

		public CluePacket ToplevelCluePacket
		{
			get { return this.toplevel_cluepacket; }
		}

		public static bool operator== (EngineMatchSet s1, EngineMatchSet s2)
		{
			if (Object.ReferenceEquals (s1, null) && Object.ReferenceEquals (s2, null))
				return true;

			if (Object.ReferenceEquals (s1, null) || Object.ReferenceEquals (s2, null))
				return false;

			if (s1.matches != null || s2.matches != null) {
				if (s1.matches == null || s2.matches == null)
					return false;

				if (s1.matches.Count != s2.matches.Count)
					return false;

				foreach (Match m in s1.matches) {
					if (! s2.matches.Contains (m))
						return false;
				}
			}

			if (s1.chained_clues != null || s2.chained_clues != null) {
				if (s1.chained_clues == null || s2.chained_clues == null)
					return false;

				if (s1.chained_clues.Count != s2.chained_clues.Count)
					return false;

				foreach (Clue c in s1.chained_clues) {
					if (! s2.chained_clues.Contains (c))
						return false;
				}
			}

			return true;
		}

		// This operator does not consider the variables marked 'input' or 'internal' above
		public static bool operator!= (EngineMatchSet s1, EngineMatchSet s2)
		{
			return ! (s1 == s2);
		}

		public override bool Equals (object obj)
		{
			if (! (obj is EngineMatchSet))
				return false;

			return this == (EngineMatchSet) obj;
		}

		public override int GetHashCode ()
		{
			int hashcode = this.matches.GetHashCode () ^
				       this.chained_clues.GetHashCode ();

			return hashcode;
		}

		public object Clone ()
		{
			EngineMatchSet clone = new EngineMatchSet ();
			clone.UpdateNotify = this.UpdateNotify;
			clone.matches = this.matches;
			clone.chained_clues = this.chained_clues;

			return clone;
		}

	}
}
