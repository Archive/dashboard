//
// GNOME Dashboard
//
// TcpServer.cs: A TCP server that derives from Dashboard.Server
// (Server.cs) and listens for incoming cluepackets.
//

using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;

namespace Dashboard {

	class TcpServer : Server {
		private const int port = 5913;
		private bool stop;
		private TcpListener listener;
		
		public TcpServer () : base () {
		}

		public override void Run () {
			listener = new TcpListener (port);
			listener.Start ();
      			stop = false;

			Console.WriteLine ("Dashboard server waiting for connections on port " + port);

			while (!stop) {
				if (listener.Pending ()) {
					TcpClient client = listener.AcceptTcpClient ();
					Console.WriteLine ("Dashboard server got TCP connection on port " + port);
					ThreadPool.QueueUserWorkItem (new WaitCallback (this.ThreadProc), client);
				}
				Thread.Sleep (500);
			}
			Console.WriteLine ("TcpServer.Run () is now exiting...");
		}

		public override void Stop () {
			stop = true;
			listener.Stop ();
			Console.WriteLine ("Stopping server...");
		}

		private void ThreadProc (object o) {
			TcpClient client = (TcpClient) o;

			this.HandleConnection (client);
		}

		private void HandleConnection (TcpClient client) {
			NetworkStream stream = client.GetStream ();
			byte[] cluepacket_data = new byte [4096];
			string xml = "";

			int bytes_read;
			while ((bytes_read = stream.Read (cluepacket_data, 0, 4095)) > 0) {
				xml += Encoding.UTF8.GetString (cluepacket_data, 0, bytes_read);
			}

			this.HandleBuffer (xml);

			client.Close ();
			Console.WriteLine ("TCP connection closed");
		}

	}
}
