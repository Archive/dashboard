//
// match.cs: a single match
//
// Copyright (c) 2003, 2004 Jim McDonald
//

using System;
using System.Collections;

namespace Dashboard {

	public class Match : ICloneable
	{

		private string type;
		private int score;
		private Hashtable properties;

		public ArrayList TriggeringClues;

		public string Type {
			get { return this.type; }
		}

		public int Score {
			get { return this.score; }
			set { this.score = value; }
		}

		public Match (string type, Clue triggeringclue)
		{
			if (triggeringclue == null) {
				Console.WriteLine ("NULL TRIGGERING CLUE NOT ALLOWED IN MATCH CONSTRUCTOR");
				Environment.Exit (1);
			}

			this.type = type;
			this.score = 0;
			this.properties = new Hashtable ();

			this.TriggeringClues = new ArrayList ();
			this.TriggeringClues.Add (triggeringclue);
		}

		public Match (string type, ArrayList triggeringclues)
		{
			this.type = type;
			this.score = 0;
			this.properties = new Hashtable ();

			this.TriggeringClues = new ArrayList ();
			foreach (Clue c in triggeringclues)
				this.TriggeringClues.Add (c);
		}

		public object this [string name]
		{
			get {
				if (!this.properties.Contains (name))
					return null;

				return (string)this.properties [name];
			}

			set {
				this.properties [name] = value;
			}
		}

		public Hashtable Properties {
			get { return this.properties; }
		}

		public static bool operator== (Match r1, Match r2)
		{
			if (Object.ReferenceEquals (r1, null) && Object.ReferenceEquals (r2, null))
				return true;

			if (Object.ReferenceEquals (r1, null) || Object.ReferenceEquals (r2, null))
				return false;


			if (r1.type != null || r2.type != null) {
				if (r1.type == null || r2.type == null)
					return false;

				if (r1.type != r2.type)
					return false;
			}

			foreach (string prop_key in r1.properties.Keys) {
				if (!r2.properties.Contains (prop_key))
					return false;

				object p1 = r1.properties [prop_key];
				object p2 = r2.properties [prop_key];

				if ((p1 == null && p2 != null) || (p1 != null && p2 == null))
					return false;

				if (p1 != null && p2 != null) {
					if (p1.GetHashCode () != p2.GetHashCode ())
						return false;
				}
			}

			return true;
		}

		public static bool operator != (Match r1, Match r2)
		{
			return ! (r1 == r2);
		}

		public override bool Equals (object obj)
		{
			if (! (obj is Match))
				return false;

			return this == (Match) obj;
		}

		public override int GetHashCode ()
		{
			return this.type.GetHashCode () ^ this.properties.GetHashCode () ^ this.TriggeringClues.GetHashCode ();
		}

		public object Clone ()
		{
			Match clone = new Match (this.type, this.TriggeringClues);
			clone.score = this.score;
			clone.properties = (Hashtable) this.properties.Clone ();

			return clone;
		}

	}
}
