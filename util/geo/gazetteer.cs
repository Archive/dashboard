// Gazetteer
// by Edd Dumbill <edd@usefulinc.com>
// thanks to Matt Hunt for his help with this.
//
// TODO:
//   Get nearest airport details and add a gazetteer for those too.

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Threading;
using System.Runtime.Remoting.Messaging;
using System.Reflection;
using System.Runtime.InteropServices;
using ICSharpCode.SharpZipLib;

namespace Dashboard {
	namespace Geo {

	public class Point {
		public double Latitude;
		public double Longitude;
		public string City;
		public string Adminregion;
		public string Country;
		public string Isocode;
		public int Id;

		private static double EARTH_RADIUS = 6367000.0;

		public Point ()
		{
		}

		public Point (int id, double latitude, double longitude,
				string city,
				string adminregion,
				string country,
				string isocode) 
		{
			this.Id = id;
			this.Latitude = latitude;
			this.Longitude = longitude;
			this.City = city;
			this.Adminregion = adminregion;
			this.Country = country;
			this.Isocode = isocode;
		}

		public double DistanceFrom (Point p)
		{
			// compute great circle distance
			// thanks to http://www.indo.com/distance/dist.pl
			double dlon = p.Longitude - this.Longitude;
			double dlat = p.Latitude - this.Latitude;
			double a;
			
			a = Math.Pow (Math.Sin (dlat / 2.0), 2.0) + Math.Cos(this.Latitude) * Math.Cos(p.Latitude) * Math.Pow (Math.Sin(dlon / 2.0), 2.0);
			double d;
			
			d = 2.0 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1.0 - a));

			return d;
		}
	}

	public class DistanceFrom {
		public Point Point;
		public double Distance;
		public DistanceFrom (Point p, double d)
		{
			this.Point = p;
			this.Distance = d;
		}
	}

	public class LatitudeComparer : IComparer {
		public int Compare (object obj1, object obj2)
		{
			Point p = (Point) obj1;
			Point q = (Point) obj2;
			if (p.Latitude < q.Latitude)
				return -1;
			else if (p.Latitude > q.Latitude)
				return 1;
			return 0;
		}
	}

	public class LongitudeComparer : IComparer {
		public int Compare (object obj1, object obj2)
		{
			Point p = (Point) obj1;
			Point q = (Point) obj2;
			if (p.Longitude < q.Longitude)
				return -1;
			else if (p.Longitude > q.Longitude)
				return 1;
			return 0;
		}
	}
	
	public class DistanceComparer : IComparer {
		public int Compare (object obj1, object obj2)
		{
			DistanceFrom p = (DistanceFrom) obj1;
			DistanceFrom q = (DistanceFrom) obj2;
			if (p.Distance < q.Distance)
				return -1;
			else if (p.Distance > q.Distance)
				return 1;
			return 0;
		}
	}

	public class Gazetteer {
		
		private ArrayList points = new ArrayList ();
		private ArrayList latindex = new ArrayList ();
		private ArrayList lngindex = new ArrayList ();
		
		public Gazetteer () 
		{
			Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly ();

			System.IO.Stream s = assembly.GetManifestResourceStream ("cities.txt.gz");
			Load (new ICSharpCode.SharpZipLib.GZip.GZipInputStream (s));
		}

		public void Load (System.IO.Stream s)
		{
			Encoding e = Encoding.GetEncoding ("iso-8859-1");
			StreamReader f = new StreamReader (s, e);

			string l = f.ReadLine ();
			l = f.ReadLine ();
			int i = 0;
			while (l != null) {
				string [] vals = l.Split ('\t');

				if (vals[8] == "\"&nbsp;\"" ||
						vals[9] == "\"&nbsp;\"") {
					l = f.ReadLine ();
					continue;
				}

				double lng, lat;

				if (vals[8].EndsWith ("N")) 
					lat = Double.Parse (vals[8].Substring (0,
								vals[8].Length-2));
				else
					lat = - ( Double.Parse (vals[8].Substring (0,
								vals[8].Length-2)) );
				if (vals[9].EndsWith ("E")) 
					lng = Double.Parse (vals[9].Substring (0,
								vals[9].Length-2));
				else
					lng = - ( Double.Parse (vals[9].Substring (0,
								vals[9].Length-2)) );


			 	/* Console.WriteLine ("{0} {1} --> {2} {3} == {4} {5} {6} {7}",
					vals[8], vals[9], lng, lat,
					vals[1], vals[3], vals[4], vals[5]); */
				
				Point p = new Point (Int32.Parse (vals[7]), lat, lng,
						vals[1], vals[3], vals[4], vals[5]);

				points.Add (p);
				latindex.Add ( p);
				lngindex.Add ( p);
				l = f.ReadLine();
				i++;
			}
			f.Close();
			latindex.Sort ( new LatitudeComparer ());
			lngindex.Sort ( new LongitudeComparer ());
			// Console.WriteLine ("Read {0} cities.", i);
		}

		public ArrayList NearestCities (double latitude, double longitude) {
			Point p = new Point ();
			double minlat, maxlat, minlng, maxlng;
			int iminlat, imaxlat, iminlng, imaxlng;
			LatitudeComparer latc = new LatitudeComparer();
			LongitudeComparer lngc = new LongitudeComparer();

			// FIXME: Instead of the static 4 degree search we should
			// start at 1 degree and then widen out in, say, 3 degree
			// steps until something is found.
			minlat = latitude - 2.0;
			minlng = longitude - 2.0;
			maxlat = latitude + 2.0;
			maxlng = longitude + 2.0;

			if (minlat < -90.0) 
				minlat = -90.0;
			if (minlng < -180.0) 
				minlng = -180.0;
			if (maxlat > 90.0)
				maxlat =  90.0;
			if (maxlng > 180.0)
				maxlng =  180.0;

			// search within this 'square'

			p.Latitude = minlat;
			p.Longitude = minlng;

			int r;

			iminlat = latindex.BinarySearch(p, latc);
			if (iminlat < 0)
				iminlat = ~iminlat;

			iminlng = lngindex.BinarySearch(p, lngc);
			if (iminlng < 0)
				iminlng = ~iminlng;

			p.Latitude = maxlat;
			p.Longitude = maxlng;

			imaxlat = latindex.BinarySearch(p, latc);
			if (imaxlat < 0)
				imaxlat = ~imaxlat;

			imaxlng = lngindex.BinarySearch(p, lngc);
			if (imaxlng < 0)
				imaxlng = ~imaxlng;

			// Console.WriteLine ("{0} {1} - {2} {3}",
			//		iminlat, iminlng, imaxlat, imaxlng);

			Hashtable ids = new Hashtable ();

			for (int j=iminlat; j < imaxlat; j++) {
				Point q = (Point) latindex[j];
				ids[q.Id] = 1;
			}
			
			ArrayList near = new ArrayList ();
			
			p.Latitude = latitude;
			p.Longitude = longitude;

			for (int j=iminlng; j < imaxlng; j++) {
				Point q = (Point) lngindex[j];
				if (ids.Contains (q.Id))
					near.Add (new DistanceFrom (q,
								q.DistanceFrom (p)));
			}

			near.Sort (new DistanceComparer ());

			return near;
		}

		public Point NearestCity (double latitude, double longitude)
		{
			ArrayList l = NearestCities (latitude, longitude);
			if (l.Count > 0) {
				DistanceFrom d = (DistanceFrom) l[0];
				return d.Point;
			}
			return null;
		}
		
		public static void Main (string [] args)
		{
			Dashboard.Geo.Gazetteer g = new Dashboard.Geo.Gazetteer ();
			Point p = g.NearestCity (53.96539, -1.04440240);
			Console.WriteLine ("Nearest city is {0}, {1}, {2}.",
					p.City, p.Adminregion, p.Country);
		}
	}

	}
}

