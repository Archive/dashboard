using System;
using System.Collections;
using System.Web;
using System.Text;

using Drive.Rdf;
using Dashboard.Rdf;
using Dashboard.Rdf.Foaf;
using Dashboard;

namespace Dashboard.Rdf.Foaf {


	public class Properties {

		public static string NAMESPACE_URI = "http://xmlns.com/foaf/0.1/";

		public static string NAME = "http://xmlns.com/foaf/0.1/name";
		public static string HOMEPAGE = "http://xmlns.com/foaf/0.1/homepage";
		public static string WEBLOG = "http://xmlns.com/foaf/0.1/weblog";
		public static string IMAGE = "http://xmlns.com/foaf/0.1/image";
		public static string MBOX = "http://xmlns.com/foaf/0.1/mbox";
		public static string MBOX_SHA1SUM = "http://xmlns.com/foaf/0.1/mbox_sha1sum";
		public static string NICK = "http://xmlns.com/foaf/0.1/nick";
		public static string KNOWS = "http://xmlns.com/foaf/0.1/knows";
		public static string AIMCHATID = "http://xmlns.com/foaf/0.1/aimChatID";
		public static string YAHOOCHATID = "http://xmlns.com/foaf/0.1/yahooChatID";
		public static string MSNCHATID = "http://xmlns.com/foaf/0.1/msnChatID";
		public static string ICQCHATID = "http://xmlns.com/foaf/0.1/icqChatID";
		public static string PHONE = "http://xmlns.com/foaf/0.1/phone";
	}

	public class Store {
		private IRdfGraph graph;
		private PropertyIndex pindex;

		public Store (string location)
		{
			IRdfParserFactory pf = new RdfParserFactory ();
			IRdfParser p = pf.GetRdfXmlParser ();
			this.graph = p.ParseRdf (location);

			this.pindex = new PropertyIndex (this.graph);
		}

		public IRdfNode FindPerson (string property, string value)
		{
			foreach (IRdfEdge e in this.pindex.Edges (property)) {
				if (e.ChildNode.ID == value)
					return e.ParentNode;
			}

			return null;
		}

		public ArrayList GetValues (IRdfNode node, string property)
		{
			ArrayList ret = new ArrayList ();

			foreach (IRdfEdge e in node.ChildEdges) {
				if (e.ID == property)
					ret.Add (e.ChildNode);
			}
			return ret;
		}

		public ArrayList GenerateClues (string property, string value, Clue c)
		{
			ArrayList clues = new ArrayList ();

			IRdfNode person = FindPerson (property, value);

			if (person == null)
				return clues;

			// FIXME: much duplicated code below, find an idiom to
			// contract it

			foreach (IRdfNode n in GetValues (person, Properties.NAME))
				clues.Add (new Clue ("full_name", n.ID, 10, c));

			foreach (IRdfNode n in GetValues (person, Properties.HOMEPAGE))
				clues.Add (new Clue ("url", n.ID, 10, c));

			foreach (IRdfNode n in GetValues (person, Properties.MBOX))
				// they start with mailto: so we start at posn 7
				clues.Add (new Clue ("email", n.ID.Substring (7), 10, c));

			foreach (IRdfNode n in GetValues (person, Properties.PHONE))
				// they start tel: or fax: so we start a posn 4
				clues.Add (new Clue ("phone", n.ID.Substring (4), 10, c));

			foreach (IRdfNode n in GetValues (person, Properties.AIMCHATID))
				clues.Add (new Clue ("aim_name", n.ID, 10, c));

			foreach (IRdfNode n in GetValues (person, Properties.MSNCHATID))
				clues.Add (new Clue ("msn_name", n.ID, 10, c));

			foreach (IRdfNode n in GetValues (person, Properties.YAHOOCHATID))
				clues.Add (new Clue ("yahoo_name", n.ID, 10, c));

			foreach (IRdfNode n in GetValues (person, Properties.ICQCHATID))
				clues.Add (new Clue ("icq_name", n.ID, 10, c));

			return clues;
		}

		public Match GenerateMatch (string property, string value, Clue c) {
				Match match = new Match ("Addressbook", c);

			IRdfNode person = FindPerson (property, value);

			if (person == null)
					return null;

			ArrayList names = GetValues (person, Properties.NAME);

			if (names.Count == 0)
				return null;

			IRdfNode name = (IRdfNode) names[0];

			foreach (IRdfNode n in GetValues (person, Properties.MBOX)) 
					match ["Email"] = n.ID.Substring (7);

			foreach (IRdfNode n in GetValues (person, Properties.HOMEPAGE))
					match ["Homepage"] = n.ID;

			foreach (IRdfNode n in GetValues (person, Properties.WEBLOG))
					match ["Weblog"] = n.ID;

			foreach (IRdfNode n in GetValues (person, Properties.NICK))
					match ["Nick"] = n.ID;

			foreach (IRdfNode n in GetValues (person, Properties.ICQCHATID))
					match ["ICQ"] = n.ID;

			foreach (IRdfNode n in GetValues (person, Properties.MSNCHATID))
					match ["MSN"] = n.ID;

			foreach (IRdfNode n in GetValues (person, Properties.AIMCHATID))
					match ["AIM"] = n.ID;

			foreach (IRdfNode n in GetValues (person, Properties.YAHOOCHATID))
					match ["Yahoo"] = n.ID;

			foreach (IRdfNode n in GetValues (person, Properties.IMAGE))
					match ["Photo"] = n.ID;

			return match;
		}

		public static void Main (string [] args) {
			Store s = new Store ("http://heddley.com/edd/foaf.rdf");

			Console.WriteLine ("Loaded FOAF file, now processing");
			//			Console.WriteLine (
			//				s.GenerateHtmlCard (Properties.MBOX,
			//					"mailto:edd@usefulinc.com"));
		}

	}

	public class HtmlUtil {
		public static string Header (string title) {
			return String.Format (
					"      <table border=\"0\" height=\"100%\" width=\"100%\" valign=\"center\"> <tr> <td>" + 
            "       <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"100%\">" + 
            "         <tr bgcolor=\"#000000\"> <td>" + 
            "         <table border=\"0\" cellspacing=\"1\" cellpadding=\"0\" width=\"100%\">" + 
            "           <tr bgcolor=\"#f0f0f0\"> <td>" + 
            "           <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">" + 
            "             <tr bgcolor=\"#303030\"> " + 
            "                             <td valign=\"center\" align=\"left\">" + 
            "                     <img valign=\"center\" src=\"internal:foaf.png\">" + 
            "                 </td>" + 
            "                             <td valign=\"center\" align=\"left\" width=100%>" + 
            "                     <font color=\"#ffffff\" size=\"+2\">{0}</font>"  + 
            "                 </td>" + 
            "             </tr>" + 
            "           </table>" + 
            "<table border=\"0\" width=\"100%\">",
			HttpUtility.HtmlEncode (title));
		}

		public static string Footer () {
			return "</table></td></tr></table>" + 
                "</td></tr></table></td></tr></table>";
		}

		public static string Abbreviate (string text) {
			if (text.Length > 22)
				return text.Substring (0, 19) + "...";
			return text;
		}

		public static string Field (string title, string value) {
			return String.Format ("<tr><td align=\"right\">{0}:</td> <td><tt>{1}</tt></td></tr>",
					HttpUtility.HtmlEncode (title),
					HttpUtility.HtmlEncode (value));
		}

		public static string Field (string title, string value, string link) {
			return String.Format ("<tr><td align=\"right\">{0}:</td> <td><tt><a href=\"{2}\">{1}</a></tt></td></tr>",
					HttpUtility.HtmlEncode (title),
					Abbreviate (HttpUtility.HtmlEncode (value)),
					HttpUtility.HtmlEncode (link));
		}
	}
}

