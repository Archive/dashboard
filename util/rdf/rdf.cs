using System;
using System.Collections;
using Drive.Rdf;

namespace Dashboard.Rdf {

	public class PropertyIndex : Hashtable {

		public PropertyIndex (IRdfGraph g) {
			IRdfNodeCollection nodes = g.Nodes;
			IDictionaryEnumerator en = (IDictionaryEnumerator)
				nodes.GetEnumerator ();

			while (en.MoveNext ()) {
				IRdfNode parentnode = (IRdfNode) en.Value;
				IRdfEdge edge;
				int count = parentnode.ChildEdges.Count;

				for (int i = 0; i < count; i++ ) {
					edge = (IRdfEdge) parentnode.ChildEdges [i];
					ArrayList nodelist;
					if (this.Contains (edge.ID)) {
						nodelist = (ArrayList) this[edge.ID];
					} else {
						nodelist = new ArrayList ();
						this.Add (edge.ID, nodelist);
					}
					nodelist.Add (edge);
				}
			}
		}

		public ArrayList Edges (string property) {
			if (this.Contains (property))
				return (ArrayList) this[property];
			else
				return new ArrayList ();
		}
	}

}
