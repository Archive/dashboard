#!/bin/sh
#
# Download the right versions of the Dashboard-patched applications,
# patch them, and build them.
#
# Nat Friedman <nat@nat.org>
#

mkdir approot 2> /dev/null
cd approot

if [ x$1 = x ]
then
    echo
    echo You must specify an argument.
    echo
    echo "    First run:"
    echo "       % ./build-applications.sh patch"
    echo "    then "
    echo "       % ./build-applications.sh build <args to autogen>"
    echo "    Make sure your GNOME CVSROOT is set."
    echo 
    echo You can also specify the \"clean\" argument to remove your
    echo trees.
    echo

    exit
fi

if [ $1 = "patch" ]
then
  for p in  ../patches/*.patch ../frontends/*.patch
  do
    if [ -f $p ]
    then
      bname=`basename $p`
      module=`echo $bname |cut -d_ -f 1`
      desc=`echo $bname |cut -d_ -f 2 |tr - ' '`
      timestamp=`echo $bname |cut -d_ -f 3 |tr - ' ' |cut -d . -f 1`

      cvsroot_file=`dirname $p`/$bname.CVSROOT
      if [ -f $cvsroot_file ]
      then
	  my_cvsroot=`cat $cvsroot_file`
      else
	  my_cvsroot=$CVSROOT
      fi
      
      echo 
      echo -----------------------------------------------------------
      echo Patch: $bname
      echo Against module: $module
      echo Description: $desc
      echo CVS timestamp: $timestamp
      echo CVSROOT: $my_cvsroot
      echo -----------------------------------------------------------
      echo

      sleep 10
  
      echo
      echo Checking out $module...
      echo
      if ! cvs -z3 -d "$my_cvsroot" co -D "$timestamp" $module
      then
	  echo
	  echo FAILED CHECKING OUT $module
	  echo
	  exit
      fi
  
      echo
      echo Applying patch ...
      echo
      cd $module
      if ! patch -p0 < ../$p
      then
	  echo
	  echo FAILED PATCHING $module with $bname
	  echo
	  exit
     fi

      cd -
    fi
  done
fi

if [ $1 = "build" ]
then
  shift
  for mod in *
  do
    
      cd $mod
      echo 
      echo -----------------------------------------------------------
      echo Module: $mod
      echo -----------------------------------------------------------
      echo

      echo
      echo Autogenning with args: $*
      echo
      if ! ./autogen.sh $*
      then
	  echo
	  echo FAILED AUTOGEN $module with args $*
	  echo
	  exit
      fi
      
      echo
      echo Building...
      echo
      if ! make
      then
	  echo
	  echo FAILED BUILDING $module
	  echo
	  exit
      fi

      cd -
  done
fi

if [ $1 = "clean" ]
then
    cd ..
    echo
    echo WARNING: About to rm -rf approot!!
    echo
    echo Hit Ctrl-C now to abort!
    echo
    sleep 3

    rm -rf approot

    echo done.
fi
